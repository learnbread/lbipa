from datetime import datetime as dt
import requests
import json
from config.dbConn import Db

class OurTransaction:
    """
    Records all transactions on a wallet
    """
    #wallet = None
    #reference = ""
    #datetime = dt.utcnow()
    #status = ""
    #amount = 0
    #transactionType = None
    #message = ""
    #checksum = ""

    #def __init__(self, wallet, reference, status, amount, transaction_type, msg, checksum):
    #    self.wallet = wallet
    #    self.reference = reference
    #    self.status = status
    #    self.transactionType = transaction_type  # we should use TransactionType.CREDIT or TransactionType.DEBIT
    #    self.amount = amount
    #    self.message = msg
    #    self.checksum = checksum

    #def __str__(self):
        #pass
    
    @staticmethod
    def pending_transaction_id():
        pass

    @staticmethod
    def completed_transaction_id():
        pass

    @staticmethod
    def canceled_transaction_id():
        pass

    @staticmethod
    def verify_callback_function(response):
        #confirm that the response for the transaction is successful
        pass
            
    @staticmethod
    def verify_payment(data):
        
        send_data = {
            "txref": "scra-1589364842227", #this is the reference from the payment button response after customer paid.
            "SECKEY": "FLWSECK_TEST-e6f4f62dd112799afeeaf135e2665af5-X" #this is the secret key of the pay button generated
        }

        #function called after your request gets a response from rave's server
        
        #this is the url of the staging server. Please make sure to change to that of production server when you are ready to go live.
        url = "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/v2/verify"

        #make the http post request to our server with the parameters
        ddd = json.dumps(send_data)
        r_response = requests.post(url, ddd, headers={"Content-Type":"application/json"})
        main_r = r_response.json()
        if main_r['status'] == "success":
            return main_r
        else:
            error_r = {
                "status": "error",
                "message": "Couldn't verify this transaction, please send us a message"
            }
            return (error_r)
        """
        if r_response.body['data']['status'] == 'success':
            #confirm that the amount for that transaction is the amount you wanted to charge
            if response.body['data']['chargecode'] == '00':
            
                if response.body['data']['amount'] == 2000:
                    print("Payment successful then give value")
        """