from abc import ABC, abstractmethod


class Pot(ABC):
    name = ""
    maxAmountBeforePayout = 0.0
    currentBalance = 0.0

    def __init__(self, name, max_amount_before_payout, current_balance):
        self.name = name
        self. maxAmountBeforePayout = max_amount_before_payout
        self.currentBalance = current_balance

    @abstractmethod
    def set_current_balance(self):
        pass

    @abstractmethod
    def set_max_amount_before_payout(self):
        pass

    def reset_pot (self):
        self.currentBalance = 0.0
