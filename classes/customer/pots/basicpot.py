#from Constants import BASIC_POT_MAX_BEFORE_PAYOUT
#from Pots.Pot import Pot


class BasicPot():
    BASIC_POT_MAX_BEFORE_PAYOUT = ""

    def __init__(self, name, current_balance):
        super(self, name, self.BASIC_POT_MAX_BEFORE_PAYOUT, current_balance)

    def set_current_balance(self, amount):
        self.currentBalance = amount

    def set_max_amount_before_payout(self, amount):
        self.maxAmountBeforePayout = amount

    def update_current_balance(self, amount):
        self.currentBalance += amount

    def payout_winnings(self):
        if self.maxAmountBeforePayout == self.currentBalance:
            # TODO we make a payout to the winning customers wallet - BASIC_POT_MAX_PAYOUT_AMOUNT see Constants.py
            # Reset the pot
            #self.reset_pot()
            pass
