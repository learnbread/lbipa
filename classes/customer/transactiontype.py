import enum


class TransactionType:
    CREDIT = 1
    DEBIT = 2
    pass