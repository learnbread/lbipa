#from customer.profile import Profile
from config.dbConn import Db
from auth.ourauth import OurAuth
from classes.customer.transaction import OurTransaction


class OurWallet:
    """
    The wallet class determines on the current financial status of the
    user i.e. current balance, withdraw and deposit
    """
    owner = None
    winnings = 0.0
    pin = ""

    balance = ""
    profile = ""
    
    @staticmethod
    def deposit_fund(amount):
        pass

    @staticmethod
    def add_winnings(amount):
        pass
       
    @staticmethod
    def withdraw_winnings(amount):
        # we need to move this amount to the owner registered bank account we only withdraw winnings we
        # don't withdraw from game play balance
        pass
    
    @staticmethod
    def debit_wallet(data):
        balance = OurWallet.get_wallet_balance(data)
        newbal = float(balance) - float(data['amount'])
        data['newbalance'] = str(newbal)

        return OurWallet.update_wallet_balance(data)

    @staticmethod
    def credit_wallet(data):
        balance = OurWallet.get_wallet_balance(data)
        newbal = float(balance) + float(data['amount'])
        data['newbalance'] = str(newbal)

        return OurWallet.update_wallet_balance(data)

    @staticmethod
    def fund_wallet(data):
        return OurTransaction.verify_payment(data)

        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        sql = "SELECT transactions.tx_status_id AS txstatus FROM transactions WHERE transactions.tx_ref = %s AND transactions.user_id = %s"
        val = (data['tx_ref'], data['userdata']['id'])
        
        mycursor.execute(sql, val)
        myresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            if myresult['txstatus'] == OurTransaction.pending_transaction_id():
                payresponse = OurTransaction.verify_payment(data)
                
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "walletbalance": myresult['walletbalance']
                }

                #return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Couldn't get Transaction. Operation Not successful",
                "walletbalance": "0"
            }
            return response

    @staticmethod
    def update_wallet_balance(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        sql = "UPDATE users SET users.uwallet = %s WHERE users.id = %s"
        val = (data['newbalance'], data['userdata']['id'])
        
        mycursor.execute(sql, val)
        mydb.commit()
        if mycursor.rowcount > 0 :
            response = {
                "status": "success",
                "code": 200,
                "message": "Wallet Update Operation Successful"
            }
            return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Wallet update not successful"
            }
            return response
        
    @staticmethod
    def get_wallet_balance(data):
        
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        sql = "SELECT users.u_max_withdraw, users.u_min_withdraw, users.u_max_deposit, users.u_min_deposit, users.uwallet AS walletbalance FROM users WHERE users.id = %s AND users.status = %s"
        val = (data['userdata']['id'], 0)
        
        mycursor.execute(sql, val)
        myresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            response = {
                "status": "success",
                "code": 200,
                "message": "Operation successful",
                "walletbalance": myresult['walletbalance'],
                "u_max_withdraw": myresult['u_max_withdraw'],
                "u_min_withdraw": myresult['u_min_withdraw'],
                "u_max_deposit": myresult['u_max_deposit'],
                "u_min_deposit": myresult['u_min_deposit']
            }

            return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Operation Not successful",
                "walletbalance": "0"
            }
            return response