class Profile:
    """
    User Customer will hold user information such
    as first_name, last_name, email, birth_date , mobile number  for
    two factor verification
    """
    firstName = ""
    lastName = ""
    email = ""
    mobile = ""
    birthday = ""
    isActive = True
    bank = ""
    accountNumber = ""

    def __init__(self, first_name, last_name, email, mobile, birth_date, is_active,bank, account_number):
        self.firstName = first_name
        self.lastName = last_name
        self.email = email
        self.mobile = mobile
        self.birthday = birth_date
        self.isActive = is_active
        self.bank = bank
        self.accountNumber = account_number

    def disable_profile(self, ):
        self.isActive = False

    def __str__(self):
        return "UserProfile for : {email} ".format(email=self.email)
