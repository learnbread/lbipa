import smtplib
from smtplib import SMTPException

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from config.config import Config
from auth.ourauth import OurAuth


class OurEmailSender:

    @staticmethod
    def sendMail(data):
        pass

    @staticmethod
    def format_to_html(data):
        html = """\
        <html>
        <body>
            <p>Hi """+data['fullname']+""",
            Your registration was successful. Click the Verification link to activate your account. 
            <a href=" """+ data['confirm_url'] +""" ">Click Here to Activate your account</a> 
            <br/>
            If you didn't subscribe to this, please ignore.
            </p>
        </body>
        </html>
        """

        return html
        
    @staticmethod
    def format_to_text(data):
        text = """\
        Hi """+data['fullname']+""",
        Please copy link to your Address bar or click the link to activate your account.
        
        """+ data['confirm_url'] +""" 

        If you didn't subscribe to this, please ignore.
            
        """
        
        return text
    
    @staticmethod
    def format_resetpwd_text(data):
        text = """\
        Hi ,
        Please click the link to create a new password.
        
        """+ data['reset_url'] +""" 

        If you didn't know about the change of your account password, please ignore.
            
        """
        
        return text
    
    @staticmethod
    def format_resetpwd_html(data):
        html = """\
        <html>
        <body>
            <p>Hi,<br/>
            Please, Click the link to complete your change of password. 
            <a href=" """+ data['reset_url'] +""" ">Click here to create a new password</a> 
            <br/>
            If you didn't know about the change of your account password, please ignore.
            </p>
        </body>
        </html>
        """

        return html
    

    @staticmethod
    def confirm_registration(data):
        return True
        data['confirm_url'] = Config.BASE_DOMAIN_URL+'confirm/'+data['confirm_token']
        
        message = MIMEMultipart("alternative")
        message["Subject"] = "Confirm Registration"
        message["From"] = Config.MAIL_DEFAULT_SENDER
        message["To"] = data['email']
        
        text = OurEmailSender.format_to_text(data)
        html = OurEmailSender.format_to_html(data)

        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        message.attach(part1)
        message.attach(part2)
        try:
            server = smtplib.SMTP_SSL(Config.MAIL_SERVER, Config.MAIL_PORT)
            server.ehlo()
            uname = Config.MAIL_USERNAME
            pword = Config.MAIL_PASSWORD
            server.login(uname, pword)
            server.sendmail(Config.MAIL_DEFAULT_SENDER, message['To'], message.as_string())
            server.close()
            return True
        except:
            return False
    
    @staticmethod
    def reset_user_pwd(data):
        return True
        data['reset_url'] = Config.BASE_DOMAIN_URL+'reset/'+data['reset_token']
        
        message = MIMEMultipart("alternative")
        message["Subject"] = "Reset Your Password"
        message["From"] = Config.MAIL_DEFAULT_SENDER
        message["To"] = data['email']
        
        text = OurEmailSender.format_resetpwd_text(data)
        html = OurEmailSender.format_resetpwd_html(data)

        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        message.attach(part1)
        message.attach(part2)
        try:
            server = smtplib.SMTP_SSL(Config.MAIL_SERVER, Config.MAIL_PORT)
            server.ehlo()
            uname = Config.MAIL_USERNAME
            pword = Config.MAIL_PASSWORD
            server.login(uname, pword)
            server.sendmail(Config.MAIL_DEFAULT_SENDER, message['To'], message.as_string())
            server.close()
            return True
        except:
            return False