import hashlib, binascii, os
import random
from flask import jsonify
from werkzeug.utils import secure_filename

from datetime import datetime, timedelta

from config.dbConn import Db
from config.config import Config
from auth.ourauth import OurAuth
from classes.general.ouremailsender import OurEmailSender
from classes.customer.wallet import OurWallet

class SendServices:
    @staticmethod
    def confirm_user_reg(token):
        decodedtoken = OurAuth.AuthDecode(token)
        if decodedtoken['status']:
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "UPDATE mmadu_ochis SET mmadu_ochis.anyanatago_at = %s, mmadu_ochis.anyanatago = 1 WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.anyanatago_tmmadu = %s"
            val = (OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(decodedtoken['email']), '0', token)
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :

                mycursor1 = mydb.cursor(dictionary=True)
                sql_last1 = "SELECT \
                    temp_mmadu_ole AS temp_reg,\
                    mmadu_ole AS con_reg \
                    FROM ebe_mmamma_ochis WHERE ebe_mmamma_ochis.id = %s AND ebe_mmamma_ochis.ebe_ono = %s"
                
                val_last1 = (decodedtoken['country'], '0')
                mycursor1.execute(sql_last1, val_last1)
                
                myresult1 = mycursor1.fetchone()
                
                if mycursor1.rowcount > 0:
                    if myresult1['temp_reg'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                        myresult1['temp_reg'] = 0
                    else:
                        myresult1['temp_reg'] = OurAuth.TextDecode(myresult1['temp_reg'])
                    
                    if myresult1['con_reg'] in ("0", '0', 0, 'NULL', 'null', None, 'None'):
                        myresult1['con_reg'] = 0
                    else:
                        myresult1['con_reg'] = OurAuth.TextDecode(myresult1['con_reg'])
                    
                    new_t_nos = int(myresult1['temp_reg'])
                    up_new_t_nos = new_t_nos - 1

                    new_c_nos = int(myresult1['con_reg'])
                    up_new_c_nos = new_c_nos + 1
                    
                    up_mycursor = mydb.cursor(dictionary=True)
                    sqlnew = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.mmadu_ole = %s, ebe_mmamma_ochis.temp_mmadu_ole = %s WHERE ebe_mmamma_ochis.id = %s AND ebe_mmamma_ochis.ebe_ono = %s"
                    valnew = (OurAuth.TextEncode(str(up_new_c_nos)), OurAuth.TextEncode(str(up_new_t_nos)), decodedtoken['country'], '0')
                    
                    up_mycursor.execute(sqlnew, valnew)
                    mydb.commit()
                    if up_mycursor.rowcount > 0 :
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Account Activated Successfully"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry, we couldn't update your country count data"
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry, We couldn't get your country data to update"
                    }
            else:
                response = {
                    "status": "sessionerror",
                    "code": 400,
                    "message": "Sorry, seems Activation Link has expired or account has been activated"
                }
                return response
        else:
            response = {
                "status": "sessionerror",
                "code": 400,
                "message": "Sorry Activation link has expired"
            }
            return response

    @staticmethod
    def confirm_user_reset_pwd(token):
        decodedtoken = OurAuth.AuthDecode(token)
        if decodedtoken['status']:
            if decodedtoken['reset_status']:
                mydb = Db.dbfun()
                mycursor = mydb.cursor(dictionary=True)

                sql = "SELECT mmadu_ochis.mezie_npkosi_tmmadu AS reset_pwd_token FROM mmadu_ochis WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
                val = (OurAuth.TextEncode(decodedtoken['email']), )
                mycursor.execute(sql, val)
                r_here = mycursor.fetchone()
                
                if mycursor.rowcount > 0:
                    if r_here['reset_pwd_token'] == token:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Password reset token Confirmed",
                            "token": token
                        }
                        return response
                    else:
                        response = {
                            "status": "sessionerror",
                            "code": 401,
                            "message": "Authentication error, your link is expired or already used."
                        }
                        return response
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "User Doesn't exist"
                    }
                    return response
            else:
                response = {
                    "status": "error",
                    "code": 401,
                    "message": "Sorry, Authentication error, something has altered your link"
                }
                return response
        else:
            response = {
                "status": "sessionerror",
                "code": 400,
                "message": "Sorry the password reset link has expired"
            }
            return response

    @staticmethod
    def create_new_password(data):
        #need tocheck if user is in the process of change password
        decodedtoken = OurAuth.AuthDecode(data['token'])
        if decodedtoken['status']:
            if decodedtoken['reset_status']:
                pp_hash = SendServices.hash_pp(data['pword'])
                
                mydb = Db.dbfun()
                mycursor = mydb.cursor(dictionary=True)

                sql = "UPDATE mmadu_ochis SET mmadu_ochis.mezie_npkosi_at = %s, mmadu_ochis.nkoshi_mmadu = %s, mmadu_ochis.nnu_mmadu = %s, mmadu_ochis.mezie_npkosi_status = 0 WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1 AND mmadu_ochis.mezie_npkosi_tmmadu = %s AND mmadu_ochis.mezie_npkosi_status = 1"
                val = (OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), pp_hash['keyvalue'], pp_hash['salt'], OurAuth.TextEncode(decodedtoken['email']), data['token'])
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Password Reset Successful, kindly login and proceed"
                    }
                    return jsonify(response)
                else:
                    response = {
                        "status": "sessionerror",
                        "code": 400,
                        "message": "Sorry, seems Password reset link has been used or expired."
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 401,
                    "message": "Sorry, seems there is error, something has altered your link"
                }
                return jsonify(response)
        else:
            response = {
                "status": "sessionerror",
                "code": 400,
                "message": "Sorry the password reset link has expired"
            }
            return jsonify(response)
    
    @staticmethod
    def createtransactionref(data):
        pass    

    @staticmethod
    def get_wallet_balance(data):
        bresponse = OurWallet.get_wallet_balance(data)
        if bresponse['status'] == "success":
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "walletbalance": bresponse['walletbalance'],
                        "max_withdraw": bresponse['u_max_withdraw'],
                        "min_withdraw": bresponse['u_min_withdraw'],
                        "max_deposit": bresponse['u_max_deposit'],
                        "min_deposit": bresponse['u_min_deposit']
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            return jsonify(bresponse)

    @staticmethod
    def scratch_card(data):
        response = {
            "status": "success",
            "code": 200,
            "message": "Congratulations, you just won N400"
        }
        return jsonify(response)
    
    @staticmethod
    def rename_file(data):
        position = data.index('.')
        file_extension = data[position + 1:]
        
        if len(str(data[0:position])) < 11 :
            file_name = data[0:position]
        else:
            file_name = "images"

        suffix = datetime.now(OurAuth.ourtimezone).strftime("%Y%m%d_%H%M%S")
        
        both_join = "_".join([file_name, suffix])
        filename = ".".join([both_join, file_extension])

        return filename

    @staticmethod
    def hash_pp(data):
        #salt = os.urandom(32)
        salt = str(Config.AUTH_SECRET_KEY).encode()
        result1 = data.encode()
        #result2 = hashlib.md5('Passwordfile2'.encode())

        #return binascii.hexlify(result1)
        #save1now = result1.hexdigest()
        #save2now = result2.hexdigest()
        #return binascii.hexlify(salt)
        keyvalue = hashlib.pbkdf2_hmac('sha256', # The hash digest algorithm for HMAC
            result1, # Convert the password to bytes
            salt, # Provide the salt
            100000 # It is recommended to use at least 100,000 iterations of SHA-256 
        )
        return {
            "salt": binascii.hexlify(salt).decode(),
            "keyvalue": binascii.hexlify(keyvalue).decode()
        }

    @staticmethod
    def allowed_file(filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in Config.ALLOWED_FILE_EXTENSIONS
    
    @staticmethod
    def allowed_img(filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in Config.ALLOWED_IMG_EXTENSIONS
           
    @staticmethod
    def verify_pp(salt, stored_pp, provided_pp):
        check_keyvalue = hashlib.pbkdf2_hmac('sha256', # The hash digest algorithm for HMAC
            provided_pp.encode(), # Convert the password to bytes
            salt, # Provide the salt
            100000 # It is recommended to use at least 100,000 iterations of SHA-256
        )
        if(stored_pp == check_keyvalue):
            return "Yes we are the same"
    
    @staticmethod
    def closeConnection(mydb, mycursor):
        if mydb.is_connected():
            mycursor.close()
            mydb.close()
    
    @staticmethod
    def getroles():
        try:
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT \
                    uroles.id AS role_id,\
                    uroles.title AS role_title,\
                    upermissions.desc AS role_permission,\
                    upermissions.id AS role_permission_id\
                    FROM uroles\
                    JOIN upermissions ON uroles.upermission_id = upermissions.id ORDER BY uroles.title"
            #val = ('1')
            mycursor.execute(sql)
            myresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "roles": myresult
                    }
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "No role available yet, contact admin to create",
                    "data": myresult
                }
                return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return jsonify(response)
        finally:
            if(mydb.is_connected()):
                mycursor.close()
                mydb.close()
                #return "MySQL connection is closed"

    @staticmethod
    def getcategories():
        try:
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT categories.id, categories.title, categories.desc, categories.slug, categories.parent_cat_id FROM categories WHERE categories.status = 0 ORDER BY categories.title"
            #val = ('1')
            mycursor.execute(sql)
            myresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "cat": myresult
                    }
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "No parent category available yet, you can create one",
                    "data": myresult
                }
                return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return jsonify(response)
        
        finally:
            if(mydb.is_connected()):
                mycursor.close()
                mydb.close()
                #return "MySQL connection is closed"

    @staticmethod
    def getpermissions():
        try:
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT upermissions.id, upermissions.title, upermissions.desc FROM upermissions WHERE upermissions.status = 0 ORDER BY upermissions.title"
            #val = ('1')
            mycursor.execute(sql)
            myresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "perms": myresult
                    },
                    "utfa": "notok"
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "No Permissions available yet, you can create one",
                    "data": myresult
                }
                return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return jsonify(response)
        
        finally:
            if(mydb.is_connected()):
                mycursor.close()
                mydb.close()
                #return "MySQL connection is closed"

    @staticmethod
    def getusers(which_option, data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if which_option == "all":
            try:
                per_page = int(20)
                page_limit = int(per_page * int(data['v_page_nos']))
                v_num = int(0)

                #return jsonify(data)

                
                sql = "SELECT \
                        users.id AS user_id,\
                        users.fullname AS user_fullname,\
                        users.email AS user_email,\
                        users.phone AS user_phone,\
                        users.img AS user_img,\
                        uroles.id AS user_role_id,\
                        uroles.title AS user_role_title\
                        FROM users\
                        JOIN uroles ON users.urole_id = uroles.id \
                        WHERE users.id >= " + str(v_num) + " AND users.status = 0 ORDER BY users.fullname LIMIT " + str(page_limit)
                #val = ('1')
                mycursor.execute(sql)
                myresult = mycursor.fetchall()

                if mycursor.rowcount > 0:
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Operation successful",
                            "data": {
                                "users": myresult
                            },
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "No User available yet, kindly contact the admin",
                        "data": myresult
                    }
                    return jsonify(response)

                #for x in myresult:
                #   print(x)
                #return "Yes you are in about page"
            except:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Sorry your sql"
                }
                return jsonify(response)
            
            finally:
                if(mydb.is_connected()):
                    mycursor.close()
                    mydb.close()
                    #return "MySQL connection is closed"

        if which_option == "byrole":
            try:
                per_page = int(20)
                page_limit = int(per_page * int(data['v_page_nos']))

                #return page_limit
                g_from = int(data['g_from'])
                d_role = int(data['role_id'])

                sql = "SELECT \
                        users.id AS user_id,\
                        users.fullname AS user_fullname,\
                        users.email AS user_email,\
                        users.phone AS user_phone,\
                        users.img AS user_img,\
                        uroles.id AS user_role_id,\
                        uroles.title AS user_role_title\
                        FROM users\
                        JOIN uroles ON users.urole_id = uroles.id \
                        WHERE users.id >= " + str(g_from) + " AND users.urole_id = " + str(d_role) + " AND users.status = 0 ORDER BY users.fullname LIMIT " + str(page_limit)
                #val = ('1')
                mycursor.execute(sql)
                myresult = mycursor.fetchall()

                if mycursor.rowcount > 0:
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Operation successful",
                            "data": {
                                "users": myresult
                            },
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "This type of user isn't available yet, kindly contact the admin",
                        "data": myresult
                    }
                    return jsonify(response)

                #for x in myresult:
                #   print(x)
                #return "Yes you are in about page"
            except:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Sorry your sql"
                }
                return jsonify(response)
            
            finally:
                if(mydb.is_connected()):
                    mycursor.close()
                    mydb.close()
                    #return "MySQL connection is closed"

    @staticmethod
    def getCatID(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        tb = "categories"
        field = "title"

        try:
            #data_e = 'thankgoduchecyril@gmail.com'
            sql = "SELECT "+tb+".id FROM "+tb+" WHERE "+tb+"."+field+" = %s AND "+tb+".status = %s"
            val = (data, 0)
            mycursor.execute(sql, val)
            
            myresult = mycursor.fetchone()
            
            if mycursor.rowcount > 0:
                return myresult['id']
            else:
                return 0
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return response
        
        finally:
            Db.closeConnection(mydb, mycursor)
            #return "MySQL connection is closed"
    
    @staticmethod
    def getproduct(which_product, data):
        ourbaseurl = os.path.realpath(Config.BASE_IMG_URL)

        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        tb = "products"
        if data:
            if which_product == "allproducts":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here....
                    per_page = int(20)
                    page_limit = int(per_page * int(data['v_page_nos']))
                    v_num = int(0)
                    #return v_num

                    #products.buy_price AS product_buy_price,\
                    #products.sell_price AS product_sell_price,\
                    #products.created_at AS product_date,\
                    #products.img AS product_img,\
                        
                    sql = "SELECT \
                        "+tb+".id AS product_id, \
                        "+tb+".title AS product_title,\
                        "+tb+".desc AS product_desc,\
                        "+tb+".created_at AS product_date,\
                        "+tb+".buy_price AS product_buy_price,\
                        "+tb+".win_price AS product_win_price,\
                        "+tb+".img AS product_img,\
                        categories.title AS product_category,\
                        categories.id AS product_category_id\
                        FROM "+tb+"\
                        JOIN categories ON "+tb+".cat_id = categories.id\
                        WHERE "+tb+"."+"id >= " + str(v_num) + " AND "+tb+".status = 0 ORDER BY "+tb+".id DESC LIMIT " + str(page_limit)
                    mycursor.execute(sql)
                    myresult = mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        mycursor.execute("SELECT COUNT(*) FROM "+tb+" WHERE "+tb+".id >="+str(v_num))
                        rcount = mycursor.fetchone()['COUNT(*)']

                        tresponse = OurAuth.getToken(data["userdata"])
                        if tresponse['status'] == "success":
                            response = {
                                "status": "success",
                                "code": 200,
                                "message": "Products gotten successfully",
                                "data": {
                                    "products": myresult,
                                    "count": rcount,
                                    "last_post": myresult[0]['product_id'],
                                    "base_img_url": ourbaseurl + '\\'
                                },
                                "utfa": tresponse['token']
                            }
                            return response
                        else:
                            return tresponse
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no product available"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
            
            if which_product == "getgames":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here....
                    per_page = int(20)
                    page_limit = int(per_page * int(data['v_page_nos']))
                    v_num = int(0)
                    #return v_num

                    #products.buy_price AS product_buy_price,\
                    #products.sell_price AS product_sell_price,\
                    #products.created_at AS product_date,\
                    #products.img AS product_img,\
                        
                    sql = "SELECT \
                        "+tb+".id AS game_id, \
                        "+tb+".title AS game_title,\
                        "+tb+".desc AS game_desc,\
                        "+tb+".created_at AS game_date,\
                        "+tb+".buy_price AS game_entry_fee,\
                        "+tb+".win_price AS game_win_fee,\
                        "+tb+".img AS game_img,\
                        categories.title AS game_category,\
                        categories.id AS game_category_id\
                        FROM "+tb+"\
                        JOIN categories ON "+tb+".cat_id = categories.id\
                        WHERE "+tb+"."+"id >= " + str(v_num) +" AND "+tb+".status = 0 ORDER BY "+tb+".id DESC LIMIT " + str(page_limit)

                    mycursor.execute(sql)
                    myresult = mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        mycursor.execute("SELECT COUNT(*) FROM "+tb+" WHERE "+tb+".id >="+str(v_num))
                        rcount = mycursor.fetchone()['COUNT(*)']

                        tresponse = OurAuth.getToken(data["userdata"])
                        if tresponse['status'] == "success":
                            #return rcount
                            response = {
                                "status": "success",
                                "code": 200,
                                "message": "Products gotten successfully",
                                "data": {
                                    "games": myresult,
                                    "count": rcount,
                                    "last_post": myresult[0]['game_id'],
                                    "base_img_url": ourbaseurl + '\\'
                                },
                                "utfa": tresponse['token']
                            }
                            return response
                        else:
                            return tresponse
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no product available"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
            
            if which_product == "getgamesbycat":
                try:
                    per_page = int(20)
                    page_limit = int(per_page * int(data['v_page_nos']))
                    g_cat = int(data['cat_id'])
                    v_num = int(0)

                    sql = "SELECT \
                        "+tb+".id AS game_id, \
                        "+tb+".title AS game_title,\
                        "+tb+".desc AS game_desc,\
                        "+tb+".created_at AS game_date,\
                        "+tb+".buy_price AS game_entry_fee,\
                        "+tb+".win_price AS game_win_fee,\
                        "+tb+".img AS game_img,\
                        categories.title AS game_category,\
                        categories.id AS game_category_id\
                        FROM "+tb+"\
                        JOIN categories ON "+tb+".cat_id = categories.id\
                        WHERE "+tb+"."+"id >= "+str(v_num)+" AND "+tb+".cat_id = "+str(g_cat)+" AND "+tb+".status = 0 ORDER BY "+tb+".id DESC LIMIT " + str(page_limit)

                    mycursor.execute(sql)
                    myresult = mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        mycursor.execute("SELECT COUNT(*) FROM "+tb+" WHERE "+tb+".id >="+str(v_num))
                        rcount = mycursor.fetchone()['COUNT(*)']

                        tresponse = OurAuth.getToken(data["userdata"])
                        if tresponse['status'] == "success":
                            
                            response = {
                                "status": "success",
                                "code": 200,
                                "message": "Products gotten successfully",
                                "data": {
                                    "games": myresult,
                                    "count": rcount,
                                    "last_post": myresult[0]['game_id'],
                                    "selected_cat": data['cat_id'],
                                    "base_img_url": ourbaseurl + '\\'
                                },
                                "utfa": tresponse['token']
                            }
                            return response
                        else:
                            return tresponse
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no product available"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
        else:
            return jsonify({
                "status": "error",
                "code": 100,
                "message": "Hey you getting something wrong"
            })
    
    @staticmethod
    def getpost(which_post, data):
        ourbaseurl = os.path.realpath(Config.BASE_IMG_URL)

        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        if data:
            if which_post == "all":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here...
                    per_page = int(20)
                    page_limit = int(per_page * int(data['v_page_nos']))
                    v_num = int(0)
                    #return v_num

                    sql = "SELECT \
                        posts.id AS post_id, \
                        posts.title AS post_title,\
                        posts.desc AS post_desc,\
                        posts.created_at AS post_date,\
                        posts.img AS post_img,\
                        categories.title AS post_category,\
                        categories.id AS post_category_id\
                        FROM posts\
                        JOIN categories ON posts.cat_id = categories.id\
                        WHERE posts.id >= " + str(v_num) + " AND posts.status = 0 ORDER BY posts.id DESC LIMIT " + str(page_limit)

                    mycursor.execute(sql)
                    myresult = mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        mycursor.execute("SELECT COUNT(*) FROM posts WHERE posts.id >="+str(v_num))
                        rcount = mycursor.fetchone()['COUNT(*)']
                        
                        tresponse = OurAuth.getToken(data["userdata"])
                        if tresponse['status'] == "success":
                        
                            response = {
                                "status": "success",
                                "code": 200,
                                "message": "Post gotten successfully",
                                "data": {
                                    "works": myresult,
                                    "count": rcount,
                                    "last_post": myresult[0]['post_id'],
                                    "base_img_url": ourbaseurl
                                },
                                "utfa": tresponse['token']
                            }
                            return response
                        else:
                            return tresponse
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no post available"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

            if which_post == "recent_work":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here...

                    v_num = int(data['g_from'])

                    #v_num = int("0")
                    #return v_num

                    sql = "SELECT \
                        posts.id AS post_id, \
                        posts.title AS post_title,\
                        posts.desc AS post_desc,\
                        posts.img AS post_img,\
                        categories.title AS post_category, \
                        categories.id AS category_id,\
                        categories.desc AS category_desc\
                        FROM posts\
                        JOIN categories ON posts.cat_id = categories.id\
                        WHERE posts.id > " + str(v_num) + " AND posts.status = 0 ORDER BY posts.id DESC LIMIT 3"

                    ##return sql

                    #sql = "SELECT posts.id, posts.title, posts.desc, posts.img, posts.cat_id FROM posts WHERE posts.status = 0 ORDER BY posts.id"
                    mycursor.execute(sql)
                    myresult = mycursor.fetchmany(6)
                    
                    ##return mycursor.rowcount

                    if mycursor.rowcount > 0:
                        tresponse = OurAuth.getToken(data["userdata"])
                        if tresponse['status'] == "success":
                            response = {
                                "status": "success",
                                "code": 200,
                                "message": "Post gotten successfully",
                                "data": {
                                    "works": myresult,
                                    "base_img_url": ourbaseurl
                                },
                                "utfa": tresponse['token']
                            }
                            return response
                        else:
                            return tresponse['token']
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no post available"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry check your sql"
                    }
                    return response
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

            if which_post == "bycat":
                #return data['g_from']
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    #stopped here...
                    per_page = int(20)
                    page_limit = int(per_page * int(data['v_page_nos']))

                    #return page_limit
                    g_from = int(data['g_from'])
                    d_cat = int(data['cat_id'])

                    sql = "SELECT \
                        posts.id AS post_id, \
                        posts.title AS post_title,\
                        posts.desc AS post_desc,\
                        posts.created_at AS post_date,\
                        posts.img AS post_img,\
                        categories.title AS post_category,\
                        categories.id AS post_category_id\
                        FROM posts\
                        JOIN categories ON posts.cat_id = categories.id\
                        WHERE posts.id >= " + str(g_from) + " AND posts.cat_id = " + str(d_cat) + " AND posts.status = 0 ORDER BY posts.id DESC LIMIT " + str(page_limit)

                    mycursor.execute(sql)
                    myresult = mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        mycursor.execute("SELECT COUNT(*) FROM posts WHERE posts.id >= " + str(g_from) + " AND posts.cat_id = " + str(d_cat) )
                        rcount = mycursor.fetchone()['COUNT(*)']

                        if rcount > 0:
                            tresponse = OurAuth.getToken(data["userdata"])
                            if tresponse['status'] == "success":
                                response = {
                                    "status": "success",
                                    "code": 200,
                                    "message": "Post gotten successfully",
                                    "data":{
                                        "works": myresult,
                                        "count": rcount,
                                        "selected_cat": d_cat,
                                        "last_post": myresult[0]['post_id'],
                                        "base_img_url": ourbaseurl
                                    },
                                    "utfa": tresponse['token']
                                }
                                return response
                            else:
                                return tresponse
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "There is no post to this category yet"
                            }
                            return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry no post available to this category..."
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
        else:
            return jsonify({
                "status": "error",
                "code": 100,
                "message": "Hey you getting something wrong"
            })
    
    @staticmethod
    def get_country_settings(which_s, data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        if which_s == 'all':
            sql = "SELECT \
                ebe_mmamma_ochis.id AS id, \
                ebe_mmamma_ochis.iche AS title, \
                ebe_mmamma_ochis.odiego AS currency, \
                ebe_mmamma_ochis.npkosi AS store,\
                ebe_mmamma_ochis.onyiye AS donations,\
                ebe_mmamma_ochis.temp_npkosi AS temp_store,\
                ebe_mmamma_ochis.temp_npkosi_max AS temp_store_max,\
                dist_fn.student AS student_dist,\
                dist_fn.temp_npkosi AS temp_store_dist,\
                dist_fn.onyiye AS donation_dist,\
                dist_fn.teacher AS teacher_dist,\
                dist_fn.shipping_fee AS shipping_fee_dist,\
                dist_fn.min_tx AS minimum_transaction,\
                dist_fn.max_tx AS maximum_transaction\
                FROM ebe_mmamma_ochis\
                JOIN dist_fn ON ebe_mmamma_ochis.dist_fn_id = dist_fn.id\
                WHERE ebe_mmamma_ochis.id = %s AND ebe_mmamma_ochis.ebe_ono = %s"
        
            val = (data['userdata']['country_id'], '0')

            mycursor.execute(sql, val)
            allresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                for myresult in allresult:
                    myresult['title'] = OurAuth.TextDecode(myresult['title'])
                    myresult['currency'] = OurAuth.TextDecode(myresult['currency'])
                    if myresult['store'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                        myresult['store'] = 0
                    else:
                        myresult['store'] = OurAuth.TextDecode(myresult['store'])
                    if myresult['donations'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                        myresult['donations'] = 0
                    else:
                        myresult['donations'] = OurAuth.TextDecode(myresult['donations'])
                    if myresult['temp_store'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                        myresult['temp_store'] = 0
                    else:
                        myresult['temp_store'] = OurAuth.TextDecode(myresult['temp_store'])
                    myresult['temp_store_max'] = OurAuth.TextDecode(myresult['temp_store_max'])
                    myresult['student_dist'] = OurAuth.TextDecode(myresult['student_dist'])
                    myresult['temp_store_dist'] = OurAuth.TextDecode(myresult['temp_store_dist'])
                    myresult['donation_dist'] = OurAuth.TextDecode(myresult['donation_dist'])
                    myresult['teacher_dist'] = OurAuth.TextDecode(myresult['teacher_dist'])
                    myresult['shipping_fee_dist'] = OurAuth.TextDecode(myresult['shipping_fee_dist'])
                    myresult['minimum_transaction'] = OurAuth.TextDecode(myresult['minimum_transaction'])
                    myresult['maximum_transaction'] = OurAuth.TextDecode(myresult['maximum_transaction'])
                    myresult['times_to_add'] = 2

                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Settings gotten successfully",
                    "settings": myresult
                }
                return response
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Couldn't get the settings",
                    "settings": []
                }
                return response
    
    @staticmethod
    def check_data_exist(field, tb, data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if tb == "nhota_ochis":
            if field == "id":
                try:
                    sql = "SELECT nhota_ochis.id AS id FROM nhota_ochis WHERE nhota_ochis.id = %s AND nhota_ochis.ebe_ono = %s"
                    val = (data, '0')
                    
                    mycursor.execute(sql, val)
                    mycursor.fetchone()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Category Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Category Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
        
        if tb == "odide_ochis":
            if field == "id":
                try:
                    sql = "SELECT odide_ochis.id AS id FROM odide_ochis WHERE odide_ochis.id = %s AND odide_ochis.ebe_ono = %s"
                    val = (data, '0')
                    
                    mycursor.execute(sql, val)
                    mycursor.fetchone()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Note Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Note Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"


        if tb == "mmadu_ochis":
            if field == "id":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT users.id FROM users WHERE users.id = %s AND users.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

            if field == "urole_id":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT users.email FROM users WHERE users.urole_id = %s AND users.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"


            if field == "emmadu":
                try:
                    
                    sql = "SELECT mmadu_ochis.emmadu AS email, mmadu_ochis.id AS id, mmadu_ochis.ahammadu AS fullname, mmadu_ochis.ummadu AS username FROM mmadu_ochis WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.ebe_ono = %s"
                    val = (OurAuth.TextEncode(data.lower()), '0')
                    
                    mycursor.execute(sql, val)
                    u_result = mycursor.fetchone()

                    if mycursor.rowcount > 0:
                        if u_result['email']:
                            u_result['email'] = OurAuth.TextDecode(u_result['email'])
                        if u_result['fullname']:
                            u_result['fullname'] = OurAuth.TextDecode(u_result['fullname'])
                        if u_result['username']:
                            u_result['username'] = OurAuth.TextDecode(u_result['username'])

                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist",
                            "data": u_result
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
            
            if field == "ummadu":
                try:
                    sql = "SELECT mmadu_ochis.ummadu AS username, mmadu_ochis.id FROM mmadu_ochis WHERE mmadu_ochis.ummadu = %s AND mmadu_ochis.ebe_ono = %s"
                    val = (OurAuth.TextEncode(data), '0')
                    
                    mycursor.execute(sql, val)
                    u_result = mycursor.fetchone()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Already exist",
                            "data": u_result
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "User Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
        
        if tb == "uroles":
            try:
                #data_e = 'thankgoduchecyril@gmail.com'
                sql = "SELECT "+tb+"."+field+" FROM "+tb+" WHERE "+tb+"."+field+" = %s AND "+tb+".status = %s"
                val = (data, 0)
                mycursor.execute(sql, val)
                mycursor.fetchall()
                
                if mycursor.rowcount > 0:
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "This Data Already exist"
                    }
                    return response
                else:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Data Doesn't exist"
                    }
                    return response
            except:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Sorry your sql"
                }
                return jsonify(response)
            
            finally:
                Db.closeConnection(mydb, mycursor)
                #return "MySQL connection is closed"

        if tb == "categories":
            if field == "slug":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT categories.slug FROM categories WHERE categories.slug = %s AND categories.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Category already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Category Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

        if tb == "posts":
            if field == "cat_id":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT posts.title FROM posts WHERE posts.cat_id = %s AND posts.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Post Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Post Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return response
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"
        
        if tb == "utokens":
            if field == "user_id":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT utokens.id FROM utokens WHERE utokens.user_id = %s AND utokens.status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Token Active"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Timedout You need to login"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

        if tb == "products":
            if field == "title":
                try:
                    #data_e = 'thankgoduchecyril@gmail.com'
                    sql = "SELECT "+tb+"."+field+" FROM "+tb+" WHERE "+tb+"."+field+" = %s AND "+tb+".status = %s"
                    val = (data, 0)
                    mycursor.execute(sql, val)
                    mycursor.fetchall()
                    
                    if mycursor.rowcount > 0:
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "This Data Already exist"
                        }
                        return response
                    else:
                        response = {
                            "status": "error",
                            "code": 100,
                            "message": "Data Doesn't exist"
                        }
                        return response
                except:
                    response = {
                        "status": "error",
                        "code": 100,
                        "message": "Sorry your sql"
                    }
                    return jsonify(response)
                
                finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

    @staticmethod
    def make_insert():
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        note_type_text = {
            "title": "Text",
            "created_at": str(datetime.now(OurAuth.ourtimezone)),
            "updated_at": str(datetime.now(OurAuth.ourtimezone)),
            "deleted_at": str(datetime.now(OurAuth.ourtimezone))
        }
        note_type_video = {
            "title": "Video",
            "created_at": str(datetime.now(OurAuth.ourtimezone)),
            "updated_at": str(datetime.now(OurAuth.ourtimezone)),
            "deleted_at": str(datetime.now(OurAuth.ourtimezone))
        }
        category_type = {
            "title": "Random",
            "created_at": str(datetime.now(OurAuth.ourtimezone)),
            "updated_at": str(datetime.now(OurAuth.ourtimezone)),
        }
        country = {
            "title": "Ghana",
            "currency": "Ghana Cedis",
            "store": "0",
            "donations": "0",
            "created_at": str(datetime.now(OurAuth.ourtimezone)),
            "updated_at": str(datetime.now(OurAuth.ourtimezone)),
            "deleted_at": str(datetime.now(OurAuth.ourtimezone)),
            "distribution_formula": {
                "in_logistics": "3",
                "out_logistics": "",
                "again and again": "   j "    
            }
        }
        
        

        sql = "SELECT \
            ebe_mmamma_ochis.id AS id, \
            ebe_mmamma_ochis.iche AS title, \
            ebe_mmamma_ochis.odiego AS currency, \
            ebe_mmamma_ochis.npkosi AS store,\
            ebe_mmamma_ochis.onyiye AS donations,\
            ebe_mmamma_ochis.temp_npkosi AS temp_store,\
            ebe_mmamma_ochis.temp_npkosi_max AS temp_store_max,\
            dist_fn.student AS student_dist,\
            dist_fn.temp_npkosi AS temp_store_dist,\
            dist_fn.onyiye AS donation_dist,\
            dist_fn.teacher AS teacher_dist,\
            dist_fn.shipping_fee AS shipping_fee_dist,\
            dist_fn.min_tx AS minimum_transaction,\
            dist_fn.max_tx AS maximum_transaction\
            FROM ebe_mmamma_ochis\
            JOIN dist_fn ON ebe_mmamma_ochis.dist_fn_id = dist_fn.id\
            WHERE ebe_mmamma_ochis.id = %s AND ebe_mmamma_ochis.ebe_ono = %s"
        
        distt = {
            "student": "20",
            "temp_npkosi": "75",
            "onyiye": "50",
            "teacher": "5",
            "shipping_fee": "50",
            "min_tx": "200",
            "max_tx": "5000",
            "created_at": str(datetime.now(OurAuth.ourtimezone)),
            "updated_at": str(datetime.now(OurAuth.ourtimezone)),
        }

        ebe_mmamma = {
            "iche": "Nigeria",
            "npkosi": "0",
            "temp_npkosi": "0",
            "temp_npkosi_max": "40",
            "onyiye": "2000",
            "dist_fn_id": "1"
        }

        omere_nke_ochis = {
            "iche": "DR",
            "created_at": str(datetime.now(OurAuth.ourtimezone)),
            "updated_at": str(datetime.now(OurAuth.ourtimezone)),
        }

        omere_ebeono_ochis = {
            "iche": "Pending",
            "created_at": str(datetime.now(OurAuth.ourtimezone)),
            "updated_at": str(datetime.now(OurAuth.ourtimezone)),
        }
        
        which_insert = "Ebe mma ochis"
        
        
        #sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s WHERE mmadu_ochis.id > 0"
        #val = (OurAuth.TextEncode("2000"), )
        
        #sql = "INSERT INTO omere_ebeono_ochis (omere_ebeono_ochis.iche, omere_ebeono_ochis.created_at) VALUES (%s, %s)"
        #val = (OurAuth.TextEncode(omere_nke_ochis['iche']), OurAuth.TextEncode(omere_nke_ochis['created_at']))
        
        sql = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.iche = %s, ebe_mmamma_ochis.npkosi = %s, ebe_mmamma_ochis.temp_npkosi = %s, ebe_mmamma_ochis.temp_npkosi_max = %s, ebe_mmamma_ochis.onyiye = %s, ebe_mmamma_ochis.dist_fn_id = %s WHERE ebe_mmamma_ochis.id = 7"
        val = (OurAuth.TextEncode(ebe_mmamma['iche']), OurAuth.TextEncode(ebe_mmamma['npkosi']), OurAuth.TextEncode(ebe_mmamma['temp_npkosi']), OurAuth.TextEncode(ebe_mmamma['temp_npkosi_max']), OurAuth.TextEncode(ebe_mmamma['onyiye']), ebe_mmamma['dist_fn_id'])
        
        mycursor.execute(sql, val)
        mydb.commit()
        if mycursor.rowcount > 0 :
            response = {
                "status": "success",
                "code": 200,
                "message": which_insert +" Updated Successfully",
                "utfa": "0"
            }
            return jsonify(response)
        else:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry an Error Occured Contact the Admin"
            }
            return jsonify(response)

    @staticmethod
    def bts_ref():
        bts_response = "BTS-REF"+ str(random.randint(0, 100)) + str(random.randint(2000, 9999))
        return bts_response

    @staticmethod
    def vnt_ref():
        bts_response = "VNT-REF"+ str(random.randint(0, 100)) + str(random.randint(2000, 9999))
        return bts_response

    @staticmethod
    def lbgift_ref():
        bts_response = "GIFT-REF"+ str(random.randint(0, 100)) + str(random.randint(2000, 9999))
        return bts_response

    @staticmethod
    def log_transaction(tx_data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        sql = "INSERT INTO omere_ochis (omere_ochis.omere_nke_id, omere_ochis.omere_egole, omere_ochis.akuko, omere_ochis.mmadu_id, omere_ochis.omere_ebeono_id, omere_ochis.omere_foro, omere_ochis.omere_ref, omere_ochis.created_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        val = (tx_data['tx_type'], OurAuth.TextEncode(tx_data['amount']), OurAuth.TextEncode(tx_data['tx_desc']), tx_data['channel_id'], tx_data['tx_status'],  OurAuth.TextEncode(tx_data['tx_balance']), OurAuth.TextEncode(tx_data['tx_ref']), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))))

        mycursor.execute(sql, val)
        mydb.commit()
        
        if mycursor.rowcount > 0:
            response = {
                "status": "success",
                "code": 200,
                "message": "Transaction Log successful"
            }
            return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry we couldn't log this transaction"
            }
            return response


    @staticmethod
    def get_user_store_value(user_email):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(20)
        page_limit = int(per_page * 1)
        #return v_num

        sql = "SELECT \
            mmadu_ochis.id AS id,\
            mmadu_ochis.npkosi AS store\
            FROM mmadu_ochis\
            WHERE mmadu_ochis.emmadu = %s ORDER BY mmadu_ochis.id DESC LIMIT " + str(page_limit)

        val = (OurAuth.TextEncode(user_email), )
        mycursor.execute(sql, val)
        allresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            if allresult['store']:
                allresult['store'] = OurAuth.TextDecode(allresult['store'])
            else:
                allresult['store'] = '0'

            response = {
                "status": "success",
                "code": 200,
                "store": allresult['store'],
                "id": allresult['id'],
                "message": "Store value gotten."
            }
            return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry, couldn't get your store value. Please login to try again."
            }
            return response

    @staticmethod
    def make_transfer(data):
        mydb = Db.dbfun()
        
        tr_from = data['userdata']['email']
        tr_to = data['send_email']
        
        stvalue_response = SendServices.get_user_store_value(tr_from)
        if stvalue_response['status'] == 'success':
            stvalue = stvalue_response['store']
            
            tx_response = SendServices.get_country_settings('all', data)
            if tx_response['status'] == 'success':
                if data['send_email'] != data['userdata']['email']:
                    cal_total_pcs = float(data['send_pcs']) + float(tx_response['settings']['shipping_fee_dist'])
            
                    if float(stvalue) >= float(cal_total_pcs):
                        checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['send_email'])
                        if checkresponse['status'] == "success":
                            
                            #debit user
                            new_sender_balance = float(stvalue) - float(cal_total_pcs)

                            t_mycursor = mydb.cursor(dictionary=True)
                            t_tr_sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = 1"
                            t_tr_val = (OurAuth.TextEncode(str(new_sender_balance)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'])
                            t_mycursor.execute(t_tr_sql, t_tr_val)
                            mydb.commit()
                            if t_mycursor.rowcount > 0 :
                                tx_ref = SendServices.bts_ref()
                                db_tx_data = {
                                    "tx_type": "2",
                                    "channel_id": data['userdata']['id'],
                                    "amount": cal_total_pcs,
                                    "tx_desc": "BT:"+str(data['send_pcs'])+"pcs transfered to "+str(data['send_email']),
                                    "tx_ref": tx_ref,
                                    "tx_balance": new_sender_balance,
                                    "tx_status": '1'
                                }
                                log_response = SendServices.log_transaction(db_tx_data)
                                if log_response['status'] == "success":
                                    
                                    receiver_stvalue_response = SendServices.get_user_store_value(tr_to)
                                    if receiver_stvalue_response['status'] == 'success':
                                        r_stvalue = receiver_stvalue_response['store']
                                        
                                        #credit other user
                                        new_receiver_balance = float(r_stvalue) + float(data['send_pcs'])
                                        tt_mycursor = mydb.cursor(dictionary=True)
                                        tt_tr_sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
                                        tt_tr_val = (OurAuth.TextEncode(str(new_receiver_balance)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(str(tr_to)))
                                        
                                        tt_mycursor.execute(tt_tr_sql, tt_tr_val)
                                        mydb.commit()
                                        if tt_mycursor.rowcount > 0 :
                                            cr_tx_data = {
                                                "tx_type": "1",
                                                "channel_id": receiver_stvalue_response['id'],
                                                "amount": data['send_pcs'],
                                                "tx_desc": "RB:"+str(data['send_pcs'])+"pcs received from "+str(tr_from),
                                                "tx_ref": tx_ref,
                                                "tx_balance": new_receiver_balance,
                                                "tx_status": '1'
                                            }
                                            log_cr_response = SendServices.log_transaction(cr_tx_data)
                                            if log_cr_response['status'] == "success":
                                                
                                                checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', str(Config.HQ_ADDRESS))
                                                if checkresponse['status'] == "success":
                                                    #old_store_value = float(tx_response['settings']['store'])
                                                    shipping_fee = float(tx_response['settings']['shipping_fee_dist'])
                                                    hq_store_response = SendServices.get_user_store_value(str(Config.HQ_ADDRESS))
                                                    
                                                    if hq_store_response['status'] == 'success':
                                                        hq_store_value = hq_store_response['store']
                                                        hq_new_store_value = float(hq_store_value) + float(shipping_fee)

                                                        hq_mycursor = mydb.cursor(dictionary=True)
                                                        hq_tr_sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
                                                        hq_tr_val = (OurAuth.TextEncode(str(hq_new_store_value)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(str(Config.HQ_ADDRESS)))
                                                        
                                                        hq_mycursor.execute(hq_tr_sql, hq_tr_val)
                                                        mydb.commit()
                                                        if hq_mycursor.rowcount > 0 :
                                                            cr_tx_data = {
                                                                "tx_type": "1",
                                                                "channel_id": hq_store_response['id'],
                                                                "amount": shipping_fee,
                                                                "tx_desc": "SF:"+"Received shipping fee for "+tx_ref+ " transaction",
                                                                "tx_ref": tx_ref,
                                                                "tx_balance": hq_new_store_value,
                                                                "tx_status": '1'
                                                            }
                                                            llog_cr_response = SendServices.log_transaction(cr_tx_data)
                                                            if llog_cr_response['status'] == "success":
                                                            
                                                                response = {
                                                                    "status": "success",
                                                                    "code": 200,
                                                                    "message": "Transfer successful."
                                                                }
                                                                return response
                                                            else:
                                                                response = {
                                                                    "status": "error",
                                                                    "code": 400,
                                                                    "message": "We couldn't contact the HQ shipping log."
                                                                }
                                                                return response
                                                        else:
                                                            
                                                            response = {
                                                                "status": "error",
                                                                "code": 400,
                                                                "message": "We couldn't update the shipping store."
                                                            }
                                                            return response
                                                    else:
                                                        response = {
                                                            "status": "error",
                                                            "code": 400,
                                                            "message": "We couldn't get the bread in shipping store."
                                                        }
                                                        return response
                                                else:
                                                    response = {
                                                        "status": "error",
                                                        "code": 400,
                                                        "message": "We couldn't contact the HQ store address ."
                                                    }
                                                    return response
                                            else:
                                                response = {
                                                    "status": "error",
                                                    "code": 400,
                                                    "message": "Couldn't log that we have added bread to the receiver store. Please contact our nearest office."
                                                }
                                                return response    

                                        else:
                                            response = {
                                                "status": "error",
                                                "code": 400,
                                                "message": "Couldn't add bread to receiver's store. Please try again."
                                            }
                                            return response
                                    else:
                                        response = {
                                            "status": "error",
                                            "code": 400,
                                            "message": "Sorry we couldn't locate the receiver's store. Try again."
                                        }
                                        return response
                                else:
                                    return log_response
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Couldn't remove from your store. Try again."
                                }
                                return response
                        else:
                            response = {
                                "status": "warning",
                                "code": 400,
                                "message": "Sorry, the RECEIVER IS NOT REGISTERED on this platform, please kindly tell them to register if they want to receive bread from you."
                            }
                            return response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry, you don't have sufficient bread."
                        }
                        return response
                else:
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Sorry, you can only send to someone else, NOT TO YOURSELF.",
                        "data": {
                        }
                    }
                    return response
            else:
                response = {
                    "status": "error",
                    "code": 401,
                    "message": "Sorry, please for now we can get your country please login to try again.",
                    "data": {
                    }
                }
                return response
        else:
            return stvalue_response

    @staticmethod
    def update(string, data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if string == "deleteuser":
            sql = "UPDATE users SET users.status = 1 WHERE users.id = %s"
            val = (data['user_id'], )
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                tresponse = OurAuth.getToken(data["userdata"])
                if tresponse['status'] == "success":
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "User Deleted Successfully",
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    return jsonify(tresponse)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "User not deleted successfully"
                }
                return jsonify(response)
        

        if string == "deletepost":
            sql = "UPDATE posts SET posts.status = 1 WHERE posts.id = %s"
            val = (data['post_id'], )
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                tresponse = OurAuth.getToken(data["userdata"])
                if tresponse['status'] == "success":
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Post Deleted Successfully",
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    return jsonify(tresponse)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Post not deleted successfully"
                }
                return jsonify(response)
        
        if string == "deleteproduct":
            sql = "UPDATE products SET products.status = 1 WHERE products.id = %s"
            val = (data['product_id'], )
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                tresponse = OurAuth.getToken(data["userdata"])
                if tresponse['status'] == "success":
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Product Deleted Successfully",
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    return jsonify(tresponse)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Product not deleted"
                }
                return jsonify(response)

        if string == "updatepost":
            sql = "UPDATE posts SET posts.title = %s, posts.desc = %s, posts.img = %s, posts.cat_id = %s, posts.status = %s WHERE posts.id = %s"
            val = (data['title'], data['desc'], data['img'], data['cat_id'], 0, data['post_id'])
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                tresponse = OurAuth.getToken(data["userdata"])
                if tresponse['status'] == "success":
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Post Updated Successfully",
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    return jsonify(tresponse)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Post not updated though, seems no changes was made. Ensure you are doing the right thing"
                }
                return jsonify(response)
        
        if string == "updateproduct":
            if 'file' in data :
                ourimg = data['file']
                #dir_path = os.path.dirname(os.path.realpath(SendServices.IMG_UPLOAD_FOLDER))
                #return ourimg.filename
                if ourimg and SendServices.allowed_img(ourimg.filename):
                    
                    ourimg.filename = SendServices.rename_file(ourimg.filename)
                    filename = secure_filename(ourimg.filename)
                    ourimg.save(os.path.join(Config.IMG_UPLOAD_PATH, filename))

                    sql = "UPDATE products SET products.title = %s, products.desc = %s, products.buy_price = %s, products.win_price = %s, products.img = %s, products.cat_id = %s, products.status = %s WHERE products.id = %s"
                    val = (data['title'], data['desc'], data['buy_price'], data['win_price'], str(ourimg.filename), data['cat_id'], 0, data['product_id'])
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry, please ensure you are uploading a valid image"
                    }
                    return jsonify(response)
            else:
                
                sql = "UPDATE products SET products.title = %s, products.desc = %s, products.buy_price = %s, products.win_price = %s, products.cat_id = %s, products.status = %s WHERE products.id = %s"
                val = (data['title'], data['desc'], data['buy_price'], data['win_price'], data['cat_id'], 0, data['product_id'])
                    
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                tresponse = OurAuth.getToken(data["userdata"])
                if tresponse['status'] == "success":
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Product Updated Successfully",
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    return jsonify(tresponse)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Product not updated though, seems no changes was made. Ensure you are doing the right thing"
                }
                return jsonify(response)
    
        if string == "updateuser":
            checkresponse = SendServices.check_data_exist('id', 'users', data['user_id'])

            if checkresponse['code'] == 200:
                val_len = len(str(data['user_pwd']))
                
                if val_len > 5:
                    pp_hash = SendServices.hash_pp(data['user_pwd'])
                    sql = "UPDATE users SET users.fullname = %s, users.email = %s, users.phone = %s, users.key_value = %s, users.salt = %s, users.urole_id = %s WHERE users.id = %s AND users.status = %s"
                    val = (data['user_fullname'], data['user_email'], data['user_phone'], pp_hash['keyvalue'], pp_hash['salt'], data['user_role_id'], data['user_id'], 0)
                else:
                    sql = "UPDATE users SET users.fullname = %s, users.email = %s, users.phone = %s, users.urole_id = %s WHERE users.id = %s AND users.status = %s"
                    val = (data['user_fullname'], data['user_email'], data['user_phone'], data['user_role_id'], data['user_id'], 0)
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "User Updated Successfully",
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)    
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "User didn't update, seems no changes was made. Ensure you are doing the right thing"
                    }
                    return jsonify(response)

        if string == "updateprofile":
            checkresponse = SendServices.check_data_exist('id', 'users', data["userdata"]["id"])

            if checkresponse['code'] == 200:

                sql = "UPDATE users SET users.fullname = %s, users.email = %s, users.phone = %s WHERE users.id = %s AND users.status = %s"
                val = (data['fullname'], data['email'], data['phone'], data["userdata"]["id"], 0)
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Profile Updated Successfully",
                            "data": {
                                "user": {
                                    "email": data["email"],
                                    "fullname": data["fullname"],
                                    "id": data["id"],
                                    "phone": data["phone"],
                                    "role": data["role"],
                                    "role_id": data["userdata"]["role_id"]
                                }
                            },
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)    
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Profile didn't update, seems no changes was made. Ensure you are doing the right thing"
                    }
                    return jsonify(response)
            
        if string == "updatecat":
            sql = "UPDATE categories SET categories.title = %s, categories.desc = %s, categories.slug = %s, categories.parent_cat_id = %s, categories.status = %s WHERE categories.id = %s"
            val = (data['title'], data['desc'], data['slug'], data['parent_cat_id'], 0, data['id'])
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                tresponse = OurAuth.getToken(data["userdata"])
                if tresponse['status'] == "success":
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Category Updated Successfully",
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    return jsonify(tresponse)    
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Category didn't updated, seems no changes was made. Ensure you are doing the right thing"
                }
                return jsonify(response)
        
        if string == "updaterole":
            checkresponse = SendServices.check_data_exist('title', 'uroles', data['role_title'])
            if checkresponse['status'] == "success":
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry Role already exist"
                }
                return jsonify(response)
            else:
                sql = "UPDATE uroles SET uroles.title = %s, uroles.upermission_id = %s WHERE uroles.id = %s"
                val = (data['role_title'], data['role_permission_id'], data['role_id'])
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Role Updated Successfully",
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Role didn't update, seems no changes was made. Ensure you are doing the right thing"
                    }
                    return jsonify(response)
        
        if string == "deletecat":
            cresponse = SendServices.check_data_exist('cat_id', 'posts', data['id'])
            if cresponse['status'] == "success":
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry this category already has a post, and can't be deleted"
                }
                return jsonify(response)
            else:
                sql = "UPDATE categories SET categories.status = 1 WHERE categories.id = %s"
                val = (data['id'], )
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Category Deleted Successfully",
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Category not deleted successfully"
                    }
                    return jsonify(response)

        if string == "deleterole":
            cresponse = SendServices.check_data_exist('urole_id', 'users', data['role_id'])
            #return jsonify(cresponse)

            if cresponse['status'] == "success":
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry this category already has a post, and can't be deleted"
                }
                return jsonify(response)
            else:
                sql = "UPDATE uroles SET uroles.status = 1 WHERE uroles.id = %s"
                val = (data['role_id'], )
                
                mycursor.execute(sql, val)
                mydb.commit()
                if mycursor.rowcount > 0 :
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Role Deleted Successfully",
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)        
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Role not deleted"
                    }
                    return jsonify(response)

        if string == "savesettings":
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)
            
            sql = "UPDATE settings SET settings.app_name = %s, settings.app_motto = %s, settings.app_why_title = %s, settings.app_why_desc = %s WHERE settings.id = %s"
            val = (data['bus_name'], data['bus_motto'], data['bus_why_title'], data['bus_why_desc'], 1)
            
            mycursor.execute(sql, val)

            mydb.commit()
            if mycursor.rowcount > 0 :
                response = {
                    "status": "success",
                    "code": 200,  
                }
                return jsonify(response)
                
            else:
                response = {
                    "status": "error",
                    "code": 400
                }
                return jsonify(response)

    @staticmethod
    #data = {"g_from": "0"}
    def select(data):
        string = data['string']
        
        if string == "/":
            r_data = SendServices.getpost("recent_work", data)
            return jsonify(r_data)
        
        if string == "ourwork":
            r_data = SendServices.getpost("all", data)
            return jsonify(r_data)
        
        if string == "allproducts":
            r_data = SendServices.getproduct("allproducts", data)
            return jsonify(r_data)
        
        if string == "ourworkbycat":
            the_cat = int(data['cat_id'])
            if the_cat == 0:
                r_data = SendServices.getpost("all", data)
                return jsonify(r_data)
            else:
                r_data = SendServices.getpost("bycat", data)
                return jsonify(r_data)
        
        if string == "usersbyrole":
            the_role = int(data['role_id'])
            if the_role == 0:
                return SendServices.getusers("all", data)
            else:
                return SendServices.getusers("byrole", data)
        

        if string == "getroles":
            return SendServices.getroles()
        
        if string == "getusers":
            return SendServices.getusers("all", data)

        if string == "getcat":
            return SendServices.getcategories()
        
        if string == "getperms":
            return SendServices.getpermissions()

        if string == "getparentcat":
           return SendServices.getcategories()

        if string == "adminlogin":
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            try:
                #data_e = 'thankgoduchecyril@gmail.com'
                #stopped here...
                #return v_num
                ##u_pp = SendServices.verify_pp(data['pword'])
                #return jsonify(str(u_pp))
                pp_hash = SendServices.hash_pp(data['pword'])

                #return jsonify(pp_hash)
                sql = "SELECT \
                    users.id AS id, \
                    users.fullname AS fullname,\
                    users.email AS email,\
                    users.phone AS phone,\
                    users.img AS img,\
                    users.confirmed AS activeaccount,\
                    uroles.title AS role,\
                    uroles.id AS role_id\
                    FROM users\
                    JOIN uroles ON users.urole_id = uroles.id\
                    WHERE users.email = %s AND users.key_value = %s AND users.salt = %s AND users.status = %s" 
                
                val = (data['uname'], pp_hash['keyvalue'], pp_hash['salt'], 0)
                mycursor.execute(sql, val)
                myresult = mycursor.fetchone()
                
                if mycursor.rowcount > 0:
                    if myresult['role_id'] == OurAuth.admin():
                        if myresult['activeaccount'] > 0:
                            payload = {
                                'user_id': myresult['id'],
                                'email': myresult['email'],
                                'role_id': myresult['role_id']
                            }
                            
                            #return jsonify(payload)

                            #return str(len(OurAuth.createToken(payload)))
                            tresponse = OurAuth.createToken(payload)
                            #return tresponse
                            if tresponse['status'] == "success":
                                #return jsonify
                                response = {
                                    "status": "success",
                                    "code": 200,
                                    "message": "Login successful",
                                    "data": {
                                        "user": myresult
                                    },
                                    "utfa": tresponse['token']
                                }
                                return jsonify(response)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Sorry, something is wrong with your authentication, Please contact the admin"
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Sorry your account isn't activated yet, kindly check your email for an activation link to activate your account"
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry you are not allowed to access here, Please ensure you are doing the right thing, or contact the admin"
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry ensure that your email and password are correct"
                    }
                    return jsonify(response)
            except:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Please contact the Admin to rectify this error"
                }
                return jsonify(response)
            
            finally:
                Db.closeConnection(mydb, mycursor)
                #return "MySQL connection is closed"

        if string == "userlogin":
            
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)
            
            #return jsonify(data)
            try:
                #data_e = 'thankgoduchecyril@gmail.com'
                #stopped here...
                #return v_num
                ##u_pp = SendServices.verify_pp(data['pword'])
                #return jsonify(str(u_pp))
                pp_hash = SendServices.hash_pp(data['pword'])

                #return jsonify(pp_hash)
                sql = "SELECT \
                    users.id AS id, \
                    users.fullname AS fullname,\
                    users.email AS email,\
                    users.phone AS phone,\
                    users.img AS img,\
                    users.confirmed AS activeaccount,\
                    uroles.title AS role,\
                    uroles.id AS role_id\
                    FROM users\
                    JOIN uroles ON users.urole_id = uroles.id\
                    WHERE users.email = %s AND users.key_value = %s AND users.salt = %s AND users.status = %s" 
                
                val = (data['uname'], pp_hash['keyvalue'], pp_hash['salt'], 0)
                mycursor.execute(sql, val)
                myresult = mycursor.fetchone()
                #return jsonify(myresult)
                if mycursor.rowcount > 0:
                    if myresult['activeaccount'] > 0:
                        if myresult['role_id'] == OurAuth.user():
                            payload = {
                                'user_id': myresult['id'],
                                'email': myresult['email'],
                                'role_id': myresult['role_id']
                            }
                            tresponse = OurAuth.createToken(payload)
                            #return tresponse
                            if tresponse['status'] == "success":
                                #return jsonify
                                response = {
                                    "status": "success",
                                    "code": 200,
                                    "message": "Login successful",
                                    "data": {
                                        "user": myresult
                                    },
                                    "utfa": tresponse['token']
                                }
                                return jsonify(response)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Sorry, something is wrong with your authentication, Please contact the admin"
                                }
                                return jsonify(response)    
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Sorry you are not allowed to access here, Please ensure you are doing the right thing, or contact the admin"
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry your account isn't activated yet, kindly check your email for an activation link to activate your account"
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry ensure that your email and password are correct"
                    }
                    return jsonify(response)
            except:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Please contact the Admin to rectify this error"
                }
                return jsonify(response)
            
            finally:
                Db.closeConnection(mydb, mycursor)
                #return "MySQL connection is closed"
        
        if string == "getgames":
            r_data = SendServices.getproduct("getgames", data)
            return jsonify(r_data)
        
        if string == "getgamesbycat":
            the_cat = int(data['cat_id'])
            if the_cat in (0, SendServices.getCatID('Games')):
                r_data = SendServices.getproduct("getgames", data)
                return jsonify(r_data)
            else:
                r_data = SendServices.getproduct("getgamesbycat", data)
                return jsonify(r_data)

        if string == "getsettings":
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)

            sql = "SELECT settings.app_name, settings.app_motto, settings.app_why_title, settings.app_why_desc FROM settings WHERE settings.id = %s"
            val = ("1",)
            mycursor.execute(sql, val)
            myresult = mycursor.fetchall()

            if mycursor.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "data": {
                        "bus_name": myresult[0]['app_name'],
                        "bus_motto": myresult[0]['app_motto'],
                        "bus_why_title": myresult[0]['app_why_title'],
                        "bus_why_desc": myresult[0]['app_why_desc']
                    }
                }
                return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Operation Successful, but no parent category available yet, you can create one",                        
                    "data": myresult

                }
                return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
        
        if string == "mydashboard":
            mydb = Db.dbfun()
            mycursor = mydb.cursor(dictionary=True)
            #return data['g_from']
            try:
                mycursor.execute("SELECT COUNT(*) FROM posts WHERE posts.status = 0")
                pcount = mycursor.fetchone()['COUNT(*)']

                mycursor.execute("SELECT COUNT(*) FROM categories WHERE categories.status = 0")
                ccount = mycursor.fetchone()['COUNT(*)']

                mycursor.execute("SELECT COUNT(*) FROM users WHERE users.status = 0")
                ucount = mycursor.fetchone()['COUNT(*)']

                if pcount > 0:
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Post gotten successfully",
                            "data":{
                                "post_count": pcount,
                                "cat_count": ccount,
                                "user_count": ucount
                            },
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)
                else:
                    tresponse = OurAuth.getToken(data["userdata"])
                    if tresponse['status'] == "success":
                        response = {
                            "status": "success",
                            "code": 200,
                            "message": "Post gotten successfully",
                            "data":{
                                "post_count": pcount,
                                "cat_count": ccount,
                                "user_count": ucount
                            },
                            "utfa": tresponse['token']
                        }
                        return jsonify(response)
                    else:
                        return jsonify(tresponse)
            except:
                response = {
                    "status": "error",
                    "code": 100,
                    "message": "Sorry your sqllllm"
                }
                return jsonify(response)
            
            finally:
                    Db.closeConnection(mydb, mycursor)
                    #return "MySQL connection is closed"

    #Learnbread begin
    @staticmethod
    def reg_user(data):
        checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['email'])
        if checkresponse['code'] == 200:
            response = {
                "code": 400,
                "status": "error",
                "message": "Sorry this email already exist. If this is your first time, please check your email for an Activation link to activate your account"
            }
            return jsonify(response)
        else:
            if len(str(data['fullname'])) > 4 and len(str(data['fullname'])) < 40:
                if int(data['country']) > 0:
                    if len(str(data['email'])) > 10 and len(str(data['email'])) < 40:
                        if str(data['email']).split('@')[1] == 'gmail.com':
                            data['email'] = str(data['email']).lower()
                            if len(str(data['pword'])) > 5 and len(str(data['pword'])) < 40:
                                if data['iagree'] == 'true':
                                    payload = {
                                        'email': data['email'],
                                        'confirmed': False,
                                        'country': data['country']
                                    }
                                    data['confirm_token'] = OurAuth.AuthEncode(payload)

                                    if OurEmailSender.confirm_registration(data):
                                        pp_hash = SendServices.hash_pp(data['pword'])
                                        
                                        mydb = Db.dbfun()
                                        mycursor = mydb.cursor(dictionary=True)
                                        
                                        data['store'] = "0"
                                        sql = "INSERT INTO mmadu_ochis (mmadu_ochis.ahammadu, mmadu_ochis.emmadu, mmadu_ochis.ummadu, mmadu_ochis.akuko, mmadu_ochis.ebe_id, mmadu_ochis.mkwere, mmadu_ochis.npkosi, mmadu_ochis.nkoshi_mmadu, mmadu_ochis.nnu_mmadu, mmadu_ochis.anyanatago_tmmadu, mmadu_ochis.ibuonye, mmadu_ochis.ebe_ono, mmadu_ochis.created_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                                        #data['role'] = OurAuth.user()
                                        val = (OurAuth.TextEncode(data['fullname']), OurAuth.TextEncode(data['email']), OurAuth.TextEncode(data['email']), OurAuth.TextEncode("Welcome to my channel, subscribe to learn more"), data['country'], OurAuth.TextEncode(data['iagree']), OurAuth.TextEncode(data['store']) ,pp_hash['keyvalue'], pp_hash['salt'], data['confirm_token'], OurAuth.TextEncode('User'), 0, OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))))

                                        mycursor.execute(sql, val)
                                        mydb.commit()
                                        if mycursor.rowcount > 0 :
                                            mycursor1 = mydb.cursor(dictionary=True)
                                            sql_last1 = "SELECT temp_mmadu_ole AS temp_reg FROM ebe_mmamma_ochis WHERE ebe_mmamma_ochis.id = %s AND ebe_mmamma_ochis.ebe_ono = %s"
                                            val_last1 = (str(data['country']), '0')

                                            mycursor1.execute(sql_last1, val_last1)
                                            myresult1 = mycursor1.fetchone()
                                            
                                            if mycursor1.rowcount > 0:
                                                if myresult1['temp_reg'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                                    myresult1['temp_reg'] = 0
                                                else:
                                                    myresult1['temp_reg'] = OurAuth.TextDecode(myresult1['temp_reg'])
                                                
                                                new_nos = int(myresult1['temp_reg'])
                                                up_new_nos = new_nos + 1
                                                
                                                up_mycursor = mydb.cursor(dictionary=True)
                                                sqlnew = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.temp_mmadu_ole = %s WHERE ebe_mmamma_ochis.id = %s AND ebe_mmamma_ochis.ebe_ono = %s"
                                                valnew = (OurAuth.TextEncode(str(up_new_nos)), data['country'], '0')
                                                
                                                up_mycursor.execute(sqlnew, valnew)
                                                mydb.commit()
                                                if up_mycursor.rowcount > 0 :
                                                    response = {
                                                        "status": "success",
                                                        "code": 200,
                                                        "message": "To activate your account. Check your email to click the Activation link sent to you.",
                                                    }
                                                    return jsonify(response)
                                                else:
                                                    response = {
                                                        "status": "error",
                                                        "code": 400,
                                                        "message": "Sorry, we couldn't update the country temp_count"
                                                    }
                                                    return jsonify(response)
                                        else:
                                            response = {
                                                "status": "error",
                                                "code": 400,
                                                "message": "Registration NOT successful"
                                            }
                                            return jsonify(response)
                                    else:
                                        response = {
                                            "status": "error",
                                            "code": 400,
                                            "message": "Sorry, having issue sending mail to your email. Please click the create button again"
                                        }
                                        return jsonify(response)
                                else:
                                    response = {
                                        "status": "warning",
                                        "code": 400,
                                        "message": "Please read our policies, terms and conditions, then check the box if you agree to proceed."
                                    }
                                    return jsonify(response)
                            else:
                                response = {
                                    "status": "warning",
                                    "code": 400,
                                    "message": "Sorry, your password should be minimum of 6 characters and valid"
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "warning",
                                "code": 400,
                                "message": "Please we only accept Gmail address"
                            }
                            return jsonify(response)
                    else:        
                        response = {
                            "status": "warning",
                            "code": 400,
                            "message": "Please ensure your email is valid"
                        }
                        return jsonify(response)
                else:    
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Please select a country"
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "warning",
                    "code": 400,
                    "message": "Please provide a valid Fullname"
                }
                return jsonify(response)
    
    @staticmethod
    def reset_pwd(data):
        if len(str(data['email'])) > 10 and len(str(data['email'])) < 40:
            if str(data['email']).split('@')[1] == 'gmail.com':
                checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['email'])
                if checkresponse['code'] == 200:
                    payload = {
                        'email': data['email'],
                        'reset_status': True,
                        'time': str(datetime.now(OurAuth.ourtimezone))
                    }
                    data['reset_token'] = OurAuth.AuthEncode(payload)
                    
                    if OurEmailSender.reset_user_pwd(data):
                        
                        mydb = Db.dbfun()
                        mycursor = mydb.cursor(dictionary=True)

                        sql = "UPDATE mmadu_ochis SET mmadu_ochis.mezie_npkosi_tmmadu = %s, mmadu_ochis.mezie_npkosi_status = 1 WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.mezie_npkosi_status = 0"
                        val = (data['reset_token'], OurAuth.TextEncode(data['email']), '1')
                        
                        mycursor.execute(sql, val)
                        mydb.commit()

                        if mycursor.rowcount > 0 :
                            response = {
                                "status": "success",
                                "code": 200,
                                "message": "Please Check your email for a verification link. Click on link from your mail to change password.",
                            }
                            return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Sorry, seems you have already made a request to reset your password before, please check your email for the verification mail sent to you previously."
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry, having issue sending you an email. Please check your network and click the button again"
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry seems you don't have an account yet, kindly create an account"
                    }
                    return jsonify(response)

            else:
                response = {
                    "status": "warning",
                    "code": 400,
                    "message": "Please we only accept Gmail address"
                }
                return jsonify(response)
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Please ensure your email is correct"
            }
            return jsonify(response)

    @staticmethod
    def verify_pword(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        #return jsonify(data)
        #try:
        pp_hash = SendServices.hash_pp(data['pword'])
        data['uname'] = str(data['uname']).lower()

        #return jsonify(pp_hash)
        sql = "SELECT \
            mmadu_ochis.id AS id, \
            mmadu_ochis.ahammadu AS fullname,\
            mmadu_ochis.emmadu AS email,\
            mmadu_ochis.ummadu AS username,\
            mmadu_ochis.ibuonye AS role,\
            mmadu_ochis.pmmadu AS phone,\
            mmadu_ochis.npkosi AS store,\
            mmadu_ochis.akuko AS bio,\
            mmadu_ochis.zeemu AS img,\
            mmadu_ochis.anyanatago AS activeaccount,\
            ebe_mmamma_ochis.iche AS country,\
            ebe_mmamma_ochis.id AS country_id,\
            ebe_mmamma_ochis.odiego AS country_currency\
            FROM mmadu_ochis\
            JOIN ebe_mmamma_ochis ON mmadu_ochis.ebe_id = ebe_mmamma_ochis.id\
            WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.nkoshi_mmadu = %s AND mmadu_ochis.nnu_mmadu = %s AND mmadu_ochis.ebe_ono = %s" 
        
        val = (OurAuth.TextEncode(data['uname']), pp_hash['keyvalue'], pp_hash['salt'], 0)
        mycursor.execute(sql, val)
        myresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            myresult['fullname'] = OurAuth.TextDecode(myresult['fullname'])
            myresult['email'] = OurAuth.TextDecode(myresult['email'])
            myresult['username'] = OurAuth.TextDecode(myresult['username'])
            myresult['bio'] = OurAuth.TextDecode(myresult['bio'])
            myresult['country'] = OurAuth.TextDecode(myresult['country'])
            myresult['role'] = OurAuth.TextDecode(myresult['role'])
            myresult['store'] = OurAuth.TextDecode(myresult['store'])
            myresult['role_id'] = "1"
            myresult['country_currency'] = OurAuth.TextDecode(myresult['country_currency'])
            if myresult['phone']:
                myresult['phone'] = OurAuth.TextDecode(myresult['phone'])
            
            if myresult['img'] in ('0', 0, 'NULL', 'null', None, 'None'):
                myresult['img'] = "0"
            else:
                myresult['img'] = OurAuth.TextDecode(myresult['img'])
            
            if int(myresult['activeaccount']) == OurAuth.account_is_active():
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Yes verified.",
                    "data": {
                        "user": myresult
                    }
                }
                return response
            else:
                response = {
                    "status": "info",
                    "code": 400,
                    "message": "ACTIVATE YOUR ACCOUNT. Sorry your account isn't activated yet, kindly check your email for an activation link to activate your account before you can continue."
                }
                return response
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Sorry your password is not correct"
            }
            return response
    
    @staticmethod
    def login_user(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        #return jsonify(data)
        #try:
        pp_hash = SendServices.hash_pp(data['pword'])
        data['uname'] = str(data['uname']).lower()

        #return jsonify(pp_hash)
        sql = "SELECT \
            mmadu_ochis.id AS id, \
            mmadu_ochis.ahammadu AS fullname,\
            mmadu_ochis.emmadu AS email,\
            mmadu_ochis.ummadu AS username,\
            mmadu_ochis.ibuonye AS role,\
            mmadu_ochis.pmmadu AS phone,\
            mmadu_ochis.npkosi AS store,\
            mmadu_ochis.akuko AS bio,\
            mmadu_ochis.zeemu AS img,\
            mmadu_ochis.anyanatago AS activeaccount,\
            ebe_mmamma_ochis.iche AS country,\
            ebe_mmamma_ochis.id AS country_id,\
            ebe_mmamma_ochis.odiego AS country_currency\
            FROM mmadu_ochis\
            JOIN ebe_mmamma_ochis ON mmadu_ochis.ebe_id = ebe_mmamma_ochis.id\
            WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.nkoshi_mmadu = %s AND mmadu_ochis.nnu_mmadu = %s AND mmadu_ochis.ebe_ono = %s" 
        
        val = (OurAuth.TextEncode(data['uname']), pp_hash['keyvalue'], pp_hash['salt'], 0)
        mycursor.execute(sql, val)
        myresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            myresult['fullname'] = OurAuth.TextDecode(myresult['fullname'])
            myresult['email'] = OurAuth.TextDecode(myresult['email'])
            myresult['username'] = OurAuth.TextDecode(myresult['username'])
            myresult['bio'] = OurAuth.TextDecode(myresult['bio'])
            myresult['country'] = OurAuth.TextDecode(myresult['country'])
            myresult['role'] = OurAuth.TextDecode(myresult['role'])
            myresult['store'] = OurAuth.TextDecode(myresult['store'])
            myresult['role_id'] = "1"
            myresult['country_currency'] = OurAuth.TextDecode(myresult['country_currency'])
            if myresult['phone']:
                myresult['phone'] = OurAuth.TextDecode(myresult['phone'])
            
            if myresult['img'] in ('0', 0, 'NULL', 'null', None, 'None'):
                myresult['img'] = "0"
            else:
                myresult['img'] = OurAuth.TextDecode(myresult['img'])
            
            if int(myresult['activeaccount']) == OurAuth.account_is_active():
                payload = {
                    'user_id': myresult['id'],
                    'role_id': myresult['role_id'],
                    'email': myresult['email']
                }
                tresponse = OurAuth.createToken(payload)
                #return tresponse
                if tresponse['status'] == "success":
                    #return jsonify
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Hi "+ myresult['fullname'].split(' ')[0] + ", you are welcome home.",
                        "data": {
                            "user": myresult
                        },
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry, something is wrong with your authentication, Please contact the admin"
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "info",
                    "code": 400,
                    "message": "ACTIVATE YOUR ACCOUNT. Sorry your account isn't activated yet, kindly check your email for an activation link to activate your account before you can continue."
                }
                return jsonify(response)
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry ensure that your email and password are correct"
            }
            return jsonify(response)
        
    @staticmethod
    def get_user_updated_data(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        sql = "SELECT \
            mmadu_ochis.id AS id, \
            mmadu_ochis.ahammadu AS fullname,\
            mmadu_ochis.emmadu AS email,\
            mmadu_ochis.ummadu AS username,\
            mmadu_ochis.ibuonye AS role,\
            mmadu_ochis.pmmadu AS phone,\
            mmadu_ochis.npkosi AS store,\
            mmadu_ochis.akuko AS bio,\
            mmadu_ochis.zeemu AS img,\
            mmadu_ochis.anyanatago AS activeaccount,\
            ebe_mmamma_ochis.iche AS country,\
            ebe_mmamma_ochis.id AS country_id,\
            ebe_mmamma_ochis.odiego AS country_currency\
            FROM mmadu_ochis\
            JOIN ebe_mmamma_ochis ON mmadu_ochis.ebe_id = ebe_mmamma_ochis.id\
            WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s" 
        
        val = (OurAuth.TextEncode(data['userdata']['email']), data['userdata']['id'], 0)
        mycursor.execute(sql, val)
        myresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            myresult['fullname'] = OurAuth.TextDecode(myresult['fullname'])
            myresult['email'] = OurAuth.TextDecode(myresult['email'])
            myresult['username'] = OurAuth.TextDecode(myresult['username'])
            myresult['bio'] = OurAuth.TextDecode(myresult['bio'])
            myresult['country'] = OurAuth.TextDecode(myresult['country'])
            myresult['role'] = OurAuth.TextDecode(myresult['role'])
            myresult['store'] = OurAuth.TextDecode(myresult['store'])
            myresult['role_id'] = "1"
            myresult['country_currency'] = OurAuth.TextDecode(myresult['country_currency'])
            if myresult['phone']:
                myresult['phone'] = OurAuth.TextDecode(myresult['phone'])
            
            if myresult['img'] in ('0', 0, 'NULL', 'null', None, 'None'):
                myresult['img'] = "0"
            else:
                myresult['img'] = OurAuth.TextDecode(myresult['img'])
            
            if int(myresult['activeaccount']) == OurAuth.account_is_active():
                payload = {
                    'user_id': myresult['id'],
                    'role_id': myresult['role_id'],
                    'email': myresult['email']
                }
                tresponse = OurAuth.createToken(payload)
                #return tresponse
                if tresponse['status'] == "success":
                    #return jsonify
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Profile updated",
                        "data": {
                            "user": myresult
                        },
                        "utfa": tresponse['token']
                    }
                    return response
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry, something is wrong with your authentication, Please contact the admin"
                    }
                    return response
            else:
                response = {
                    "status": "info",
                    "code": 400,
                    "message": "ACTIVATE YOUR ACCOUNT. Sorry your account isn't activated yet, kindly check your email for an activation link to activate your account before you can continue."
                }
                return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry this account does not exist kinly logout and try to login again."
            }
            return response

    @staticmethod
    def get_user_channels(data):
        #return SendServices.make_insert()
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(4)
        page_limit = int(per_page * 1)
        v_num = int(0)
        last_channel = int('0')
        #return v_num

        sql = "SELECT \
            mmadu_ochis.id AS id, \
            mmadu_ochis.ahammadu AS fullname,\
            mmadu_ochis.emmadu AS email,\
            mmadu_ochis.ummadu AS username,\
            mmadu_ochis.ibuonye AS role,\
            mmadu_ochis.akuko AS bio,\
            mmadu_ochis.pmmadu AS phone,\
            mmadu_ochis.zeemu AS img,\
            mmadu_ochis.anyanatago AS activeaccount,\
            osuso_ochis.mmadu_id AS channel_id,\
            osuso_ochis.sobe_id AS follower_id,\
            ebe_mmamma_ochis.iche AS country,\
            ebe_mmamma_ochis.id AS country_id,\
            ebe_mmamma_ochis.odiego AS country_currency\
            FROM mmadu_ochis\
            JOIN ebe_mmamma_ochis ON mmadu_ochis.ebe_id = ebe_mmamma_ochis.id\
            LEFT JOIN osuso_ochis ON osuso_ochis.mmadu_id = mmadu_ochis.id\
            WHERE osuso_ochis.sobe_id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = 1 ORDER BY mmadu_ochis.id DESC LIMIT " + str(page_limit)

        val = (str(data['userdata']['id']),'0')
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        
        if mycursor.rowcount > 0:
            sql_last = "SELECT COUNT(*) FROM osuso_ochis WHERE osuso_ochis.sobe_id =%s"
            val_last = (str(data['userdata']['id']),)

            mycursor.execute(sql_last, val_last)
            rcount = mycursor.fetchone()['COUNT(*)']
            
            for allresult in myresult:
                allresult['fullname'] = OurAuth.TextDecode(allresult['fullname'])
                allresult['email'] = OurAuth.TextDecode(allresult['email'])
                allresult['username'] = OurAuth.TextDecode(allresult['username'])
                allresult['country'] = OurAuth.TextDecode(allresult['country'])
                allresult['role'] = OurAuth.TextDecode(allresult['role'])
                allresult['country_currency'] = OurAuth.TextDecode(allresult['country_currency'])
                if allresult['phone']:
                    allresult['phone'] = OurAuth.TextDecode(allresult['phone'])
                if allresult['bio']:
                    allresult['bio'] = OurAuth.TextDecode(allresult['bio'])
                if allresult['img']:
                    allresult['img'] = OurAuth.TextDecode(allresult['img'])
                
                
                    #allresult['role_id'] = "1"
                    #allresult[each_key] = OurAuth.TextDecode(allresult[each_key])
            
            if len(myresult) > 0:
                new_last_channel = myresult[-1]['id']
            else:
                new_last_channel = last_channel

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Channels gotten successfully",
                    "data": {
                        "channels": myresult,
                        "channels_count": rcount,
                        "last_channel": new_last_channel
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "You have not subscribed to any channel yet.",
                    "data": {
                        "channels": [],
                        #"count": rcount,
                        "last_channel": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
    
    @staticmethod
    def confirm_user_sending(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(20)
        page_limit = int(per_page * 1)
        #return v_num

        sql = "SELECT \
            mmadu_ochis.id AS id,\
            mmadu_ochis.npkosi AS store\
            FROM mmadu_ochis\
            WHERE mmadu_ochis.id = %s ORDER BY mmadu_ochis.id DESC LIMIT " + str(page_limit)

        val = (str(data['userdata']['id']), )
        mycursor.execute(sql, val)
        allresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:

            if allresult['store']:
                allresult['store'] = OurAuth.TextDecode(allresult['store'])
            else:
                allresult['store'] = '0'
            
            tx_response = SendServices.get_country_settings('all', data)
            if tx_response['status'] == 'success':
                if data['send_email'] != data['userdata']['email']:
                    cal_total_pcs = float(data['send_pcs']) + float(tx_response['settings']['shipping_fee_dist'])
                    if float(allresult['store']) >= cal_total_pcs:
                        checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['send_email'])
                        if checkresponse['code'] == 200:
                            tresponse = OurAuth.getToken(data["userdata"])
                            if tresponse['status'] == "success":
                                response = {
                                    "status": "success",
                                    "code": 200,
                                    "message": "Channels gotten successfully",
                                    "data": {
                                        "receiver": {
                                            "email": checkresponse['data']['email'],
                                            "fullname": checkresponse['data']['fullname'],
                                            "username": checkresponse['data']['username'],
                                            "send_pcs": data['send_pcs'],
                                            "shipping_fee": tx_response['settings']['shipping_fee_dist'],
                                            "total": cal_total_pcs
                                        }
                                    },
                                    "utfa": tresponse['token']
                                }
                                return jsonify(response)
                            else:
                                return jsonify(tresponse)
                        else:
                            response = {
                                "status": "warning",
                                "code": 400,
                                "message": "Sorry, the RECEIVER IS NOT REGISTERED on this platform, please kindly tell them to register if they want to receive bread from you."
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "warning",
                            "code": 400,
                            "message": "You do not have sufficient bread in your store.",
                            "data": {
                            }
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Sorry, you can only send to someone else, NOT TO YOURSELF.",
                        "data": {
                        }
                    }
                    return jsonify(response)
                    
            else:
                response = {
                    "status": "error",
                    "code": 401,
                    "message": "Sorry, please for now we can get your country please login to try again.",
                    "data": {
                    }
                }
                return jsonify(response)

                    
        else:
            #return rcount
            response = {
                "status": "error",
                "code": 401,
                "message": "Sorry, you don't have an store created, please create an account.",
                "data": {
                }
            }
            return jsonify(response)

    @staticmethod
    def share_mybread(data):
        if data['verifyisme']:
            data['pword'] = data['verifyisme']
            data['uname'] = data['userdata']['email']
            v_pword_response = SendServices.verify_pword(data)
            if v_pword_response['status'] == 'success':
                mydb = Db.dbfun()
                mycursor = mydb.cursor(dictionary=True)
                
                per_page = int(20)
                page_limit = int(per_page * 1)
                #return v_num

                sql = "SELECT \
                    mmadu_ochis.id AS id,\
                    mmadu_ochis.npkosi AS store\
                    FROM mmadu_ochis\
                    WHERE mmadu_ochis.id = %s ORDER BY mmadu_ochis.id DESC LIMIT " + str(page_limit)

                val = (str(data['userdata']['id']), )
                mycursor.execute(sql, val)
                allresult = mycursor.fetchone()
                
                if mycursor.rowcount > 0:

                    if allresult['store']:
                        allresult['store'] = OurAuth.TextDecode(allresult['store'])
                    else:
                        allresult['store'] = '0'
                    
                    tx_response = SendServices.get_country_settings('all', data)
                    if tx_response['status'] == 'success':
                        if data['send_email'] != data['userdata']['email']:
                            cal_total_pcs = float(data['send_pcs']) + float(tx_response['settings']['shipping_fee_dist'])
                            if float(allresult['store']) >= cal_total_pcs:
                                checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['send_email'])
                                if checkresponse['code'] == 200:
                                    mk_transfer_response = SendServices.make_transfer(data)
                                    if mk_transfer_response['status'] == "success":
                                        tresponse = OurAuth.getToken(data["userdata"])
                                        if tresponse['status'] == "success":
                                            #return rcount
                                            response = {
                                                "status": "success",
                                                "code": 200,
                                                "message": "Bread Shared Successfully",
                                                "utfa": tresponse['token']
                                            }
                                            return jsonify(response)
                                        else:
                                            return jsonify(tresponse)
                                    else:
                                        return jsonify(mk_transfer_response)
                                else:
                                    response = {
                                        "status": "warning",
                                        "code": 400,
                                        "message": "Sorry, the RECEIVER IS NOT REGISTERED on this platform, please kindly tell them to register if they want to receive bread from you."
                                    }
                                    return jsonify(response)
                            else:
                                response = {
                                    "status": "warning",
                                    "code": 400,
                                    "message": "You do not have sufficient bread in your store.",
                                    "data": {
                                    }
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "warning",
                                "code": 400,
                                "message": "Sorry, you can only send to someone else, NOT TO YOURSELF.",
                                "data": {
                                }
                            }
                            return jsonify(response)
                            
                    else:
                        response = {
                            "status": "error",
                            "code": 401,
                            "message": "Sorry, please for now we can get your country please login to try again.",
                            "data": {
                            }
                        }
                        return jsonify(response)
                            
                else:
                    #return rcount
                    response = {
                        "status": "error",
                        "code": 401,
                        "message": "Sorry, you don't have an store created, please create an account.",
                        "data": {
                        }
                    }
                    return jsonify(response)
            else:
                return jsonify(v_pword_response)
        
    @staticmethod
    def get_user_store(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        page_limit = int(per_page * 1)
        last_tx = int('0')
        #return v_num

        sql = "SELECT \
            mmadu_ochis.id AS id,\
            mmadu_ochis.npkosi AS store\
            FROM mmadu_ochis\
            WHERE mmadu_ochis.id = %s ORDER BY mmadu_ochis.id "

        val = (str(data['userdata']['id']), )
        mycursor.execute(sql, val)
        allresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            
            mydb = Db.dbfun()
            mycursor2 = mydb.cursor(dictionary=True)
            
            sql22 = "SELECT \
                omere_ochis.id AS id, \
                omere_ochis.omere_nke_id AS tx_type_id, \
                omere_ochis.omere_egole AS tx_amount, \
                omere_ochis.akuko AS tx_description, \
                omere_ochis.omere_foro AS tx_balance, \
                omere_ochis.omere_ref AS tx_ref,\
                omere_ochis.created_at AS tx_date,\
                omere_nke_ochis.iche AS tx_type\
                FROM omere_ochis\
                JOIN omere_nke_ochis ON omere_ochis.omere_nke_id = omere_nke_ochis.id\
                WHERE omere_ochis.mmadu_id = %s AND omere_ochis.omere_ebeono_id = 1 ORDER BY omere_ochis.id DESC LIMIT " + str(page_limit)

            val22 = (str(data['userdata']['id']),)
            mycursor2.execute(sql22, val22)
            usertx = mycursor2.fetchall()

            if mycursor2.rowcount > 0:
                sql_last = "SELECT COUNT(*) FROM omere_ochis WHERE omere_ochis.mmadu_id =%s"
                val_last = (str(data['userdata']['id']),)

                mycursor2.execute(sql_last, val_last)
                rcount = mycursor2.fetchone()['COUNT(*)']
                
                for utx in usertx:
                    utx['tx_description'] = OurAuth.TextDecode(utx['tx_description'])
                    utx['tx_balance'] = OurAuth.TextDecode(utx['tx_balance'])
                    utx['tx_amount'] = OurAuth.TextDecode(utx['tx_amount'])
                    utx['tx_ref'] = OurAuth.TextDecode(utx['tx_ref'])
                    utx['tx_type'] = OurAuth.TextDecode(utx['tx_type'])
                    utx['tx_date'] = OurAuth.TextDecode(utx['tx_date'])

            else:
                usertx = []
                
            if allresult['store']:
                allresult['store'] = OurAuth.TextDecode(allresult['store'])
            
            if len(usertx) > 0:
                new_last_tx = usertx[-1]['id']
            else:
                new_last_tx = last_tx
            
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Channels gotten successfully",
                    "data": {
                        "store": {
                            "pcs": allresult['store'],
                            "tx": usertx,
                            "tx_count": rcount,
                            "last_tx": new_last_tx
                        }
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry you don't have a store yet.",
            }
            return jsonify(response)
            
    @staticmethod
    def get_user_profile(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(20)
        page_limit = int(per_page * 1)
        #return v_num

        sql = "SELECT \
            mmadu_ochis.id AS id,\
            mmadu_ochis.ahammadu AS fullname,\
            mmadu_ochis.emmadu AS email,\
            mmadu_ochis.ummadu AS username,\
            mmadu_ochis.ibuonye AS role,\
            mmadu_ochis.akuko AS bio,\
            mmadu_ochis.pmmadu AS phone,\
            mmadu_ochis.zeemu AS img,\
            mmadu_ochis.osuso_ole AS nos_of_subscribers,\
            mmadu_ochis.nhota_ole AS nos_of_categories,\
            mmadu_ochis.odide_ole AS nos_of_notes,\
            mmadu_ochis.anyanatago AS activeaccount,\
            osuso_ochis.mmadu_id AS channel_id,\
            osuso_ochis.sobe_id AS follower_id,\
            ebe_mmamma_ochis.iche AS country,\
            ebe_mmamma_ochis.id AS country_id,\
            ebe_mmamma_ochis.odiego AS country_currency\
            FROM mmadu_ochis\
            JOIN ebe_mmamma_ochis ON mmadu_ochis.ebe_id = ebe_mmamma_ochis.id\
            LEFT JOIN osuso_ochis ON mmadu_ochis.id = osuso_ochis.mmadu_id AND osuso_ochis.sobe_id = %s\
            WHERE mmadu_ochis.ummadu = %s ORDER BY mmadu_ochis.id DESC LIMIT " + str(page_limit)

        val = (str(data['userdata']['id']), OurAuth.TextEncode(data['slug']))
        mycursor.execute(sql, val)
        allresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            
            mydb = Db.dbfun()
            mycursor2 = mydb.cursor(dictionary=True)
            
            sql22 = "SELECT \
                nhota_ochis.id AS id, \
                nhota_ochis.iche AS title, \
                nhota_ochis.akuko AS description, \
                nhota_ochis.odide_ole AS number_of_notes, \
                nhota_ochis.nhota_nke_id AS category_type_id\
                FROM nhota_ochis\
                WHERE nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = 0 ORDER BY nhota_ochis.id DESC"

            val22 = (str(allresult['id']),)
            mycursor2.execute(sql22, val22)
            ucategories = mycursor2.fetchall()

            if mycursor2.rowcount > 0:
                for ucat in ucategories:
                    for each_key in ucat:
                        if each_key == 'title' or each_key == 'description':
                            ucat[each_key] = OurAuth.TextDecode(ucat[each_key])
                        
                        if each_key == 'number_of_notes':
                            if ucat[each_key] in ('0', 0, 'NULL', 'null', None, 'None'):
                                ucat[each_key] = '0'
                            else:
                                ucat[each_key] = OurAuth.TextDecode(ucat[each_key])
                                if ucat[each_key] in ('-1', '-2', '-3'):
                                    ucat[each_key] = '0'
            else:
                ucategories = []
                
            allresult['fullname'] = OurAuth.TextDecode(allresult['fullname'])
            allresult['email'] = OurAuth.TextDecode(allresult['email'])
            allresult['username'] = OurAuth.TextDecode(allresult['username'])
            allresult['country'] = OurAuth.TextDecode(allresult['country'])
            allresult['role'] = OurAuth.TextDecode(allresult['role'])
            allresult['country_currency'] = OurAuth.TextDecode(allresult['country_currency'])
            if allresult['nos_of_subscribers'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                allresult['nos_of_subscribers'] = '0'
            else:
                allresult['nos_of_subscribers'] = OurAuth.TextDecode(allresult['nos_of_subscribers'])
                if allresult['nos_of_subscribers'] in ('-1', '-2', '-3'):
                    allresult['nos_of_subscribers'] = '0'
            
            if allresult['nos_of_categories'] in ('0', 0, 'NULL', 'null', None, 'None'):
                allresult['nos_of_categories'] = '0'
            else:
                allresult['nos_of_categories'] = OurAuth.TextDecode(allresult['nos_of_categories'])
                if allresult['nos_of_categories'] in ('-1', '-2', '-3'):
                    allresult['nos_of_categories'] = '0'
            
            if allresult['nos_of_notes'] in ('0', 0, 'NULL', 'null', None, 'None'):
                allresult['nos_of_notes'] = '0'
            else:
                allresult['nos_of_notes'] = OurAuth.TextDecode(allresult['nos_of_notes'])
                if allresult['nos_of_notes'] in ('-1', '-2', '-3'):
                    allresult['nos_of_notes'] = '0'
            
            if allresult['follower_id']:
                allresult['subscribe_status'] = 1
            else:
                allresult['subscribe_status'] = 0
            if allresult['phone']:
                allresult['phone'] = OurAuth.TextDecode(allresult['phone'])
            if allresult['bio']:
                allresult['bio'] = OurAuth.TextDecode(allresult['bio'])
            if allresult['img'] in ('0', 0, 'NULL', 'null', None, 'None'):
                allresult['img'] = "0"
            else:
                allresult['img'] = OurAuth.TextDecode(allresult['img'])
                #allresult['role_id'] = "1"
                #allresult[each_key] = OurAuth.TextDecode(allresult[each_key])

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Channels gotten successfully",
                    "data": {
                        "profile": allresult,
                        "categories": ucategories
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "You have not subscribed to any channel yet.",
                    "data": {
                        "channel": [],
                        #"count": rcount,
                        "last_channel": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)

    @staticmethod
    def get_learning_zone_cat_data(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        page_limit = int(per_page * 1)
        last_category = int('0')

        sql = "SELECT \
            nhota_ochis.id AS id, \
            nhota_ochis.iche AS title, \
            nhota_ochis.odide_ole AS number_of_notes, \
            nhota_ochis.nhota_nke_id AS category_type_id,\
            nhota_nke_ochis.iche AS category_type,\
            nhota_ochis.akuko AS description,\
            nhota_ochis.egoole AS amount,\
            nhota_ochis.mmadu_id AS user_id\
            FROM nhota_ochis\
            JOIN nhota_nke_ochis ON nhota_ochis.nhota_nke_id = nhota_nke_ochis.id\
            WHERE nhota_ochis.id = %s AND nhota_ochis.ebe_ono = %s"

        val = (data['cat_id'], '0')
        mycursor.execute(sql, val)
        allresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            sql_last = "SELECT COUNT(*) FROM nhota_ochis WHERE nhota_ochis.mmadu_id=%s AND nhota_ochis.ebe_ono = %s"
            val_last = (str(data['userdata']['id']), '0')

            mycursor.execute(sql_last, val_last)
            rcount = mycursor.fetchone()['COUNT(*)']

            #return jsonify(myresult)
            allresult['title'] = OurAuth.TextDecode(allresult['title'])
            allresult['amount'] = OurAuth.TextDecode(allresult['amount'])
            allresult['description'] = OurAuth.TextDecode(allresult['description'])
            if allresult['category_type'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                allresult['category_type'] = 'none'
            else:
                allresult['category_type'] = OurAuth.TextDecode(allresult['category_type'])

            if allresult['number_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                    allresult['number_of_notes'] = '0'
            else:
                allresult['number_of_notes'] = OurAuth.TextDecode(allresult['number_of_notes'])
                if allresult['number_of_notes'] in ('-1', '-2', '-3'):
                    allresult['number_of_notes'] = '0'

            
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Category data successfully",
                    "data": {
                        "category": allresult,
                        "count": rcount
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 400,
                    "message": "This Category does not exist.",
                    "data": {
                        "category": [],
                        #"count": rcount,
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
    
    
    @staticmethod
    def update_the_data(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        if data['userdata']['email'] == str(Config.HQ_ADDRESS):
            sql = "UPDATE mmadu_ochis SET mmadu_ochis.ahammadu = %s, mmadu_ochis.ummadu = %s, mmadu_ochis.akuko = %s, mmadu_ochis.zeemu = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
            val = (OurAuth.TextEncode(str(data['fullname'])), OurAuth.TextEncode(str(data['username'])), OurAuth.TextEncode(str(data['bio'])), OurAuth.TextEncode(str(data['img'])), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(str(data['userdata']['email'])))
                
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :

                tx_response = SendServices.get_country_settings('all', data)
                if tx_response['status'] == 'success':
                    old_donations = tx_response['settings']['donations']
                    new_donations = float(old_donations) + float(Config.HQ_DONATIONS)
                    
                    mycursor2 = mydb.cursor(dictionary=True)
                    sql2 = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.onyiye = %s, ebe_mmamma_ochis.updated_at = %s WHERE ebe_mmamma_ochis.id = %s"
                    val2 = (OurAuth.TextEncode(new_donations), OurAuth.TextEncode(datetime.now(OurAuth.ourtimezone)), data['userdata']['country_id'])
                        
                    mycursor2.execute(sql2, val2)
                    mydb.commit()
                    if mycursor2.rowcount > 0 :
                        dresponse = SendServices.get_user_updated_data(data)
                        return dresponse
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Sorry didn't add to donations."
                        }
                        return response
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Sorry Couldn't get country setting."
                    }
                    return response
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Seems no changes was made."
                }
                return response
        else:
            sql = "UPDATE mmadu_ochis SET mmadu_ochis.ahammadu = %s, mmadu_ochis.ummadu = %s, mmadu_ochis.akuko = %s, mmadu_ochis.zeemu = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
            val = (OurAuth.TextEncode(str(data['fullname'])), OurAuth.TextEncode(str(data['username'])), OurAuth.TextEncode(str(data['bio'])), OurAuth.TextEncode(str(data['img'])), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(str(data['userdata']['email'])))
            
            mycursor.execute(sql, val)
            mydb.commit()
            if mycursor.rowcount > 0 :
                
                dresponse = SendServices.get_user_updated_data(data)
                return dresponse

            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Seems no changes was made."
                }
                return response
    
    @staticmethod
    def update_user_profile(data):
        checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['userdata']['email'])
        if checkresponse['code'] == 200:
            if data['email'] == data['userdata']['email']:
                if len(str(data['fullname'])) > 4 and len(str(data['fullname'])) < 40:
                    if len(str(data['bio'])) > 4 and len(str(data['bio'])) < 61:
                        if len(str(data['username'])) >  4 and len(str(data['fullname'])) < 40:
                            if str(data['username']).isalnum():
                                data['username'] = str(data['username']).lower()
                                checkresponse2 = SendServices.check_data_exist('ummadu', 'mmadu_ochis', data['username'])
                                if checkresponse2['code'] == 200 :
                                    if checkresponse2['data']['id'] == data['userdata']['id']:
                                        dresponse = SendServices.update_the_data(data)
                                        return jsonify(dresponse)
                                    else:
                                        response = {
                                            "status": "error",
                                            "code": 400,
                                            "message": "Sorry the username you provide is not available, try using a different username."
                                        }
                                        return jsonify(response)
                                else:
                                    dresponse = SendServices.update_the_data(data)
                                    return jsonify(dresponse)
                                    
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Please your username should not contain any special character."
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Please username is too short"
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Your bio shouldn't exceed 60 characters"
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Please provide a valid name."
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 401,
                    "message": "Hey Something is wrong somewhere please you will need to login to continue"
                }
                return jsonify(response)

    @staticmethod
    def subscribe_to_channel(data):
        #return jsonify(data['channel_email'])
        
        checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['channel_email'])
        if checkresponse['code'] == 200:
            
            checkresponse2 = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['userdata']['email'])
            if checkresponse2['code'] == 200:
                mydb = Db.dbfun()
                mycursor = mydb.cursor(dictionary=True)

                sql = "SELECT osuso_ochis.id FROM osuso_ochis WHERE osuso_ochis.mmadu_id = %s AND osuso_ochis.sobe_id = %s"
                val = (data['channel_id'], data['userdata']['id'])
                
                mycursor.execute(sql, val)
                mycursor.fetchall()
                
                if mycursor.rowcount > 0:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "You are already subscribed to this channel."
                    }
                    return jsonify(response)
                else:
                    mycursor2 = mydb.cursor(dictionary=True)
                    sql2 = "INSERT INTO osuso_ochis (osuso_ochis.mmadu_id, osuso_ochis.sobe_id, osuso_ochis.created_at) VALUES (%s, %s, %s)"
                    val2 = (data['channel_id'], data['userdata']['id'], OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))))
                    
                    mycursor2.execute(sql2, val2)
                    mydb.commit()
                    
                    if mycursor2.rowcount > 0 :
                        
                        mycursor3 = mydb.cursor(dictionary=True)
                        
                        sql3 = "SELECT \
                            mmadu_ochis.id AS channel_id,\
                            mmadu_ochis.osuso_ole AS nos_of_subscribers \
                            FROM mmadu_ochis\
                            WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = %s"
                        
                        val3 = (data['channel_id'], '0', '1')
                        mycursor3.execute(sql3, val3)
                        myresult3 = mycursor3.fetchone()

                        if mycursor3.rowcount > 0:
                            if myresult3['nos_of_subscribers'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                myresult3['nos_of_subscribers'] = '0'
                            else:
                                myresult3['nos_of_subscribers'] = OurAuth.TextDecode(myresult3['nos_of_subscribers'])

                            new_nos = int(myresult3['nos_of_subscribers'])
                            up_new_nos = new_nos + 1
                            
                            
                            up_mycursor = mydb.cursor(dictionary=True)
                            sqlnew = "UPDATE mmadu_ochis SET mmadu_ochis.osuso_ole = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.ebe_ono = %s"
                            valnew = (OurAuth.TextEncode(str(up_new_nos)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['channel_id'], '1', '0')
                            
                            up_mycursor.execute(sqlnew, valnew)
                            mydb.commit()
                            if up_mycursor.rowcount > 0 :
                                
                                tresponse = OurAuth.getToken(data["userdata"])
                                if tresponse['status'] == "success":
                                    response = {
                                        "status": "success",
                                        "code": 200,
                                        "message": "Subscribed successfully"
                                    }
                                    return jsonify(response)
                                else:
                                    return jsonify(tresponse)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Couldn't update Number of subscribers"
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Couldn't get this channel"
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Subscription NOT successful"
                        }
                        return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry, who are you, and what are you trying to do ?"
                }
                return jsonify(response)
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry, you can't subscribe to this channel for now."
            }
            return jsonify(response)
    
    @staticmethod
    def unsubscribe_from_channel(data):
        #return jsonify(data['channel_email'])
        
        checkresponse = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['channel_email'])
        if checkresponse['code'] == 200:
            
            checkresponse2 = SendServices.check_data_exist('emmadu', 'mmadu_ochis', data['userdata']['email'])
            if checkresponse2['code'] == 200:
                mydb = Db.dbfun()
                mycursor = mydb.cursor(dictionary=True)

                sql = "SELECT osuso_ochis.id FROM osuso_ochis WHERE osuso_ochis.mmadu_id = %s AND osuso_ochis.sobe_id = %s"
                val = (data['channel_id'], data['userdata']['id'])
                
                mycursor.execute(sql, val)
                mycursor.fetchall()
                
                if mycursor.rowcount > 0:
                    mycursor2 = mydb.cursor(dictionary=True)
                    sql2 = "DELETE FROM osuso_ochis WHERE osuso_ochis.mmadu_id = %s AND osuso_ochis.sobe_id = %s"
                    val2 = (data['channel_id'], data['userdata']['id'])
                    
                    mycursor2.execute(sql2, val2)
                    mydb.commit()
                    
                    if mycursor2.rowcount > 0 :
                        
                        mycursor3 = mydb.cursor(dictionary=True)
                        
                        sql3 = "SELECT \
                            mmadu_ochis.id AS channel_id,\
                            mmadu_ochis.osuso_ole AS nos_of_subscribers \
                            FROM mmadu_ochis\
                            WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = %s"
                        
                        val3 = (data['channel_id'], '0', '1')
                        mycursor3.execute(sql3, val3)
                        myresult3 = mycursor3.fetchone()

                        if mycursor3.rowcount > 0:
                            if myresult3['nos_of_subscribers'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                myresult3['nos_of_subscribers'] = '0'
                            else:
                                myresult3['nos_of_subscribers'] = OurAuth.TextDecode(myresult3['nos_of_subscribers'])

                            new_nos = int(myresult3['nos_of_subscribers'])
                            up_new_nos = new_nos - 1
                            
                            
                            up_mycursor = mydb.cursor(dictionary=True)
                            sqlnew = "UPDATE mmadu_ochis SET mmadu_ochis.osuso_ole = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.ebe_ono = %s"
                            valnew = (OurAuth.TextEncode(str(up_new_nos)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['channel_id'], '1', '0')
                            
                            up_mycursor.execute(sqlnew, valnew)
                            mydb.commit()
                            if up_mycursor.rowcount > 0 :
                                tresponse = OurAuth.getToken(data["userdata"])
                                if tresponse['status'] == "success":
                                    response = {
                                        "status": "success",
                                        "code": 200,
                                        "message": "UnSubscribed successfully",
                                        "utfa": tresponse['token']
                                    }
                                    return jsonify(response)
                                else:
                                    return jsonify(tresponse)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Couldn't update Number of subscribers"
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Couldn't get this channel"
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Subscription NOT successful"
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "You are already subscribed to this channel."
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "error",
                    "code": 400,
                    "message": "Sorry, who are you, and what are you trying to do ?"
                }
                return jsonify(response)
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry, you can't subscribe to this channel for now."
            }
            return jsonify(response)
    
    @staticmethod
    def get_category_notes(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        page_limit = int(per_page * 1)
        last_note = int('0')

        sql = "SELECT \
            odide_ochis.id AS id, \
            odide_ochis.odide AS content, \
            odide_ochis.odide_nke_id AS note_type_id,\
            odide_nke_ochis.iche AS note_type\
            FROM odide_ochis\
            JOIN odide_nke_ochis ON odide_ochis.odide_nke_id = odide_nke_ochis.id\
            WHERE odide_ochis.id > %s AND odide_ochis.mmadu_id = %s AND odide_ochis.nhota_id = %s AND odide_ochis.ebe_ono = %s ORDER BY odide_ochis.id DESC LIMIT " + str(page_limit)

        val = (last_note, data['userdata']['id'], data['cat_id'], '0')
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        
        if mycursor.rowcount > 0:
            sql_last = "SELECT COUNT(*) FROM odide_ochis WHERE odide_ochis.nhota_id =%s AND odide_ochis.mmadu_id=%s AND odide_ochis.ebe_ono = %s"
            val_last = (str(data['cat_id']), str(data['userdata']['id']), '0')

            mycursor.execute(sql_last, val_last)
            rcount = mycursor.fetchone()['COUNT(*)']

            for allresult in myresult:
                allresult['content'] = OurAuth.TextDecode(allresult['content'])
                allresult['note_type'] = OurAuth.TextDecode(allresult['note_type'])

            if len(myresult) > 0:
                new_last_note = myresult[-1]['id']
            else:
                new_last_note = last_note

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Notes gotten successfully",
                    "data": {
                        "notes": myresult,
                        "count": rcount,
                        "last_note": new_last_note
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "No Notes.",
                    "data": {
                        "notes": [],
                        #"count": rcount,
                        "last_note": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
    
    @staticmethod
    def get_category_settings(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        
        sql = "SELECT \
            nhota_ochis.id AS id, \
            nhota_ochis.iche AS title, \
            nhota_ochis.odide_ole AS number_of_notes, \
            nhota_ochis.nhota_nke_id AS category_type_id,\
            nhota_nke_ochis.iche AS category_type,\
            nhota_ochis.akuko AS description,\
            nhota_ochis.egoole AS amount,\
            nhota_ochis.mmadu_id AS user_id,\
            mmadu_ochis.emmadu AS useremail,\
            mmadu_ochis.ummadu AS username\
            FROM nhota_ochis\
            JOIN nhota_nke_ochis ON nhota_ochis.nhota_nke_id = nhota_nke_ochis.id\
            JOIN mmadu_ochis ON nhota_ochis.mmadu_id = mmadu_ochis.id\
            WHERE nhota_ochis.id = %s AND nhota_ochis.ebe_ono = %s"

        val = (data['cat_id'], '0')
        mycursor.execute(sql, val)
        allresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            allresult['title'] = OurAuth.TextDecode(allresult['title'])
            allresult['amount'] = OurAuth.TextDecode(allresult['amount'])
            allresult['username'] = OurAuth.TextDecode(allresult['username'])
            allresult['useremail'] = OurAuth.TextDecode(allresult['useremail'])
            allresult['description'] = OurAuth.TextDecode(allresult['description'])
            if allresult['category_type'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                allresult['category_type'] = 'none'
            else:
                allresult['category_type'] = OurAuth.TextDecode(allresult['category_type'])

            if allresult['number_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                    allresult['number_of_notes'] = '0'
            else:
                allresult['number_of_notes'] = OurAuth.TextDecode(allresult['number_of_notes'])
                if allresult['number_of_notes'] in ('-1', '-2', '-3'):
                    allresult['number_of_notes'] = '0'
            
            response = {
                "status": "success",
                "code": 200,
                "message": "Category settings gotten successfully",
                "category": allresult
            }
            return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "This Category does not exist.",
                "category": []
            }
            return response
    
    @staticmethod
    def readnote_financial_bill(data):
        mydb = Db.dbfun()
        
        stvalue_response = SendServices.get_user_store_value(data['userdata']['email'])
        if stvalue_response['status'] == 'success':
            stvalue = stvalue_response['store']

            cat_settings = SendServices.get_category_settings(data)
            if cat_settings['status'] == 'success':
                cat_amount = float(cat_settings['category']['amount'])
                if float(stvalue) >= float(cat_amount):
                    #Debit user
                    new_learner_balance = float(stvalue) - float(cat_amount)

                    t_mycursor = mydb.cursor(dictionary=True)
                    t_tr_sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = 1"
                    t_tr_val = (OurAuth.TextEncode(str(new_learner_balance)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'])
                    t_mycursor.execute(t_tr_sql, t_tr_val)
                    mydb.commit()
                    if t_mycursor.rowcount > 0 :
                        tx_ref = SendServices.vnt_ref()
                        db_tx_data = {
                            "tx_type": "2",
                            "channel_id": data['userdata']['id'],
                            "amount": cat_amount,
                            "tx_desc": "Viewed note w/ "+str(cat_amount)+"pcs. "+str(cat_settings['category']['title'])+"/"+str(cat_settings['category']['username'])+"'s channel",
                            "tx_ref": tx_ref,
                            "tx_balance": new_learner_balance,
                            "tx_status": '1'
                        }
                        log_response = SendServices.log_transaction(db_tx_data)
                        if log_response['status'] == "success":
                            
                            country_settings = SendServices.get_country_settings('all', data)
                            if country_settings['status'] == 'success':
                                #Then distribute the bread
                                donation_share = float(cat_amount) * (float(country_settings['settings']['donation_dist'])/100)
                                
                                student_share = float(cat_amount) * (float(country_settings['settings']['student_dist'])/100)
                                teacher_share = float(cat_amount) * (float(country_settings['settings']['teacher_dist'])/100)
                                temp_store_share = float(cat_amount) * (float(country_settings['settings']['temp_store_dist'])/100)
                                
                                compare_shares = student_share + teacher_share + temp_store_share

                                if float(compare_shares) == float(cat_amount):
                                    
                                    new_temp_store = float(country_settings['settings']['temp_store']) + float(temp_store_share)
                                    tt_mycursor = mydb.cursor(dictionary=True)
                                    tt_tr_sql = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.temp_npkosi = %s, ebe_mmamma_ochis.updated_at = %s WHERE ebe_mmamma_ochis.id = %s"
                                    tt_tr_val = (OurAuth.TextEncode(str(new_temp_store)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), str(country_settings['settings']['id']))
                                    
                                    tt_mycursor.execute(tt_tr_sql, tt_tr_val)
                                    mydb.commit()
                                    if tt_mycursor.rowcount > 0 :

                                        new_country_settings = SendServices.get_country_settings('all', data)
                                        if new_country_settings['status'] == 'success':
                                            donations_store = new_country_settings['settings']['donations']
                                            
                                            check_temp_store = float(new_country_settings['settings']['temp_store'])
                                            if check_temp_store > float(country_settings['settings']['temp_store']):
                                                tx_ref = SendServices.lbgift_ref()
                                                #get teacher's store
                                                teacher_stvalue_response = SendServices.get_user_store_value(cat_settings['category']['useremail'])
                                                if teacher_stvalue_response['status'] == 'success':
                                                    t_stvalue = teacher_stvalue_response['store']
                                                    
                                                    #credit teacher store
                                                    new_teacher_balance = float(t_stvalue) + float(teacher_share)
                                                    tet_mycursor = mydb.cursor(dictionary=True)
                                                    tet_tr_sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
                                                    tet_tr_val = (OurAuth.TextEncode(str(new_teacher_balance)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(cat_settings['category']['useremail']))
                                                    
                                                    tet_mycursor.execute(tet_tr_sql, tet_tr_val)
                                                    mydb.commit()
                                                    if tet_mycursor.rowcount > 0 :
                                                        cr_tx_data = {
                                                            "tx_type": "1",
                                                            "channel_id": cat_settings['category']['user_id'],
                                                            "amount": teacher_share,
                                                            "tx_desc": str(teacher_share)+"pcs received from Learnbread. Continue learning.",
                                                            "tx_ref": tx_ref,
                                                            "tx_balance": new_teacher_balance,
                                                            "tx_status": '1'
                                                        }
                                                        log_cr_response = SendServices.log_transaction(cr_tx_data)
                                                        if log_cr_response['status'] == "success":
                                                            #Get student store
                                                            student_stvalue_response = SendServices.get_user_store_value(data['userdata']['email'])
                                                            if student_stvalue_response['status'] == 'success':
                                                                st_stvalue = student_stvalue_response['store']
                                                                
                                                                #check temp store
                                                                newt_temp_store = float(new_country_settings['settings']['temp_store'])
                                                                newt_temp_store_max = float(new_country_settings['settings']['temp_store_max'])
                                                                
                                                                if newt_temp_store  >= newt_temp_store_max:
                                                                    what_to_add = (float(temp_store_share) * float(new_country_settings['settings']['times_to_add']))
                                                                    if newt_temp_store > what_to_add:
                                                                        give_student = what_to_add
                                                                        send_to_hq = newt_temp_store - give_student
                                                                    
                                                                    if newt_temp_store == what_to_add:
                                                                        give_student = newt_temp_store / 2
                                                                        send_to_hq = newt_temp_store / 2
                                                                else:
                                                                    give_student = 0
                                                                    send_to_hq = 0
                                                                
                                                                if float(donations_store) > float(donation_share):
                                                                    our_donation_gift = donation_share
                                                                else:
                                                                    our_donation_gift = 0


                                                                #credit student store
                                                                student_gift = float(give_student) + float(student_share) + float(our_donation_gift)
                                                                new_student_balance = float(student_gift) + float(st_stvalue)
                                                                stet_mycursor = mydb.cursor(dictionary=True)
                                                                stet_tr_sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
                                                                stet_tr_val = (OurAuth.TextEncode(str(new_student_balance)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(data['userdata']['email']))
                                                                
                                                                stet_mycursor.execute(stet_tr_sql, stet_tr_val)
                                                                mydb.commit()
                                                                if stet_mycursor.rowcount > 0 :
                                                                    scr_tx_data = {
                                                                        "tx_type": "1",
                                                                        "channel_id": data['userdata']['id'],
                                                                        "amount": student_gift,
                                                                        "tx_desc": str(student_gift)+"pcs received from Learnbread. Continue learning.",
                                                                        "tx_ref": tx_ref,
                                                                        "tx_balance": new_student_balance,
                                                                        "tx_status": '1'
                                                                    }
                                                                    slog_cr_response = SendServices.log_transaction(scr_tx_data)
                                                                    if slog_cr_response['status'] == "success":
                                                                        #get note data
                                                                        #return_this_as_noteGLOBAL

                                                                        if float(send_to_hq) > 0:
                                                                            #send to hq
                                                                            hq_stvalue_response = SendServices.get_user_store_value(str(Config.HQ_ADDRESS))
                                                                            if hq_stvalue_response['status'] == 'success':
                                                                                hq_st_stvalue = hq_stvalue_response['store']
                                                                                new_hq_balance = float(hq_st_stvalue) + float(send_to_hq)

                                                                                hqtet_mycursor = mydb.cursor(dictionary=True)
                                                                                hqtet_tr_sql = "UPDATE mmadu_ochis SET mmadu_ochis.npkosi = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.emmadu = %s AND mmadu_ochis.anyanatago = 1"
                                                                                hqtet_tr_val = (OurAuth.TextEncode(str(new_hq_balance)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), OurAuth.TextEncode(str(Config.HQ_ADDRESS)))
                                                                                
                                                                                hqtet_mycursor.execute(hqtet_tr_sql, hqtet_tr_val)
                                                                                mydb.commit()
                                                                                if hqtet_mycursor.rowcount > 0 :
                                                                                    hqcr_tx_data = {
                                                                                        "tx_type": "1",
                                                                                        "channel_id": hq_stvalue_response['id'],
                                                                                        "amount": send_to_hq,
                                                                                        "tx_desc": str(send_to_hq)+"pcs received as Gift from Learnbread.",
                                                                                        "tx_ref": tx_ref,
                                                                                        "tx_balance": new_hq_balance,
                                                                                        "tx_status": '1'
                                                                                    }
                                                                                    hqlog_cr_response = SendServices.log_transaction(hqcr_tx_data)
                                                                                    if hqlog_cr_response['status'] == "success":
                                                                                        if our_donation_gift > 0:
                                                                                            new_store_donations = float(donations_store) - float(our_donation_gift)
                                                                                            country_sql = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.onyiye = %s, ebe_mmamma_ochis.temp_npkosi = %s, ebe_mmamma_ochis.updated_at = %s WHERE ebe_mmamma_ochis.id = %s"
                                                                                            country_val = (OurAuth.TextEncode(str(new_store_donations)), OurAuth.TextEncode('0'), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), new_country_settings['settings']['id'])
                                                                                        else:
                                                                                            country_sql = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.temp_npkosi = %s, ebe_mmamma_ochis.updated_at = %s WHERE ebe_mmamma_ochis.id = %s"
                                                                                            country_val = (OurAuth.TextEncode('0'), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), new_country_settings['settings']['id'])

                                                                                        reset_mycursor = mydb.cursor(dictionary=True)
                                                                                        reset_mycursor.execute(country_sql, country_val)
                                                                                        mydb.commit()
                                                                                        if reset_mycursor.rowcount > 0 :
                                                                                            response = {
                                                                                                "status": "success",
                                                                                                "code": 200,
                                                                                                "message": "Note financial transaction Successful",
                                                                                                "gift": student_gift
                                                                                            }
                                                                                            return response
                                                                                        else:
                                                                                            response = {
                                                                                                "status": "error",
                                                                                                "code": 400,
                                                                                                "message": "Sorry, we couldn't clear the temp store where we keep our bread. Please try again."
                                                                                            }
                                                                                            return response    
                                                                                    else:
                                                                                        response = {
                                                                                            "status": "error",
                                                                                            "code": 400,
                                                                                            "message": "Sorry, we couldn't log the transaction at hq store. Please try again."
                                                                                        }
                                                                                        return response 
                                                                                else:
                                                                                    response = {
                                                                                        "status": "error",
                                                                                        "code": 400,
                                                                                        "message": "Sorry, we couldn't add to hq store. Please try again."
                                                                                    }
                                                                                    return response 
                                                                            else:
                                                                                response = {
                                                                                    "status": "error",
                                                                                    "code": 400,
                                                                                    "message": "Sorry, we couldn't get hq current store value. Please try again."
                                                                                }
                                                                                return response             
                                                                        else:
                                                                            if our_donation_gift > 0:
                                                                                new_store_donations = float(donations_store) - float(our_donation_gift)
                                                                                country_sql = "UPDATE ebe_mmamma_ochis SET ebe_mmamma_ochis.onyiye = %s, ebe_mmamma_ochis.updated_at = %s WHERE ebe_mmamma_ochis.id = %s"
                                                                                country_val = (OurAuth.TextEncode(str(new_store_donations)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), new_country_settings['settings']['id'])

                                                                                reset_mycursor = mydb.cursor(dictionary=True)
                                                                                reset_mycursor.execute(country_sql, country_val)
                                                                                mydb.commit()
                                                                                if reset_mycursor.rowcount > 0 :
                                                                                    response = {
                                                                                        "status": "success",
                                                                                        "code": 200,
                                                                                        "message": "Note financial transaction Successful",
                                                                                        "gift": student_gift
                                                                                    }
                                                                                    return response
                                                                                else:
                                                                                    response = {
                                                                                        "status": "error",
                                                                                        "code": 400,
                                                                                        "message": "Sorry, we couldn't clear the temp store where we keep our bread. Please try again."
                                                                                    }
                                                                                    return response
                                                                            else:
                                                                                response = {
                                                                                    "status": "success",
                                                                                    "code": 200,
                                                                                    "message": "Note financial transaction Successful",
                                                                                    "gift": student_gift
                                                                                }
                                                                                return response
                                                                    else:
                                                                        response = {
                                                                            "status": "error",
                                                                            "code": 400,
                                                                            "message": "Sorry, couldn't log your gift as a student. Please try again."
                                                                        }
                                                                        return response
                                                                else:
                                                                    response = {
                                                                        "status": "error",
                                                                        "code": 400,
                                                                        "message": "Sorry, couldn't add to your store as a student. Please try again."
                                                                    }
                                                                    return response
                                                            else:
                                                                response = {
                                                                    "status": "error",
                                                                    "code": 400,
                                                                    "message": "Sorry, couldn't get the initial pcs of bread in your store as a student. Please try again."
                                                                }
                                                                return response
                                                        else:
                                                            response = {
                                                                "status": "error",
                                                                "code": 400,
                                                                "message": "Sorry, couldn't log the your gift as a teacher. Please try again."
                                                            }
                                                            return response
                                                    else:
                                                        response = {
                                                            "status": "error",
                                                            "code": 400,
                                                            "message": "Sorry, couldn't add to your store as a teacher. Please try again."
                                                        }
                                                        return response
                                                else:
                                                    response = {
                                                        "status": "error",
                                                        "code": 400,
                                                        "message": "Sorry, couldn't get the initial pcs of bread in your store as a teacher. Please try again."
                                                    }
                                                    return response
                                            else:
                                                response = {
                                                    "status": "error",
                                                    "code": 400,
                                                    "message": "Sorry, there is an issue with this computations. Please try again."
                                                }
                                                return response
                                        else:
                                            response = {
                                                "status": "error",
                                                "code": 400,
                                                "message": "Sorry, there is an issue adding bread to the initial store. Please try again."
                                            }
                                            return response
                                    else:
                                        response = {
                                            "status": "error",
                                            "code": 400,
                                            "message": "Sorry, there is an error adding bread to the initial store. Please try again."
                                        }
                                        return response
                                else:
                                    response = {
                                        "status": "error",
                                        "code": 400,
                                        "message": "Sorry, there is an error distributing bread. Please try again."
                                    }
                                    return response
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "We couldn't get country settings."
                                }
                                return response
                        else:
                            return log_response
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Couldn't remove from your store. Try again."
                        }
                        return response
                else:
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Sorry, you do not have sufficient bread to read from this category."
                    }
                    return response
            else:
                response = {
                    "status": "warning",
                    "code": 400,
                    "message": "Sorry, we couldn't get this category's requirements. Please try again."
                }
                return response
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Sorry, we couldn't get your current store value. Please try again."
            }
            return response
    
    @staticmethod
    def read_next_note(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(1)
        page_limit = int(per_page * 1)
        last_note = int(str(data['last_note_id']))

        #Reading of Notes
        if data['category_type'] == 'Random':
            sql = "SELECT \
                odide_ochis.id AS id, \
                odide_ochis.odide AS content, \
                odide_ochis.odide_nke_id AS note_type_id,\
                odide_nke_ochis.iche AS note_type\
                FROM odide_ochis\
                JOIN odide_nke_ochis ON odide_ochis.odide_nke_id = odide_nke_ochis.id\
                WHERE odide_ochis.id != %s AND odide_ochis.nhota_id = %s AND odide_ochis.ebe_ono = 0 AND RAND()<(SELECT (("+str(page_limit)+"/COUNT(*))*10) FROM odide_ochis) ORDER BY RAND() DESC LIMIT " + str(page_limit)
            val = (last_note, str(data['cat_id']),)
        if data['category_type'] == 'Series':
            if data['go_where'] == 'Next':
                last_note = data['last_note_id']
                sql = "SELECT \
                    odide_ochis.id AS id, \
                    odide_ochis.odide AS content, \
                    odide_ochis.odide_nke_id AS note_type_id,\
                    odide_nke_ochis.iche AS note_type\
                    FROM odide_ochis\
                    JOIN odide_nke_ochis ON odide_ochis.odide_nke_id = odide_nke_ochis.id\
                    WHERE odide_ochis.id > %s AND odide_ochis.mmadu_id = %s AND odide_ochis.nhota_id = %s AND odide_ochis.ebe_ono = %s ORDER BY odide_ochis.id LIMIT " + str(page_limit)

                val = (last_note, data['teacher_id'], data['cat_id'], '0')
            
            if data['go_where'] == 'Previous':
                last_note = data['last_note_id']
                sql = "SELECT \
                    odide_ochis.id AS id, \
                    odide_ochis.odide AS content, \
                    odide_ochis.odide_nke_id AS note_type_id,\
                    odide_nke_ochis.iche AS note_type\
                    FROM odide_ochis\
                    JOIN odide_nke_ochis ON odide_ochis.odide_nke_id = odide_nke_ochis.id\
                    WHERE odide_ochis.id < %s AND odide_ochis.mmadu_id = %s AND odide_ochis.nhota_id = %s AND odide_ochis.ebe_ono = %s ORDER BY odide_ochis.id DESC LIMIT " + str(page_limit)

                val = (last_note, data['teacher_id'], data['cat_id'], '0')
            
        mycursor.execute(sql, val)
        myresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            #now do the financial checking
            rfb_response = SendServices.readnote_financial_bill(data)
            if rfb_response['status'] == "success":
                myresult['content'] = OurAuth.TextDecode(myresult['content'])
                myresult['note_type'] = OurAuth.TextDecode(myresult['note_type'])

                tresponse = OurAuth.getToken(data["userdata"])
                if tresponse['status'] == "success":
                    #return rcount
                    response = {
                        "status": "success",
                        "code": 200,
                        "message": "Note gotten successfully",
                        "data": {
                            "note": myresult,
                            "gift": rfb_response['gift']
                        },
                        "utfa": tresponse['token']
                    }
                    return jsonify(response)
                else:
                    return jsonify(tresponse)
            else:
                return jsonify(rfb_response)

        else:
            response = {
                "status": "warning",
                "code": 200,
                "message": "No note to view.",
                "data": {
                    "note": [],
                    "gift": 0
                }
            }
            return jsonify(response)
    
    @staticmethod
    def get_more_user_tx(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        page_limit = int(per_page * 1)
        s_last_tx = str(data['last_tx_id'])
        last_tx = int(s_last_tx)
        #return v_num

        sql = "SELECT \
            mmadu_ochis.id AS id,\
            mmadu_ochis.npkosi AS store\
            FROM mmadu_ochis\
            WHERE mmadu_ochis.id = %s ORDER BY mmadu_ochis.id "

        val = (str(data['userdata']['id']), )
        mycursor.execute(sql, val)
        allresult = mycursor.fetchone()
        
        if mycursor.rowcount > 0:
            
            mydb = Db.dbfun()
            mycursor2 = mydb.cursor(dictionary=True)
            
            if last_tx > 0:
                sql22 = "SELECT \
                    omere_ochis.id AS id, \
                    omere_ochis.omere_nke_id AS tx_type_id, \
                    omere_ochis.omere_egole AS tx_amount, \
                    omere_ochis.akuko AS tx_description, \
                    omere_ochis.omere_foro AS tx_balance, \
                    omere_ochis.omere_ref AS tx_ref,\
                    omere_ochis.created_at AS tx_date,\
                    omere_nke_ochis.iche AS tx_type\
                    FROM omere_ochis\
                    JOIN omere_nke_ochis ON omere_ochis.omere_nke_id = omere_nke_ochis.id\
                    WHERE omere_ochis.id < %s AND omere_ochis.mmadu_id = %s AND omere_ochis.omere_ebeono_id = 1 ORDER BY omere_ochis.id DESC LIMIT " + str(page_limit)

                val22 = (last_tx, str(data['userdata']['id']))
            else:
                sql22 = "SELECT \
                    omere_ochis.id AS id, \
                    omere_ochis.omere_nke_id AS tx_type_id, \
                    omere_ochis.omere_egole AS tx_amount, \
                    omere_ochis.akuko AS tx_description, \
                    omere_ochis.omere_foro AS tx_balance, \
                    omere_ochis.omere_ref AS tx_ref,\
                    omere_ochis.created_at AS tx_date,\
                    omere_nke_ochis.iche AS tx_type\
                    FROM omere_ochis\
                    JOIN omere_nke_ochis ON omere_ochis.omere_nke_id = omere_nke_ochis.id\
                    WHERE omere_ochis.id > %s AND omere_ochis.mmadu_id = %s AND omere_ochis.omere_ebeono_id = 1 ORDER BY omere_ochis.id DESC LIMIT " + str(page_limit)

                val22 = (last_tx, str(data['userdata']['id']))
            mycursor2.execute(sql22, val22)
            usertx = mycursor2.fetchall()

            if mycursor2.rowcount > 0:
                sql_last = "SELECT COUNT(*) FROM omere_ochis WHERE omere_ochis.mmadu_id =%s"
                val_last = (str(data['userdata']['id']),)

                mycursor2.execute(sql_last, val_last)
                rcount = mycursor2.fetchone()['COUNT(*)']
                
                for utx in usertx:
                    utx['tx_description'] = OurAuth.TextDecode(utx['tx_description'])
                    utx['tx_balance'] = OurAuth.TextDecode(utx['tx_balance'])
                    utx['tx_amount'] = OurAuth.TextDecode(utx['tx_amount'])
                    utx['tx_ref'] = OurAuth.TextDecode(utx['tx_ref'])
                    utx['tx_type'] = OurAuth.TextDecode(utx['tx_type'])
                    utx['tx_date'] = OurAuth.TextDecode(utx['tx_date'])

            else:
                usertx = []
                
            if allresult['store']:
                allresult['store'] = OurAuth.TextDecode(allresult['store'])
            
            if len(usertx) > 0:
                new_last_tx = usertx[-1]['id']
            else:
                new_last_tx = last_tx
            
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Channels gotten successfully",
                    "data": {
                        "store": {
                            "pcs": allresult['store'],
                            "tx": usertx,
                            "tx_count": rcount,
                            "last_tx": new_last_tx
                        }
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "Sorry you don't have a store yet.",
            }
            return jsonify(response)
        
    
    @staticmethod
    def get_more_user_channels(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(3)
        page_limit = int(per_page * 1)
        s_last_channel = str(data['last_channel_id'])
        last_channel = int(s_last_channel)

        if last_channel > 0:
            sql = "SELECT \
                mmadu_ochis.id AS id,\
                mmadu_ochis.ahammadu AS fullname,\
                mmadu_ochis.emmadu AS email,\
                mmadu_ochis.ummadu AS username,\
                mmadu_ochis.ibuonye AS role,\
                mmadu_ochis.akuko AS bio,\
                mmadu_ochis.pmmadu AS phone,\
                mmadu_ochis.zeemu AS img,\
                mmadu_ochis.anyanatago AS activeaccount,\
                osuso_ochis.mmadu_id AS channel_id,\
                osuso_ochis.sobe_id AS follower_id,\
                ebe_mmamma_ochis.iche AS country,\
                ebe_mmamma_ochis.id AS country_id,\
                ebe_mmamma_ochis.odiego AS country_currency\
                FROM mmadu_ochis\
                JOIN ebe_mmamma_ochis ON mmadu_ochis.ebe_id = ebe_mmamma_ochis.id\
                JOIN osuso_ochis ON osuso_ochis.mmadu_id = mmadu_ochis.id\
                WHERE mmadu_ochis.id < %s AND osuso_ochis.sobe_id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = 1 ORDER BY mmadu_ochis.id DESC LIMIT " + str(page_limit)

            val = (last_channel, str(data['userdata']['id']),'0')
        else:
            sql = "SELECT \
                mmadu_ochis.id AS id,\
                mmadu_ochis.ahammadu AS fullname,\
                mmadu_ochis.emmadu AS email,\
                mmadu_ochis.ummadu AS username,\
                mmadu_ochis.ibuonye AS role,\
                mmadu_ochis.akuko AS bio,\
                mmadu_ochis.pmmadu AS phone,\
                mmadu_ochis.zeemu AS img,\
                mmadu_ochis.anyanatago AS activeaccount,\
                osuso_ochis.mmadu_id AS channel_id,\
                osuso_ochis.sobe_id AS follower_id,\
                ebe_mmamma_ochis.iche AS country,\
                ebe_mmamma_ochis.id AS country_id,\
                ebe_mmamma_ochis.odiego AS country_currency\
                FROM mmadu_ochis\
                JOIN ebe_mmamma_ochis ON mmadu_ochis.ebe_id = ebe_mmamma_ochis.id\
                JOIN osuso_ochis ON osuso_ochis.mmadu_id = mmadu_ochis.id\
                WHERE mmadu_ochis.id > %s AND osuso_ochis.sobe_id = %s AND mmadu_ochis.ebe_ono = 0 AND mmadu_ochis.anyanatago = 1 ORDER BY mmadu_ochis.id DESC LIMIT " + str(page_limit)

            val = (last_channel, str(data['userdata']['id']),'0')
            
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        
        if mycursor.rowcount > 0:
            sql_last = "SELECT COUNT(*) FROM osuso_ochis WHERE osuso_ochis.sobe_id =%s"
            val_last = (str(data['userdata']['id']),)

            mycursor.execute(sql_last, val_last)
            rcount = mycursor.fetchone()['COUNT(*)']
            
            for allresult in myresult:
                allresult['fullname'] = OurAuth.TextDecode(allresult['fullname'])
                allresult['email'] = OurAuth.TextDecode(allresult['email'])
                allresult['username'] = OurAuth.TextDecode(allresult['username'])
                allresult['country'] = OurAuth.TextDecode(allresult['country'])
                allresult['role'] = OurAuth.TextDecode(allresult['role'])
                allresult['country_currency'] = OurAuth.TextDecode(allresult['country_currency'])
                if allresult['phone']:
                    allresult['phone'] = OurAuth.TextDecode(allresult['phone'])
                if allresult['bio']:
                    allresult['bio'] = OurAuth.TextDecode(allresult['bio'])
                if allresult['img']:
                    allresult['img'] = OurAuth.TextDecode(allresult['img'])

            if len(myresult) > 0:
                new_last_channel = myresult[-1]['id']
            else:
                new_last_channel = last_channel

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "More Channels gotten successfully",
                    "data": {
                        "channels": myresult,
                        "count": rcount,
                        "last_channel": new_last_channel
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "No more Channel.",
                    "data": {
                        "notes": [],
                        #"count": rcount,
                        "last_note": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
    

    @staticmethod
    def get_more_category_notes(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(3)
        page_limit = int(per_page * 1)
        s_last_note = str(data['last_note'])
        last_note = int(s_last_note)

        if last_note > 0:
            sql = "SELECT \
                odide_ochis.id AS id, \
                odide_ochis.odide AS content, \
                odide_ochis.odide_nke_id AS note_type_id,\
                odide_nke_ochis.iche AS note_type\
                FROM odide_ochis\
                JOIN odide_nke_ochis ON odide_ochis.odide_nke_id = odide_nke_ochis.id\
                WHERE odide_ochis.id < %s AND odide_ochis.mmadu_id = %s AND odide_ochis.nhota_id = %s AND odide_ochis.ebe_ono = %s ORDER BY odide_ochis.id DESC LIMIT " + str(page_limit)

            val = (last_note, data['userdata']['id'], data['cat_id'], '0')
        else:
            sql = "SELECT \
                odide_ochis.id AS id, \
                odide_ochis.odide AS content, \
                odide_ochis.odide_nke_id AS note_type_id,\
                odide_nke_ochis.iche AS note_type\
                FROM odide_ochis\
                JOIN odide_nke_ochis ON odide_ochis.odide_nke_id = odide_nke_ochis.id\
                WHERE odide_ochis.id > %s AND odide_ochis.mmadu_id = %s AND odide_ochis.nhota_id = %s AND odide_ochis.ebe_ono = %s ORDER BY odide_ochis.id DESC LIMIT " + str(page_limit)

            val = (last_note, data['userdata']['id'], data['cat_id'], '0')
            
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        
        if mycursor.rowcount > 0:
            sql_last = "SELECT COUNT(*) FROM odide_ochis WHERE odide_ochis.nhota_id =%s AND odide_ochis.mmadu_id=%s AND odide_ochis.ebe_ono = %s"
            val_last = (str(data['cat_id']), str(data['userdata']['id']), '0')

            mycursor.execute(sql_last, val_last)
            rcount = mycursor.fetchone()['COUNT(*)']

            for allresult in myresult:
                allresult['content'] = OurAuth.TextDecode(allresult['content'])
                allresult['note_type'] = OurAuth.TextDecode(allresult['note_type'])

            if len(myresult) > 0:
                new_last_note = myresult[-1]['id']
            else:
                new_last_note = last_note

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Notes gotten successfully",
                    "data": {
                        "notes": myresult,
                        "count": rcount,
                        "last_note": new_last_note
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "No more Notes.",
                    "data": {
                        "notes": [],
                        #"count": rcount,
                        "last_note": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
    
    @staticmethod
    def seemore_get_user_categories(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        page_limit = int(per_page * 1)
        last_category = int(str(data['last_category_id']))

        if last_category > 0:
            sql = "SELECT \
                nhota_ochis.id AS id, \
                nhota_ochis.iche AS title, \
                nhota_ochis.odide_ole AS number_of_notes, \
                nhota_ochis.nhota_nke_id AS category_type_id,\
                nhota_ochis.akuko AS description,\
                nhota_ochis.egoole AS amount,\
                nhota_ochis.mmadu_id AS user_id\
                FROM nhota_ochis\
                WHERE nhota_ochis.id < %s AND nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = %s ORDER BY nhota_ochis.id DESC LIMIT " + str(page_limit)

            val = (last_category, data['userdata']['id'], '0')
        else:
            sql = "SELECT \
                nhota_ochis.id AS id, \
                nhota_ochis.iche AS title, \
                nhota_ochis.odide_ole AS number_of_notes, \
                nhota_ochis.nhota_nke_id AS category_type_id,\
                nhota_ochis.akuko AS description,\
                nhota_ochis.egoole AS amount,\
                nhota_ochis.mmadu_id AS user_id\
                FROM nhota_ochis\
                WHERE nhota_ochis.id > %s AND nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = %s ORDER BY nhota_ochis.id DESC LIMIT " + str(page_limit)

            val = (last_category, data['userdata']['id'], '0')
            
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        
        if mycursor.rowcount > 0:
            sql_last = "SELECT COUNT(*) FROM nhota_ochis WHERE nhota_ochis.mmadu_id=%s AND nhota_ochis.ebe_ono = %s"
            val_last = (str(data['userdata']['id']), '0')

            mycursor.execute(sql_last, val_last)
            rcount = mycursor.fetchone()['COUNT(*)']

            #return jsonify(myresult)
            for allresult in myresult:
                allresult['title'] = OurAuth.TextDecode(allresult['title'])
                allresult['amount'] = OurAuth.TextDecode(allresult['amount'])
                allresult['description'] = OurAuth.TextDecode(allresult['description'])
                if allresult['number_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                        allresult['number_of_notes'] = '0'
                else:
                    allresult['number_of_notes'] = OurAuth.TextDecode(allresult['number_of_notes'])
                    if allresult['number_of_notes'] in ('-1', '-2', '-3'):
                        allresult['number_of_notes'] = '0'

            
            if len(myresult) > 0:
                new_last_category = myresult[-1]['id']
            else:
                new_last_category = last_category

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Categories gotten successfully",
                    "data": {
                        "categories": myresult,
                        "count": rcount,
                        "last_category": new_last_category
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "No Categories.",
                    "data": {
                        "categories": [],
                        #"count": rcount,
                        "last_category": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
    
        
    @staticmethod
    def get_user_categories(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        page_limit = int(per_page * 1)
        last_category = int('0')

        sql = "SELECT \
            nhota_ochis.id AS id, \
            nhota_ochis.iche AS title, \
            nhota_ochis.odide_ole AS number_of_notes, \
            nhota_ochis.nhota_nke_id AS category_type_id,\
            nhota_ochis.akuko AS description,\
            nhota_ochis.egoole AS amount,\
            nhota_ochis.mmadu_id AS user_id\
            FROM nhota_ochis\
            WHERE nhota_ochis.id > %s AND nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = %s ORDER BY nhota_ochis.id DESC LIMIT " + str(page_limit)

        val = (last_category, data['userdata']['id'], '0')
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        
        if mycursor.rowcount > 0:
            sql_last = "SELECT COUNT(*) FROM nhota_ochis WHERE nhota_ochis.mmadu_id=%s AND nhota_ochis.ebe_ono = %s"
            val_last = (str(data['userdata']['id']), '0')

            mycursor.execute(sql_last, val_last)
            rcount = mycursor.fetchone()['COUNT(*)']

            #return jsonify(myresult)
            for allresult in myresult:
                allresult['title'] = OurAuth.TextDecode(allresult['title'])
                allresult['amount'] = OurAuth.TextDecode(allresult['amount'])
                allresult['description'] = OurAuth.TextDecode(allresult['description'])
                if allresult['number_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                        allresult['number_of_notes'] = '0'
                else:
                    allresult['number_of_notes'] = OurAuth.TextDecode(allresult['number_of_notes'])
                    if allresult['number_of_notes'] in ('-1', '-2', '-3'):
                        allresult['number_of_notes'] = '0'

            
            if len(myresult) > 0:
                new_last_category = myresult[-1]['id']
            else:
                new_last_category = last_category

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Categories gotten successfully",
                    "data": {
                        "categories": myresult,
                        "count": rcount,
                        "last_category": new_last_category
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "No Categories.",
                    "data": {
                        "categories": [],
                        #"count": rcount,
                        "last_category": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
          
    @staticmethod
    def get_category_types(data):
        #return SendServices.make_insert()
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(20)
        page_limit = int(per_page * 1)
        v_num = int(0)
        #return v_num

        sql = "SELECT nhota_nke_ochis.id AS id, nhota_nke_ochis.iche AS title FROM nhota_nke_ochis WHERE nhota_nke_ochis.ebe_ono = %s"
        val = ('0',)
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()

        if mycursor.rowcount > 0:
            for allresult in myresult:
                for each_key in allresult:
                    if each_key == 'id':
                        pass
                    else:
                        allresult[each_key] = OurAuth.TextDecode(allresult[each_key])

            response = {
                "status": "success",
                "code": 200,
                "message": "Operation successful",
                "data": {
                    "types": myresult
                }
            }
            return jsonify(response)
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "No Category Type available yet.",
                "data": myresult
            }
            return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"
    
    @staticmethod
    def get_suggested_channels(data):
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        per_page = int(5)
        page_limit = int(per_page * 1)
        v_num = int(0)
        #last_suggested_channel_id = int(data['last_suggested_channel_id'])
        #return v_num

        sql = "SELECT \
            mmadu_ochis.id AS id, \
            mmadu_ochis.ahammadu AS fullname,\
            mmadu_ochis.emmadu AS email,\
            mmadu_ochis.ummadu AS username,\
            mmadu_ochis.ibuonye AS role,\
            mmadu_ochis.akuko AS bio,\
            mmadu_ochis.pmmadu AS phone,\
            mmadu_ochis.zeemu AS img,\
            mmadu_ochis.anyanatago AS activeaccount\
            FROM mmadu_ochis\
            WHERE mmadu_ochis.id !=%s AND mmadu_ochis.ebe_id = %s AND mmadu_ochis.ebe_ono = 0 AND mmadu_ochis.anyanatago = 1 AND RAND()<(SELECT (("+str(page_limit)+"/COUNT(*))*10) FROM mmadu_ochis) ORDER BY RAND() DESC LIMIT " + str(page_limit)
        #SELECT * FROM myTable WHERE ;
        val = (str(data['userdata']['id']), str(data['userdata']['country_id']))
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()
        
        if mycursor.rowcount > 0:
            #mycursor.execute("SELECT COUNT(*) FROM "+tb+" WHERE "+tb+".id >="+str(v_num))
            #rcount = mycursor.fetchone()['COUNT(*)']
            for allresult in myresult:
                allresult['fullname'] = OurAuth.TextDecode(allresult['fullname'])
                allresult['email'] = OurAuth.TextDecode(allresult['email'])
                allresult['username'] = OurAuth.TextDecode(allresult['username'])
                allresult['role'] = OurAuth.TextDecode(allresult['role'])
                if allresult['phone']:
                    allresult['phone'] = OurAuth.TextDecode(allresult['phone'])
                if allresult['bio']:
                    allresult['bio'] = OurAuth.TextDecode(allresult['bio'])
                if allresult['img']:
                    allresult['img'] = OurAuth.TextDecode(allresult['img'])
                
                    #allresult['role_id'] = "1"
                    #allresult[each_key] = OurAuth.TextDecode(allresult[each_key])
                    

            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Suggested Channels gotten successfully",
                    "data": {
                        "channels": myresult,
                        #"count": rcount,
                        "last_channel": myresult[0]['id']
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)
        else:
            tresponse = OurAuth.getToken(data["userdata"])
            if tresponse['status'] == "success":
                #return rcount
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "You have not subscribed to any channel yet.",
                    "data": {
                        "channels": [],
                        #"count": rcount,
                        "last_channel": ""
                        #"base_img_url": ourbaseurl + '\\'
                    },
                    "utfa": tresponse['token']
                }
                return jsonify(response)
            else:
                return jsonify(tresponse)

    @staticmethod
    def delete_cat(data):
        if int(data['id']) > 0:
            checkresponse = SendServices.check_data_exist('id', 'nhota_ochis', data['id'])
            if checkresponse['code'] == 200:
                mydb = Db.dbfun()
                mycursor = mydb.cursor(dictionary=True)
                
                sql_last = "SELECT COUNT(*) FROM odide_ochis WHERE odide_ochis.mmadu_id = %s AND odide_ochis.nhota_id =%s AND odide_ochis.ebe_ono =%s"
                val_last = (str(data['userdata']['id']), str(data['id']), '0')

                mycursor.execute(sql_last, val_last)
                rcount = mycursor.fetchone()['COUNT(*)']

                if int(rcount) > 0:
                    response = {
                        "code": 400,
                        "status": "error",
                        "message": "Sorry this category has some notes. Please ensure there is no note in this category if you want to delete."
                    }
                    return jsonify(response)    
                else:
                    up_mycursor = mydb.cursor(dictionary=True)
                    sqlnew = "UPDATE nhota_ochis SET nhota_ochis.ebe_ono = %s, nhota_ochis.deleted_at = %s WHERE nhota_ochis.mmadu_id = %s AND nhota_ochis.id = %s"
                    valnew = ('1', OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'], str(data['id']))
                    
                    up_mycursor.execute(sqlnew, valnew)
                    mydb.commit()
                    if up_mycursor.rowcount > 0 :
                        
                        mycursor3 = mydb.cursor(dictionary=True)    
                        sql3 = "SELECT \
                            mmadu_ochis.id AS channel_id,\
                            mmadu_ochis.nhota_ole AS nos_of_categories\
                            FROM mmadu_ochis\
                            WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = %s"
                        
                        val3 = (data['userdata']['id'], '0', '1')
                        mycursor3.execute(sql3, val3)
                        myresult3 = mycursor3.fetchone()

                        if mycursor3.rowcount > 0:
                            if myresult3['nos_of_categories'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                myresult3['nos_of_categories'] = '1'
                            else:
                                myresult3['nos_of_categories'] = OurAuth.TextDecode(myresult3['nos_of_categories'])
                                if myresult3['nos_of_categories'] in ('-1', '-2', '-3'):
                                    myresult3['nos_of_categories'] = '1'


                            new_nos = int(myresult3['nos_of_categories'])
                            up_new_nos = new_nos - 1
                            
                            
                            mup_mycursor = mydb.cursor(dictionary=True)
                            msqlnew = "UPDATE mmadu_ochis SET mmadu_ochis.nhota_ole = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.ebe_ono = %s"
                            mvalnew = (OurAuth.TextEncode(str(up_new_nos)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'], '1', '0')
                            
                            mup_mycursor.execute(msqlnew, mvalnew)
                            mydb.commit()
                            if mup_mycursor.rowcount > 0 :
                                
                                tresponse = OurAuth.getToken(data["userdata"])
                                if tresponse['status'] == "success":
                                    response = {
                                        "status": "success",
                                        "code": 200,
                                        "message": "Category deleted successfully",
                                        "utfa": tresponse['token']
                                    }
                                    return jsonify(response)
                                else:
                                    return jsonify(tresponse)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Couldn't update Number of Categories"
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Couldn't get this Category."
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Couldn't delete Category"
                        }
                        return jsonify(response)
            else:
                response = {
                    "code": 400,
                    "status": "error",
                    "message": "Sorry this category does not exist."
                }
                return jsonify(response)
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Please select a valid Category to delete"
            }
            return jsonify(response)    
    
    @staticmethod
    def delete_note(data):
        if int(data['note_id']) > 0:
            checkresponse = SendServices.check_data_exist('id', 'odide_ochis', data['note_id'])
            if checkresponse['code'] == 200:
                mydb = Db.dbfun()
                up_mycursor = mydb.cursor(dictionary=True)
                
                sqlnew = "DELETE FROM odide_ochis WHERE odide_ochis.mmadu_id = %s AND odide_ochis.id = %s AND odide_ochis.nhota_id = %s"
                valnew = (str(data['userdata']['id']), str(data['note_id']), str(data['cat_id']))
                
                up_mycursor.execute(sqlnew, valnew)
                mydb.commit()
                if up_mycursor.rowcount > 0 :
                    
                    mycursor3 = mydb.cursor(dictionary=True)    
                    sql3 = "SELECT \
                        mmadu_ochis.id AS channel_id,\
                        mmadu_ochis.odide_ole AS nos_of_notes\
                        FROM mmadu_ochis\
                        WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = %s"
                    
                    val3 = (data['userdata']['id'], '0', '1')
                    mycursor3.execute(sql3, val3)
                    myresult3 = mycursor3.fetchone()

                    if mycursor3.rowcount > 0:
                        if myresult3['nos_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                            myresult3['nos_of_notes'] = '1'
                        else:
                            myresult3['nos_of_notes'] = OurAuth.TextDecode(myresult3['nos_of_notes'])
                            if myresult3['nos_of_notes'] in ('-1', '-2', '-3'):
                                myresult3['nos_of_notes'] = '1'

                        new_nos = int(myresult3['nos_of_notes'])
                        up_new_nos = new_nos - 1
                        
                        
                        mup_mycursor = mydb.cursor(dictionary=True)
                        msqlnew = "UPDATE mmadu_ochis SET mmadu_ochis.odide_ole = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.ebe_ono = %s"
                        mvalnew = (OurAuth.TextEncode(str(up_new_nos)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'], '1', '0')
                        
                        mup_mycursor.execute(msqlnew, mvalnew)
                        mydb.commit()
                        if mup_mycursor.rowcount > 0 :
                            
                            mycursor34 = mydb.cursor(dictionary=True)    
                            sql34 = "SELECT \
                                nhota_ochis.id AS category_id,\
                                nhota_ochis.odide_ole AS nos_of_notes\
                                FROM nhota_ochis\
                                WHERE nhota_ochis.id = %s AND nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = %s "
                            
                            val34 = (data['cat_id'], data['userdata']['id'], '0')
                            mycursor34.execute(sql34, val34)
                            myresult34 = mycursor34.fetchone()

                            if mycursor34.rowcount > 0:
                                if myresult34['nos_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                    myresult34['nos_of_notes'] = '1'
                                else:
                                    myresult34['nos_of_notes'] = OurAuth.TextDecode(myresult34['nos_of_notes'])
                                    if myresult34['nos_of_notes'] in ('-1', '-2', '-3'):
                                        myresult34['nos_of_notes'] = '1'

                                new_nos4 = int(myresult34['nos_of_notes'])
                                up_new_nos4 = new_nos4 - 1
                                
                                
                                up_mycursor4 = mydb.cursor(dictionary=True)
                                sqlnew4 = "UPDATE nhota_ochis SET nhota_ochis.odide_ole = %s, nhota_ochis.updated_at = %s WHERE nhota_ochis.id = %s AND nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = %s"
                                valnew4 = (OurAuth.TextEncode(str(up_new_nos4)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), str(data['cat_id']), data['userdata']['id'], '0')
                                
                                up_mycursor4.execute(sqlnew4, valnew4)
                                mydb.commit()
                                if up_mycursor4.rowcount > 0 :
                                    
                                    tresponse = OurAuth.getToken(data["userdata"])
                                    if tresponse['status'] == "success":
                                        response = {
                                            "status": "success",
                                            "code": 200,
                                            "message": "Note deleted successfully",
                                            "utfa": tresponse['token']
                                        }
                                        return jsonify(response)
                                    else:
                                        return jsonify(tresponse)
                                else:
                                    response = {
                                        "status": "error",
                                        "code": 400,
                                        "message": "Couldn't update Number of Notes in this category"
                                    }
                                    return jsonify(response)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Couldn't get the category where this note belongs"
                                }
                                return jsonify(response)
                            #Stopped  here
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Couldn't update Number of Notes"
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "Couldn't get this Note."
                        }
                        return jsonify(response)
                else:
                    response = {
                        "status": "error",
                        "code": 400,
                        "message": "Couldn't delete Category"
                    }
                    return jsonify(response)
            else:
                response = {
                    "code": 400,
                    "status": "error",
                    "message": "Sorry this Note does not exist."
                }
                return jsonify(response)
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Please select a valid Note to delete"
            }
            return jsonify(response)    
    
    @staticmethod
    def create_cat(data):
        if len(str(data['title'])) > 4 and len(str(data['title'])) < 40:
            if len(str(data['desc'])) > 4 and len(str(data['desc'])) < 61:
                if str(data['amount']) in SendServices.our_allowed_prices():
                    if str(data['cat_type']) != '0' or str(data['cat_type']) != '':
                        mydb = Db.dbfun()
                        mycursor = mydb.cursor(dictionary=True)
                        
                        sql = "INSERT INTO nhota_ochis (nhota_ochis.iche, nhota_ochis.mmadu_id, nhota_ochis.akuko, nhota_ochis.nhota_nke_id, nhota_ochis.egoole, nhota_ochis.ebe_ono, nhota_ochis.created_at) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                        val = (OurAuth.TextEncode(data['title']), data['userdata']['id'], OurAuth.TextEncode(data['desc']), data['cat_type'], OurAuth.TextEncode(str(data['amount'])), '0', OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))))

                        mycursor.execute(sql, val)
                        mydb.commit()
                        if mycursor.rowcount > 0 :
                            mycursor3 = mydb.cursor(dictionary=True)    
                            sql3 = "SELECT \
                                mmadu_ochis.id AS channel_id,\
                                mmadu_ochis.nhota_ole AS nos_of_categories\
                                FROM mmadu_ochis\
                                WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = %s"
                            
                            val3 = (data['userdata']['id'], '0', '1')
                            mycursor3.execute(sql3, val3)
                            myresult3 = mycursor3.fetchone()

                            if mycursor3.rowcount > 0:
                                if myresult3['nos_of_categories'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                    myresult3['nos_of_categories'] = '0'
                                else:
                                    myresult3['nos_of_categories'] = OurAuth.TextDecode(myresult3['nos_of_categories'])
                                    if myresult3['nos_of_categories'] in ('-1', '-2', '-3'):
                                        myresult3['nos_of_categories'] = '0'

                                new_nos = int(myresult3['nos_of_categories'])
                                up_new_nos = new_nos + 1
                                
                                
                                up_mycursor = mydb.cursor(dictionary=True)
                                sqlnew = "UPDATE mmadu_ochis SET mmadu_ochis.nhota_ole = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.ebe_ono = %s"
                                valnew = (OurAuth.TextEncode(str(up_new_nos)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'], '1', '0')
                                
                                up_mycursor.execute(sqlnew, valnew)
                                mydb.commit()
                                if up_mycursor.rowcount > 0 :
                                    
                                    tresponse = OurAuth.getToken(data["userdata"])
                                    if tresponse['status'] == "success":
                                        response = {
                                            "status": "success",
                                            "code": 200,
                                            "message": "Category created successfully",
                                            "utfa": tresponse['token']
                                        }
                                        return jsonify(response)
                                    else:
                                        return jsonify(tresponse)
                                else:
                                    response = {
                                        "status": "error",
                                        "code": 400,
                                        "message": "Couldn't update Number of Categories"
                                    }
                                    return jsonify(response)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Couldn't get this Category"
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "error creating Category, try again."
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "warning",
                            "code": 400,
                            "message": "Please select a valid category type"
                        }
                        return jsonify(response)
                else:        
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Please select a valid amount"
                    }
                    return jsonify(response)
            else:    
                response = {
                    "status": "warning",
                    "code": 400,
                    "message": "Please your category description shouldn't be less than 4 character or greater than 60 characters."
                }
                return jsonify(response)
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Please provide a valid Category title"
            }
            return jsonify(response)
    
    @staticmethod
    def create_note(data):
        if int(data['cat_id']) > 0:
            if int(data['note_type']) > 0:
                if len(str(data['note_content'])) > 4 and len(str(data['note_content'])) < 131:
                    mydb = Db.dbfun()
                    mycursor = mydb.cursor(dictionary=True)
                    
                    sql = "INSERT INTO odide_ochis (odide_ochis.odide, odide_ochis.odide_nke_id, odide_ochis.nhota_id, odide_ochis.mmadu_id, odide_ochis.created_at) VALUES (%s, %s, %s, %s, %s)"
                    val = (OurAuth.TextEncode(data['note_content']), str(data['note_type']), str(data['cat_id']), str(data['userdata']['id']), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))))

                    mycursor.execute(sql, val)
                    mydb.commit()
                    if mycursor.rowcount > 0 :
                        mycursor3 = mydb.cursor(dictionary=True)    
                        sql3 = "SELECT \
                            mmadu_ochis.id AS channel_id,\
                            mmadu_ochis.odide_ole AS nos_of_notes\
                            FROM mmadu_ochis\
                            WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s AND mmadu_ochis.anyanatago = %s"
                        
                        val3 = (data['userdata']['id'], '0', '1')
                        mycursor3.execute(sql3, val3)
                        myresult3 = mycursor3.fetchone()

                        if mycursor3.rowcount > 0:
                            if myresult3['nos_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                myresult3['nos_of_notes'] = '0'
                            else:
                                myresult3['nos_of_notes'] = OurAuth.TextDecode(myresult3['nos_of_notes'])
                                if myresult3['nos_of_notes'] in ('-1', '-2', '-3'):
                                    myresult3['nos_of_notes'] = '0'

                            new_nos = int(myresult3['nos_of_notes'])
                            up_new_nos = new_nos + 1
                            
                            
                            up_mycursor = mydb.cursor(dictionary=True)
                            sqlnew = "UPDATE mmadu_ochis SET mmadu_ochis.odide_ole = %s, mmadu_ochis.updated_at = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.anyanatago = %s AND mmadu_ochis.ebe_ono = %s"
                            valnew = (OurAuth.TextEncode(str(up_new_nos)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'], '1', '0')
                            
                            up_mycursor.execute(sqlnew, valnew)
                            mydb.commit()
                            if up_mycursor.rowcount > 0 :
                                #Started here
                                mycursor34 = mydb.cursor(dictionary=True)    
                                sql34 = "SELECT \
                                    nhota_ochis.id AS category_id,\
                                    nhota_ochis.odide_ole AS nos_of_notes\
                                    FROM nhota_ochis\
                                    WHERE nhota_ochis.id = %s AND nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = %s "
                                
                                val34 = (data['cat_id'], data['userdata']['id'], '0')
                                mycursor34.execute(sql34, val34)
                                myresult34 = mycursor34.fetchone()

                                if mycursor34.rowcount > 0:
                                    if myresult34['nos_of_notes'] in ("0",'0', 0, 'NULL', 'null', None, 'None'):
                                        myresult34['nos_of_notes'] = '0'
                                    else:
                                        myresult34['nos_of_notes'] = OurAuth.TextDecode(myresult34['nos_of_notes'])
                                        if myresult34['nos_of_notes'] in ('-1', '-2', '-3'):
                                            myresult34['nos_of_notes'] = '0'
                                        

                                    new_nos4 = int(myresult34['nos_of_notes'])
                                    up_new_nos4 = new_nos4 + 1
                                    
                                    
                                    up_mycursor4 = mydb.cursor(dictionary=True)
                                    sqlnew4 = "UPDATE nhota_ochis SET nhota_ochis.odide_ole = %s, nhota_ochis.updated_at = %s WHERE nhota_ochis.id = %s AND nhota_ochis.mmadu_id = %s AND nhota_ochis.ebe_ono = %s"
                                    valnew4 = (OurAuth.TextEncode(str(up_new_nos4)), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), str(data['cat_id']), data['userdata']['id'], '0')
                                    
                                    up_mycursor4.execute(sqlnew4, valnew4)
                                    mydb.commit()
                                    if up_mycursor4.rowcount > 0 :
                                        
                                        tresponse = OurAuth.getToken(data["userdata"])
                                        if tresponse['status'] == "success":
                                            response = {
                                                "status": "success",
                                                "code": 200,
                                                "message": "Note created successfully",
                                                "utfa": tresponse['token']
                                            }
                                            return jsonify(response)
                                        else:
                                            return jsonify(tresponse)
                                    else:
                                        response = {
                                            "status": "error",
                                            "code": 400,
                                            "message": "Couldn't update Number of Notes in this category"
                                        }
                                        return jsonify(response)
                                else:
                                    response = {
                                        "status": "error",
                                        "code": 400,
                                        "message": "Couldn't get the category where this note belongs"
                                    }
                                    return jsonify(response)
                                #Stopped  here
                            else:
                                response = {
                                    "status": "error",
                                    "code": 400,
                                    "message": "Couldn't update user Number of Notes"
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "error",
                                "code": 400,
                                "message": "Couldn't get this user Note"
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "error",
                            "code": 400,
                            "message": "error creating Note, try again."
                        }
                        return jsonify(response)
                else:    
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Please provide a valid text or link. Your note content should not be less than 5 characters or more than 130 characters."
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "warning",
                    "code": 400,
                    "message": "Please select a valid content type for the note you want to create."
                }
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Please ensure you selected a valid Category."
            }
            return jsonify(response)    
    
    @staticmethod
    def save_note_changes(data):
        if int(data['note_id']) > 0:
            if int(str(data['note_type'])) > 0:
                if len(str(data['note_content'])) > 4 and len(str(data['note_content'])) < 131:
                    if str(data['cat_id']) != '0' or str(data['cat_id']) != '':
                        mydb = Db.dbfun()
                        mycursor = mydb.cursor(dictionary=True)
                        
                        sql = "UPDATE odide_ochis SET odide_ochis.odide = %s, odide_ochis.odide_nke_id = %s, odide_ochis.updated_at = %s WHERE odide_ochis.mmadu_id = %s AND odide_ochis.id = %s AND odide_ochis.nhota_id = %s"
                        val = (OurAuth.TextEncode(data['note_content']), data['note_type'], OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'], data['note_id'], data['cat_id'])
                        
                        mycursor.execute(sql, val)
                        mydb.commit()
                        if mycursor.rowcount > 0 :
                            tresponse = OurAuth.getToken(data["userdata"])
                            if tresponse['status'] == "success":
                                response = {
                                    "status": "success",
                                    "code": 200,
                                    "message": "Note updated successfully",
                                    "utfa": tresponse['token']
                                }
                                return jsonify(response)
                            else:
                                return jsonify(tresponse)
                        else:
                            response = {
                                "status": "error",
                                "code": 200,
                                "message": "No changes made.."
                            }
                            return jsonify(response)
                    else:
                        response = {
                            "status": "warning",
                            "code": 400,
                            "message": "Please select a valid category"
                        }
                        return jsonify(response)
                else:    
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Please provide a valid text or link. Your note content should not be less than 5 characters or more than 130 characters."
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "warning",
                    "code": 400,
                    "message": "Please provide a valid content type"
                }
                return jsonify(response)
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Please you need to select a note first"
            }
            return jsonify(response)
    
    @staticmethod
    def save_cat_changes(data):
        if int(data['cat_id']) > 0:
            if len(str(data['title'])) > 4 and len(str(data['title'])) < 40:
                if len(str(data['desc'])) > 4 and len(str(data['desc'])) < 61:
                    if str(data['amount']) in SendServices.our_allowed_prices():
                        if str(data['cat_type']) != '0' or str(data['cat_type']) != '':
                            mydb = Db.dbfun()
                            mycursor = mydb.cursor(dictionary=True)
                            
                            sql = "UPDATE nhota_ochis SET nhota_ochis.iche = %s, nhota_ochis.akuko = %s, nhota_ochis.nhota_nke_id = %s, nhota_ochis.egoole = %s, nhota_ochis.updated_at = %s WHERE nhota_ochis.mmadu_id = %s AND nhota_ochis.id = %s"
                            val = (OurAuth.TextEncode(data['title']), OurAuth.TextEncode(data['desc']), data['cat_type'], OurAuth.TextEncode(str(data['amount'])), OurAuth.TextEncode(str(datetime.now(OurAuth.ourtimezone))), data['userdata']['id'], data['cat_id'])
                            
                            mycursor.execute(sql, val)
                            mydb.commit()
                            if mycursor.rowcount > 0 :
                                tresponse = OurAuth.getToken(data["userdata"])
                                if tresponse['status'] == "success":
                                    response = {
                                        "status": "success",
                                        "code": 200,
                                        "message": "Category updated successfully",
                                        "utfa": tresponse['token']
                                    }
                                    return jsonify(response)
                                else:
                                    return jsonify(tresponse)
                            else:
                                response = {
                                    "status": "error",
                                    "code": 200,
                                    "message": "No changes made.."
                                }
                                return jsonify(response)
                        else:
                            response = {
                                "status": "warning",
                                "code": 400,
                                "message": "Please select a valid category type"
                            }
                            return jsonify(response)
                    else:        
                        response = {
                            "status": "warning",
                            "code": 400,
                            "message": "Please select a valid amount"
                        }
                        return jsonify(response)
                else:    
                    response = {
                        "status": "warning",
                        "code": 400,
                        "message": "Please your category description shouldn't be less than 4 character or greater than 60 characters."
                    }
                    return jsonify(response)
            else:
                response = {
                    "status": "warning",
                    "code": 400,
                    "message": "Please provide a valid Category title"
                }
                return jsonify(response)
        else:
            response = {
                "status": "warning",
                "code": 400,
                "message": "Please you need to select a category first"
            }
            return jsonify(response)
    
    @staticmethod
    def our_allowed_prices():
        theprices = ("20", "30")
        return theprices

    @staticmethod
    def our_prices():
        myresult = [
            {
                "id": "1",
                "amount": "20"
            },
            {
                "id": "2",
                "amount": "30"
            }
        ]
        response = {
            "status": "success",
            "code": 200,
            "message": "Operation successful",
            "data": {
                "prices": myresult
            }
        }
        return jsonify(response)

    @staticmethod
    def our_countries():
        #return SendServices.make_insert()

        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
         
        sql = "SELECT ebe_mmamma_ochis.id AS id, ebe_mmamma_ochis.iche AS title FROM ebe_mmamma_ochis WHERE ebe_mmamma_ochis.ebe_ono = %s"
        val = ('0',)
        mycursor.execute(sql, val)
        myresult = mycursor.fetchall()

        if mycursor.rowcount > 0:
            for allresult in myresult:
                for each_key in allresult:
                    if each_key == 'id':
                        pass
                    else:
                        allresult[each_key] = OurAuth.TextDecode(allresult[each_key])

            response = {
                "status": "success",
                "code": 200,
                "message": "Operation successful",
                "data": {
                    "countries": myresult
                }
            }
            return jsonify(response)
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "No User available yet, kindly contact the admin",
                "data": myresult
            }
            return jsonify(response)

            #for x in myresult:
            #   print(x)
            #return "Yes you are in about page"