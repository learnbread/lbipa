from classes.general.sendservices import SendServices

class UserController:
    
    @staticmethod
    def getuserchannels(data):
        return SendServices.get_user_channels(data)

    @staticmethod
    def subscribetochannel(data):
        return SendServices.subscribe_to_channel(data)
        
    @staticmethod
    def unsubscribefromchannel(data):
        return SendServices.unsubscribe_from_channel(data)
        
    @staticmethod
    def getuserprofile(data):
        return SendServices.get_user_profile(data)

    @staticmethod
    def confirmusersending(data):
        return SendServices.confirm_user_sending(data)

    @staticmethod
    def sharemybread(data):
        return SendServices.share_mybread(data)

    @staticmethod
    def getuserstore(data):
        return SendServices.get_user_store(data)

    @staticmethod
    def getlearningzonecatdata(data):
        return SendServices.get_learning_zone_cat_data (data)

    @staticmethod
    def updateuserprofile(data):
        return SendServices.update_user_profile(data)

    @staticmethod
    def getusercategories(data):
        return SendServices.get_user_categories(data)

    @staticmethod
    def seemoregetusercategories(data):
        return SendServices.seemore_get_user_categories(data)

    @staticmethod
    def getcategorynotes(data):
        return SendServices.get_category_notes(data)

    @staticmethod
    def readnextnote(data):
        return SendServices.read_next_note(data)

    @staticmethod
    def getmorecategorynotes(data):
        return SendServices.get_more_category_notes(data)

    @staticmethod
    def getmoreuserchannels(data):
        return SendServices.get_more_user_channels(data)
    
    @staticmethod
    def getmoreusertx(data):
        return SendServices.get_more_user_tx(data)
    
    @staticmethod
    def getsuggestedchannels(data):
        return SendServices.get_suggested_channels(data)

    @staticmethod
    def ourprices():
        return SendServices.our_prices()

    @staticmethod
    def createcat(data):
        return SendServices.create_cat(data)
    
    @staticmethod
    def createnote(data):
        return SendServices.create_note(data)

    @staticmethod
    def deletecat(data):
        return SendServices.delete_cat(data)

    @staticmethod
    def deletenote(data):
        return SendServices.delete_note(data)

    @staticmethod
    def savenotechanges(data):
        return SendServices.save_note_changes(data)

    @staticmethod
    def savecatchanges(data):
        return SendServices.save_cat_changes(data)

    @staticmethod
    def getcategorytypes(data):
        return SendServices.get_category_types(data)
        
    @staticmethod
    def createtransactionref(data):
        return SendServices.createtransactionref(data)
    
    @staticmethod
    def getgamesbycat(data):
        return SendServices.select(data)
    
    @staticmethod
    def scratch_card(data):
        return SendServices.scratch_card(data)

    @staticmethod
    def get_wallet_balance(data):
        return SendServices.get_wallet_balance(data)