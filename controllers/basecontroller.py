##from flask_cors import CORS, cross_origin
from classes.general.sendservices import SendServices

class BaseController:
    
    @staticmethod
    def homepage(data):
        ##return string
        return SendServices.select(data)
        ##alldb = mycursor.execute("SHOW DATABASES")
        ##remember to send the request data as thus 'the table', 'the column', and 'the data'
        ##mycursor.execute("SHOW DATABASES")
        ##alldata = mycursor.fetchall()
        ##for x in alldata:
            ##print(x)
        
        ##    print("".join(x))
        ##pass
    
    @staticmethod
    def confirm_user_reg(token):
        return SendServices.confirm_user_reg(token)
    
    @staticmethod
    def reset_password(data):
        return SendServices.reset_pwd(data)

    @staticmethod
    def confirm_user_reset_pwd(token):
        return SendServices.confirm_user_reset_pwd(token)
    
    @staticmethod
    def create_new_password(data):
        return SendServices.create_new_password(data)
    
    @staticmethod
    def workpage(data):
        return SendServices.select(data)
    
    @staticmethod
    def workpagebycat(data):
        return SendServices.select(data)
    
    @staticmethod
    def getparentcat(data):
        return SendServices.select(data)
    
    @staticmethod
    def getsettings(data):
        return SendServices.select(data)
    
    @staticmethod
    def adminlogin(data):
        return SendServices.select(data)
    
    @staticmethod
    def userlogin(data):
        return SendServices.login_user(data)
    
    @staticmethod
    def savesettings(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def userreg(data):
        return SendServices.reg_user(data)
    
    @staticmethod
    def userresetpwd(data):
        return SendServices.reset_pwd(data)

    @staticmethod
    def ourcountries():
        return SendServices.our_countries()
    
