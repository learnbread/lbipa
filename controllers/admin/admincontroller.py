from classes.general.sendservices import SendServices

class AdminController:
    
    @staticmethod
    def usersbyrole(data):
        return SendServices.select(data)

    @staticmethod
    def mydashboard(data):
        return SendServices.select(data)
    
    @staticmethod
    def allproducts(data):
        return SendServices.select(data)
    
    @staticmethod
    def getusers(data):
        return SendServices.select(data)
    
    @staticmethod
    def getperms(data):
        return SendServices.select(data)

    @staticmethod
    def updatepost(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def updateproduct(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def updateuser(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def updateprofile(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def updatecat(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def updaterole(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def deletepost(string, data):
        return SendServices.update(string, data)
    
    @staticmethod
    def deleteproduct(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def deleterole(string, data):
        return SendServices.update(string, data)
        
    @staticmethod
    def deletecat(string, data):
        return SendServices.update(string, data)

    @staticmethod
    def deleteuser(string, data):
        return SendServices.update(string, data)
    