import sys
##sys.path.insert(0, '/home/suntsjjc/scratcha.solunotion.com/backend/')

#system library imports
import json

#third party imports
from flask import Flask, redirect, url_for, request
from flask_cors import CORS, cross_origin

#local imports
from controllers.basecontroller import BaseController
from controllers.user.usercontroller import UserController
from controllers.admin.admincontroller import AdminController

from ochiche.ourochiche import OurOchiche

from config.config import Config
#from classes.OurOchiche import OurOchiche

allow_origin = ["https://learnbread.com", "http://localhost:8080", "http://learnbread.com", "http://www.learnbread.com", "https://www.learnbread.com"]
app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": allow_origin}})



main_error = {
    "code": 400,
    "status": "error",
    "message": "Hey, What are you doing"
}
auth_error = {
    "code": 401,
    "status": "sessionerror",
    "message": "Sorry, your Session is expired, you will need to login to continue"
}
platform_error = "https://desktop.learnbread.com"
##print(main_error)
##redirect(url_for('success',name = 'yes it is working'))

#@cross_origin()
#routes

#Public Routes
@app.route('/', methods=['POST'])
def homePage():
    #return json.dumps(request.user_agent.string)
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)

            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)
            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.Public(h_data['Authorization'], i_data["userdata"]):
                    return BaseController.homepage(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/adminlogin', methods=['POST'])
def admin_login():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Public(h_data['Authorization'], i_data["userdata"]):
                return BaseController.adminlogin(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/userlogin', methods=['POST'])
def user_login():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.Public(h_data['Authorization'], i_data["userdata"]):
                    return BaseController.userlogin(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/getuserchannels', methods=['POST'])
def getuser_channels():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getuserchannels(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/getuserprofile', methods=['POST'])
def getuser_profile():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getuserprofile(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/confirmusersending', methods=['POST'])
def confirmuser_sending():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.confirmusersending(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/sharebread', methods=['POST'])
def share_mybread():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.sharemybread(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)


@app.route('/getuserstore', methods=['POST'])
def getuser_store():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getuserstore(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/getlearningzonecatdata', methods=['POST'])
def getlearning_zone_cat_data():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getlearningzonecatdata(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/subscribetochannel', methods=['POST'])
def subscribe_tochannel():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.subscribetochannel(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/unsubscribefromchannel', methods=['POST'])
def unsubscribe_fromchannel():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.unsubscribefromchannel(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/updatechannel', methods=['POST'])
def update_channel():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.updateuserprofile(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)


@app.route('/getcategorynotes', methods=['POST'])
def get_category_notes():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getcategorynotes(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)


@app.route('/readnextnote', methods=['POST'])
def read_next_note():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.readnextnote(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)


@app.route('/getmorecategorynotes', methods=['POST'])
def get_more_category_notes():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getmorecategorynotes(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/seemoregetuserstore', methods=['POST'])
def see_more_get_user_store():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getmoreusertx(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/seemoregetuserchannels', methods=['POST'])
def see_more_get_user_channels():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getmoreuserchannels(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)


@app.route('/getusercategories', methods=['POST'])
def getuser_categories():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getusercategories(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/seemoregetusercategories', methods=['POST'])
def seemore_getuser_categories():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.seemoregetusercategories(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)


@app.route('/getsuggestedchannels', methods=['POST'])
def getsuggested_channels():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getsuggestedchannels(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/resetpassword', methods=['POST'])
def userresetpwd():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v

            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.Public(h_data['Authorization'], i_data):
                    return BaseController.userresetpwd(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/reguser', methods=['POST'])
def userreg():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v

            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.Public(h_data['Authorization'], i_data):
                    return BaseController.userreg(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/ourcategorytypes', methods=['POST'])
def ourcategorytypes():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.getcategorytypes(i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/ourcountries', methods=['POST'])
def ourcountries():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.Public(h_data['Authorization'], i_data):
                    return BaseController.ourcountries()
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)

@app.route('/deletenote', methods=['POST'])
def delete_note():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.deletenote(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/deletecat', methods=['POST'])
def delete_category():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.deletecat(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)


@app.route('/createcat', methods=['POST'])
def create_category():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.createcat(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/createnote', methods=['POST'])
def create_note():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.createnote(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/savenotechanges', methods=['POST'])
def save_note_changes():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.savenotechanges(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)


@app.route('/savecategorychanges', methods=['POST'])
def save_category_changes():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.savecatchanges(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/ourprices', methods=['POST'])
def ourprices():
    if OurOchiche.checkPlatform(request):
        if request.form:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v

            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                    return UserController.ourprices()
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return redirect(platform_error)


@app.route('/savesettings', methods=['POST'])
def save_settings():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Public(h_data['Authorization'], i_data["userdata"]):
                return BaseController.savesettings(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

#User routes

@app.route('/createtransactionref', methods=['POST'])
def createtransactionref():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
        h_data = dict(request.headers)
        
        if OurOchiche.checkOrigin(h_data):
            #return json.dumps(OurOchiche.Admin(h_data['Authorization'], i_data['userdata']))
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.createtransactionref(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/getwalletbalance', methods=['POST'])
def getwalletbalance():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
        h_data = dict(request.headers)
        
        if OurOchiche.checkOrigin(h_data):
            #return json.dumps(OurOchiche.Admin(h_data['Authorization'], i_data['userdata']))
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.get_wallet_balance(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/scratchcard', methods=['POST'])
def scratch_card():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
        h_data = dict(request.headers)
        
        if OurOchiche.checkOrigin(h_data):
            #return json.dumps(OurOchiche.Admin(h_data['Authorization'], i_data['userdata']))
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.scratch_card(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/getgamesbycat', methods=['POST'])
def get_gamesbycat():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
        h_data = dict(request.headers)
        
        if OurOchiche.checkOrigin(h_data):
            #return json.dumps(OurOchiche.Admin(h_data['Authorization'], i_data['userdata']))
            if OurOchiche.User(h_data['Authorization'], i_data["userdata"]):
                return UserController.getgamesbycat(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)


#Admin routes
@app.route('/usersbyrole', methods=['POST'])
def usersByRole():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
        h_data = dict(request.headers)
        
        if OurOchiche.checkOrigin(h_data):
            #return json.dumps(OurOchiche.Admin(h_data['Authorization'], i_data['userdata']))
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.usersbyrole(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/mydashboard', methods=['POST'])
def mydashboard():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.mydashboard(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)         
    else:
        return json.dumps(main_error)

@app.route('/confirm/<token>')
def confirm_user_reg(token):
    response = BaseController.confirm_user_reg(token)
    if response['status'] == "success":
        #r_here = Config.BASE_DOMAIN_URL+'/createnewpassword/'+response['token']
        r_here = 'http://localhost:8080/user/myactivated/'
        return redirect(r_here)
    else:
        #r_here = Config.BASE_DOMAIN_URL+'/expiredlink'
        r_here = 'http://localhost:8080/expiredlink'
        return redirect(r_here)

@app.route('/reset/<token>')
def confirm_user_reset_pwd(token):
    response = BaseController.confirm_user_reset_pwd(token)
    if response['status'] == "success":
        #r_here = Config.BASE_DOMAIN_URL+'/createnewpassword/'+response['token']
        r_here = 'http://localhost:8080/createnewpassword/'+response['token']
        return redirect(r_here)
    else:
        #r_here = Config.BASE_DOMAIN_URL+'/expiredlink'
        r_here = 'http://localhost:8080/expiredlink'
        return redirect(r_here)

@app.route('/createnewpassword', methods=['POST'])
def create_new_password():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
        h_data = dict(request.headers)
        
        if OurOchiche.checkOrigin(h_data):
            #return json.dumps(OurOchiche.Admin(h_data['Authorization'], i_data['userdata']))
            if OurOchiche.Public(h_data['Authorization'], i_data["userdata"]):
                return BaseController.create_new_password(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)
    
@app.route('/allproducts', methods=['POST'])
def allproducts():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.allproducts(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)         
    else:
        return json.dumps(main_error)

@app.route('/getroles', methods=['POST'])
def get_roles():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
        
        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.getroles(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/getusers', methods=['POST'])
def get_users():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.getusers(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/getperms', methods=['POST'])
def get_perms():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.getperms(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/getcat', methods=['POST'])
def get_cat():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                pass
                #return AdminController.getcat(i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/createcattt', methods=['POST'])
def create_categoryy():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                #return AdminController.createcat(i_data['string'], i_data)
                pass
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/createpost', methods=['POST'])
def create_post():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.createpost(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/updatepost', methods=['POST'])
def update_post():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.updatepost(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/updateproduct', methods=['POST'])
def update_product():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v
            
        if request.files and request.files['img']:
            i_data['file'] = request.files['img']
        
        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.updateproduct(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/updateuser', methods=['POST'])
def update_user():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.updateuser(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/updateprofile', methods=['POST'])
def update_profile():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.updateprofile(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/updatecat', methods=['POST'])
def update_cat():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.updatecat(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/updaterole', methods=['POST'])
def update_role():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.updaterole(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/deletepost', methods=['POST'])
def delete_post():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.deletepost(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)

@app.route('/deleteproduct', methods=['POST'])
def delete_product():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.deleteproduct(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)

@app.route('/deleterole', methods=['POST'])
def delete_role():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.deleterole(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)

@app.route('/deletecatt', methods=['POST'])
def delete_cat():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.deletecat(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)

@app.route('/deleteuser', methods=['POST'])
def delete_user():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                return AdminController.deleteuser(i_data['string'], i_data)
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)

@app.route('/createrole', methods=['POST'])
def create_role():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                #return AdminController.createrole(i_data['string'], i_data)
                pass
            else:
                return json.dumps(auth_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/createuser', methods=['POST'])
def create_user():
    if request.form:
        i_data = dict(request.form)
        new_v = json.loads(i_data['userdata'])
        i_data['userdata'] = new_v

        h_data = dict(request.headers)

        if OurOchiche.checkOrigin(h_data):
            pass
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

@app.route('/createproduct', methods=['POST'])
def create_product():
    if request.form:
        if request.files and request.files['img']:
            i_data = dict(request.form)
            new_v = json.loads(i_data['userdata'])
            i_data['userdata'] = new_v
            i_data['file'] = request.files['img']
            
            h_data = dict(request.headers)

            if OurOchiche.checkOrigin(h_data):
                if OurOchiche.Admin(h_data['Authorization'], i_data["userdata"]):
                    return AdminController.createproduct(i_data['string'], i_data)
                else:
                    return json.dumps(auth_error)
            else:
                return json.dumps(main_error)
        else:
            return json.dumps(main_error)
    else:
        return json.dumps(main_error)

if __name__ == '__main__':
   app.run()