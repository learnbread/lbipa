from datetime import datetime, timedelta
import tzlocal
import pytz
from auth.ourjwt import OurJwt
from config.dbConn import Db
from config.config import Config



class OurAuth:
    ourlocaltz = tzlocal.get_localzone().zone
    
    ourtimezone = pytz.timezone('Africa/Lagos')
    
    @staticmethod
    def AuthEncode(payload):
        return OurJwt.ourEncode(payload)

    @staticmethod
    def AuthDecode(token):
        return OurJwt.ourDecode(token)

    @staticmethod
    def NotExpireSoon(decodedtoken):
        ourexp = datetime.strptime(decodedtoken['ourtime'], "%Y-%m-%d %H:%M:%S.%f%z")
        nowtime = datetime.now(OurAuth.ourtimezone)
        some_exp = ourexp + timedelta(minutes= -5)
        
        if some_exp > nowtime:
            return True
        else:
            return False

    @staticmethod
    def IsTokenValid(decodedtoken):
        ourexp = datetime.strptime(decodedtoken['ourtime'], "%Y-%m-%d %H:%M:%S.%f%z")
        nowtime = datetime.now(OurAuth.ourtimezone)
        
        if ourexp > nowtime:
            return True
        else:
            return False
    
    @staticmethod
    def ValidateBothData(vdata, u_data):
        if vdata['role_id'] == u_data['role_id']:
            if vdata['user_id'] == u_data['id']:
                if vdata['email'] == u_data['email']:
                    if vdata['role_id'] == u_data['role_id']:
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    
    @staticmethod
    def ValidateBothAdminData(vdata, u_data):
        if vdata['role_id'] == u_data['role_id']:
            if vdata['user_id'] == u_data['id']:
                if vdata['email'] == u_data['email']:
                    if vdata['role_id'] == OurAuth.admin():
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    
    @staticmethod
    def ValidateBothUserData(vdata, u_data):
        if "role_id" in vdata:
            if u_data['role_id']:
                if vdata['role_id'] == u_data['role_id']:
                    if vdata['user_id'] == u_data['id']:
                        if vdata['email'] == u_data['email']:
                            if vdata['role_id'] == OurAuth.user():
                                return True
                            else:
                                return False
                        else:
                            return False
                    else:
                        return False
                else:
                    return False
            else:
                return False
        else:
            return False
    
    @staticmethod
    def getauth(data):
        if data == "User":
            return "1"
    
    @staticmethod
    def getauth22(data):
        if data == "User":
            return "1"
            
        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)

        tb = "uroles"
        field = "title"

        try:
            #data_e = 'thankgoduchecyril@gmail.com'
            sql = "SELECT "+tb+".id FROM "+tb+" WHERE "+tb+"."+field+" = %s AND "+tb+".status = %s"
            val = (data, 0)
            mycursor.execute(sql, val)
            
            myresult = mycursor.fetchone()
            
            if mycursor.rowcount > 0:
                return myresult['id']
            else:
                return 0
        except:
            response = {
                "status": "error",
                "code": 100,
                "message": "Sorry your sql"
            }
            return response
        
        finally:
            Db.closeConnection(mydb, mycursor)
            #return "MySQL connection is closed"
    
    @staticmethod
    def admin():
        return OurAuth.getauth("Admin")
    
    @staticmethod
    def editor():
        return OurAuth.getauth("Editor")
    
    @staticmethod
    def user():
        return OurAuth.getauth("User")
    
    @staticmethod
    def account_is_active():
        return 1

    @staticmethod
    def getToken(userdata):
        userid = int(userdata['id'])
        try:
            mydb = Db.dbfun()
            mycursorr = mydb.cursor(dictionary=True)

            sql = "SELECT mmadu_ochis.tmmadu AS utoken FROM mmadu_ochis WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = %s"
            val = (userid, 0)
            
            mycursorr.execute(sql, val)
            myresultt = mycursorr.fetchone()

            if mycursorr.rowcount > 0:
                response = {
                    "status": "success",
                    "code": 200,
                    "message": "Operation successful",
                    "token": myresultt['utoken']
                }

                decodedtoken = OurAuth.AuthDecode(response['token'])
                if decodedtoken['status']:
                    if OurAuth.IsTokenValid(decodedtoken):
                        if OurAuth.NotExpireSoon(decodedtoken):
                            if OurAuth.ValidateBothData(decodedtoken, userdata):
                                return response
                            else:
                                response = {
                                    "status": "error",
                                    "code": 401,
                                    "message": "1. Why naaa, What do you think you are doing ?",
                                }

                                return response
                        else:
                            if OurAuth.ValidateBothData(decodedtoken, userdata):
                                payload = {
                                    'user_id': decodedtoken['id'],
                                    'email': decodedtoken['email'],
                                    'role_id': decodedtoken['role_id']
                                }

                                re_response = OurAuth.createToken(payload)
                                if re_response['status'] == "success":
                                    response = {
                                        "status": "success",
                                        "code": 200,
                                        "message": "Operation successful",
                                        "token": re_response['token']
                                    }
                                    return response
                                else:
                                    response = {
                                        "status": "error",
                                        "code": 400,
                                        "message": "Sorry couldn't generate you a new token",
                                    }

                                    return response
                            else:
                                response = {
                                    "status": "error",
                                    "code": 401,
                                    "message": "2. Why naaa, What do you think you are doing ?",
                                }

                                return response
                    else:
                        response = {
                            "status": "error",
                            "code": 401,
                            "message": "Sorry Authentication Error",
                        }

                        return response
                    #return decodedtoken
                else:
                    
                    response = {
                        "status": "error",
                        "code": 401,
                        "message": "Sorry Authentication 2 Error",
                    }

                    return response
            else:
                response = {
                    "status": "error",
                    "code": 401,
                    "message": "This User don't have a token",
                }
                return response
        except:
            response = {
                "status": "sessionerror",
                "code": 401,
                "message": "Sorry your session expired, kindly login to proceed."
            }
            return response
        
        finally:
            if(mydb.is_connected()):
                mycursorr.close()
                mydb.close()
                #return "MySQL connection is closed"
    @staticmethod
    def generateToken(payload):
        utok = OurAuth.AuthEncode(payload)

        mydb = Db.dbfun()
        mycursor = mydb.cursor(dictionary=True)
        
        sql = "UPDATE mmadu_ochis SET mmadu_ochis.tmmadu = %s WHERE mmadu_ochis.id = %s AND mmadu_ochis.ebe_ono = 0"
        val = (utok, payload['user_id'])
        
        mycursor.execute(sql, val)
        mydb.commit()
        #return mycursor.rowcount
        if mycursor.rowcount > 0 :
            response = {
                "status": "success",
                "code": 200,
                "message": "User Session Token created Successfully",
                "token": utok
            }
            return response
        else:
            response = {
                "status": "error",
                "code": 400,
                "message": "User Session Token not created"
            }
            return response
        
    @staticmethod
    def createToken(payload):
        
        payload['exp'] = datetime.now(OurAuth.ourtimezone) + timedelta(minutes=int(Config.TOKEN_EXPIRY_TIME_MINS))
        payload['ourtime'] = str(datetime.now(OurAuth.ourtimezone) + timedelta(minutes=Config.TOKEN_EXPIRY_TIME_MINS))
        return OurAuth.generateToken(payload)
    
    @staticmethod
    def createRegToken(payload):
        payload['exp'] = datetime.now(OurAuth.ourtimezone) + timedelta(minutes=int(Config.TOKEN_EXPIRY_TIME_MINS))
        payload['ourtime'] = str(datetime.now(OurAuth.ourtimezone) + timedelta(minutes=Config.TOKEN_EXPIRY_TIME_MINS))
        
        return OurAuth.generateToken(payload)

    @staticmethod
    def updateToken(userdata):
        tresponse = OurAuth.getToken(userdata['id'])
        if tresponse['status'] == 'success':
            decodedtoken = OurAuth.AuthDecode(tresponse['token'])
            if decodedtoken['status']:
                if OurAuth.IsTokenValid(decodedtoken):
                    if OurAuth.NotExpireSoon(decodedtoken):
                        if OurAuth.ValidateBothData(decodedtoken, userdata):
                            return True
                        else:
                            return False
                    else:
                        if OurAuth.ValidateBothData(decodedtoken, userdata):
                            payload = {
                                'user_id': decodedtoken['id'],
                                'email': decodedtoken['email'],
                                'role_id': decodedtoken['role_id']
                            }
                            re_response = OurAuth.createToken(payload)
                            if re_response['status'] == "success":
                                return True
                            else:
                                return False
                        else:
                            return False
                else:
                    return False
                #return decodedtoken
            else:
                return False
        
    @staticmethod
    def TextEncode(t_string):
        # this version makes it also base64:
        ourstring = str(t_string)
        key = Config.AUTH_SECRET_KEY2
        encoded_chars = []
        for i in range(len(ourstring)):
            key_c = key[i % len(key)]
            encoded_c = chr(ord(ourstring[i]) + ord(key_c) % 256)
            encoded_chars.append(encoded_c)
        
        encoded_string = ''.join(encoded_chars)
        return encoded_string

    @staticmethod
    def TextDecode(string):
        # this version makes it also base64:
        key = Config.AUTH_SECRET_KEY2
        #key = "abcd"
        encoded_chars = []
        for i in range(len(string)):
            key_c = key[i % len(key)]
            encoded_c = chr((ord(string[i]) - ord(key_c) + 256) % 256)
            encoded_chars.append(encoded_c)
        
        encoded_string = ''.join(encoded_chars)
        return encoded_string