import os
import jwt

class OurJwt:
    secret = str(os.environ.get('AUTH_ALGORITHM')).encode()
    algorithm = [str(os.environ.get('AUTH_SECRET_KEY'))]

    @staticmethod
    def ourEncode(payload):
        #key = 'secret'
        encoded = jwt.encode(payload, OurJwt.secret, algorithm='HS256')
        return str(encoded)
        #return jwt.encode(payload, OurJwt.secret, OurJwt.algorithm)
        #return "ejkkduajlkdjaJklaldkjafdf].lakjdlkj2208357akdjal.ieoauhdaflkjda"
        #pass
    
    @staticmethod
    def ourDecode(utoken):
        try:
            dtoken = eval(utoken).decode()
            decoded = jwt.decode(dtoken, OurJwt.secret, algorithm=['HS256'], verify=True)
            decoded["status"] = True
            return decoded
            #pass
        except jwt.ExpiredSignature:
            decoded = {
                "status": False
            }
            return decoded
        except jwt.InvalidSignatureError:
            decoded = {
                "status": False
            }
            return decoded

    @staticmethod
    def ourSignature(payload):
        #return "Hello"
        pass
        