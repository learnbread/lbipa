from auth.ourauth import OurAuth
from datetime import datetime, timedelta

class OurOchiche:
    
    @staticmethod
    def checkPlatform(u_request):
        i_data = dict(u_request.form)
        u_platform = i_data['platform']
        i_platform = u_request.user_agent.platform
        if u_platform == i_platform:
            if u_platform in ("Andriod", "webOS", "iPhone", "iPad", "iPod"):
                return True
            else:
                return False
        else:
            return True

    @staticmethod
    def checkOrigin(ih_data):
                
        i_origin = ih_data["Origin"]

        if i_origin in ("https://learnbread.com", "http://localhost:8080", "http://learnbread.com", "http://www.learnbread.com", "https://www.learnbread.com"):
            return True
        else:
            return False
            
    @staticmethod
    def Admin(token, userdata):
        decodedtoken = OurAuth.AuthDecode(token)
        if decodedtoken['status']:
            if OurAuth.IsTokenValid(decodedtoken):
                if OurAuth.ValidateBothAdminData(decodedtoken, userdata):
                    return True
                else:
                    return False
            else:
                return False
            #return decodedtoken
        else:
            return False

    @staticmethod
    def Guest(token, userdata):
        pass
    
    @staticmethod
    def User(token, userdata):
        decodedtoken = OurAuth.AuthDecode(token)
        if decodedtoken['status']:
            if OurAuth.IsTokenValid(decodedtoken):
                if OurAuth.ValidateBothUserData(decodedtoken, userdata):
                    return True
                else:
                    return False
            else:
                return False
            #return decodedtoken
        else:
            return False


    @staticmethod
    def Public(token, userdata):
        return True