import mysql.connector
from mysql.connector import Error

from config.config import Config


class Db:
	@staticmethod
	def dbfun():

		#mysql+mysqlconnector://<user>:<password>@<host>[:<port>]/<dbname>
		mydb = mysql.connector.connect(
			host = Config.MYSQL_HOST,
			user = Config.MYSQL_USER,
			passwd = Config.MYSQL_PASSWORD,
			database = str(Config.MYSQL_DATABASE)
		)

		if mydb.is_connected():
			return mydb
		else:
			print("we are not connecting")
	
	@staticmethod
	def closeConnection(mydb, mycursor):
		try:
			if mydb.is_connected():
				mycursor.close()
				mydb.close()
			else:
				print("We are not clossing")
		except Error as e:
			print (e)