"""Flask config class."""
import os


class Config:
    """Set Flask configuration vars."""

    # General Config
    TESTING = os.environ.get('TESTING')
    DEBUG = os.environ.get('DEBUG')
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SESSION_COOKIE_NAME = os.environ.get('SESSION_COOKIE_NAME')

    # Database
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')
    MYSQL_HOST = os.environ.get('MYSQL_HOST')
    MYSQL_USER = os.environ.get('MYSQL_USER')
    MYSQL_PASSWORD = os.environ.get('MYSQL_PASSWORD')
    MYSQL_DATABASE = os.environ.get('MYSQL_DATABASE')

    # mail server
    # Flask-Mail SMTP server settings
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = os.environ.get('MAIL_PORT')
    MAIL_USE_SSL = False
    MAIL_USE_TLS = False
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_DEFAULT_SENDER = os.environ.get('MAIL_DEFAULT_SENDER')

    # Administrator login email
    FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')

    # AWS
    # AWS_SECRET_KEY = 'OBIVU5BIG$%&*IRTU#GV&^UHACKEDYGFTI7U5EGEWRG'
    # AWS_KEY_ID = 'B&*D%F^&IYGUIFU'

    # My API
    # API_ENDPOINT = 'http://unsecureendpoint.com/'
    # API_ACCESS_TOKEN = 'HBV%^&UDFIUGFYGJHVIFUJ'
    # API_CLIENT_ID = '3857463'

    # Test User Details
    # TEST_USER = 'Johnny "My Real Name" Boi'
    # TEST_PASSWORD = 'usertest'
    # TEST_SOCIAL_SECURITY_NUMBER = '420-69-1738'
    # TEST_SECURITY_QUESTION = 'Main Street, USA'

    #Other General Config Variable
    #FILES AND IMAGES
    #Paths
    IMG_UPLOAD_PATH = os.environ.get('IMG_UPLOAD_PATH')
    FILE_UPLOAD_PATH = os.environ.get('FILE_UPLOAD_PATH')
    PROFILE_UPLOAD_PATH = os.environ.get('PROFILE_UPLOAD_PATH')

    ALLOWED_FILE_EXTENSIONS = os.environ.get('ALLOWED_FILE_EXTENSIONS')
    ALLOWED_IMG_EXTENSIONS = os.environ.get('ALLOWED_IMG_EXTENSIONS')

    #HQ
    HQ_ADDRESS = os.environ.get('HQ_ADDRESS')
    HQ_DONATIONS = os.environ.get('HQ_DONATIONS')

    #BaseURL
    BASE_IMG_URL = os.environ.get('BASE_IMG_URL')
    BASE_FILE_URL = os.environ.get('BASE_FILE_URL')
    BASE_PROFILE_URL = os.environ.get('BASE_PROFILE_URL')
    BASE_DOMAIN_URL = os.environ.get('BASE_DOMAIN_URL')

    #EXPIRY
    TOKEN_EXPIRY_TIME_MINS = 15

    #Auth
    AUTH_ALGORITHM = os.environ.get('AUTH_ALGORITHM')
    AUTH_SECRET_KEY = os.environ.get('AUTH_SECRET_KEY')
    AUTH_SECRET_KEY2 = os.environ.get('AUTH_SECRET_KEY2')
    
class ProdConfig(Config):
    DEBUG = False
    TESTING = False
    DATABASE_URI = os.environ.get('PROD_DATABASE_URI')


class DevConfig(Config):
    DEBUG = True
    TESTING = True
    DATABASE_URI = os.environ.get('DEV_DATABASE_URI')
    #
